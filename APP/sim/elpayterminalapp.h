#ifndef ELPAYTERMINALAPP_H
#define ELPAYTERMINALAPP_H

#include <QThread>

#include "josapi.h"
#include "josusrapp.h"
#include "app.h"

class ELpayTerminal : public POS_PC1000::JUserApp
{
  Q_OBJECT

  int main()
  {
    if (init_Sys())
      return -1;

    if (init_Net())
      return -2;

    init_Terminal();
    init_GUI();

    int res = app_exec();

    close_Terminal();
    close_Net();

    return res;
  }

  void stop()
  {
    app_quit();
  }
};

#endif // ELPAYTERMINALAPP_H
