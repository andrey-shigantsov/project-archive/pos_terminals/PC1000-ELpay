/*
 * logs.c
 *
 *  Created on: 25 февр. 2017 г.
 *      Author: Svetozar
 */

#include <QDebug>
#include <QFile>

#include "SYSTEM/base.h"
#include "SYSTEM/logs.h"

#include <QDateTime>

char sysLogInit = 0;
static QFile logFile;

extern "C"
{
  void init_logs(const char * LogFileName)
  {
    sysLogInit = 1;
    logFile.setFileName(LogFileName);
    logFile.open(QIODevice::WriteOnly | QIODevice::Text);
    logFile.write(QDateTime::currentDateTime().toString("yyyy.MM.dd\n").toStdString().c_str());
    logFile.close();
  }

#define LOG_BASE \
  if (!sysLogInit) return; \
  QDebug debug = qDebug().noquote().nospace(); \
  logFile.open(QIODevice::Append | QIODevice::Text);

static inline void raw_qdebug_write(QDebug & out, const char * data)
{
  size_t len = strlen(data), counter = 0, n0 = 256;
  do
  {
    size_t
        count = len-counter,
        n = qMin(count,n0);
    out << QByteArray(&data[counter], count);
    counter += count;
  } while (counter < len);
}

#define LOG_WRITE(m) \
  raw_qdebug_write(debug,m); \
  logFile.write(m);

#define LOG_WRITE_ENDL \
  logFile.write("\n");

#define LOG_FREE \
  logFile.close();

  void log_text_raw(const char * text)
  {
    LOG_BASE
    LOG_WRITE(text)
    LOG_FREE
  }

  void log_msg_raw(const char * msg)
  {
    LOG_BASE
    LOG_WRITE(QDateTime::currentDateTime().toString("[HH:mm:ss.zzz]").toStdString().c_str())
    LOG_WRITE(msg)
    LOG_WRITE_ENDL
    LOG_FREE
  }
} // extern "C"
