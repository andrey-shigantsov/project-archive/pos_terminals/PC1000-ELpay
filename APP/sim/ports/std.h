/*
 * std.h
 *
 *  Created on: 11 апр. 2017 г.
 *      Author: Svetozar
 */

#ifndef PORTS_STD_H_
#define PORTS_STD_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C"{
#endif

char * dtostrf_p (double val, unsigned char prec, char * sout, size_t soutSize);

#ifdef __cplusplus
}
#endif

#endif /* PORTS_STD_H_ */
