/*
 * assert.c
 *
 *  Created on: 1 мар. 2017 г.
 *      Author: Svetozar
 */

#include "base.h"
#include "exceptions.h"

#include "SYSTEM/logs.h"

typedef void (*_sig_func_ptr)(int);

typedef struct
{
  _sig_func_ptr hardfault;
  _sig_func_ptr memmanage;
} ExceptionHandlers_t;

typedef enum {excHardFault, excMemManage, EXCEPTIONS_COUNT} InternalExceptions_t;

static int ExceptionCode[EXCEPTIONS_COUNT] = {SIGABRT, SIGSEGV};
static _sig_func_ptr exception_handlers[EXCEPTIONS_COUNT];

static ExceptionHandlers_t ExceptionHandlers;

static void exception_default_handler(int ec)
{
  while(1);
}

void init_exceptions()
{
  for (int i = 0; i < EXCEPTIONS_COUNT; ++i)
    exception_handlers[i] = exception_default_handler;
}

static void exception_handler(InternalExceptions_t exc)
{
  LOG(EXC, "%s", exception_code_str(ExceptionCode[exc]));
  exception_handlers[exc](ExceptionCode[exc]);
}

void exception_hardfault_handler()
{
  exception_handler(excHardFault);
}

void exception_memmanage_handler()
{
  exception_handler(excMemManage);
}

_sig_func_ptr signal(int sig, _sig_func_ptr handler)
{
  for (int i = 0; i < EXCEPTIONS_COUNT; ++i)
    if (sig == ExceptionCode[i])
    {
      exception_handlers[i] = handler;
      return handler;
    }
  return 0;
}

void init_ports_base()
{
  init_exceptions();
}
