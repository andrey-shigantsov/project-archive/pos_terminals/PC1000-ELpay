/*
 * std.c
 *
 *  Created on: 5 мар. 2017 г.
 *      Author: Svetozar
 */

#include "SYSTEM/logs.h"

#include <stdarg.h>

void __assert(const char * file, int line, const char * e)
{
  log_printf(INF, "ASSERT: file = \"%s\", line = %d, e = %s", file, line, e);
}

int printf(const char * format, ...)
{
  va_list ap;
  va_start(ap, format);

  char __logbuf[LOG_PRINTF_BUFSIZE];
  int res = vsnprintf(__logbuf, sizeof(__logbuf), format, ap);
  log_msg_raw(__logbuf);

  va_end(ap);

  return res;
}

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <errno.h>

char* skipwhite(char  *q)
{
  char  *p = q;
  while (isspace(*p))
    ++p;
  return p;
}
#define vim_isdigit(x) isdigit(x)

double strtod(const char * str, char ** end)
{
  double d = 0.0;
  int sign;
  int n = 0;
  const char *p, *a;

  a = p = str;
  p = skipwhite((char*)p);

  /* decimal part */
  sign = 1;
  if (*p == '-')
  {
    sign = -1;
    ++p;
  } else if (*p == '+')
  ++p;
  if (vim_isdigit(*p))
  {
    d = (double)(*p++ - '0');
    while (*p && vim_isdigit(*p))
    {
      d = d * 10.0 + (double)(*p - '0');
      ++p;
      ++n;
    }
    a = p;
  } else if (*p != '.')
    goto done;
  d *= sign;

  /* fraction part */
  if (*p == '.')
  {
    double f = 0.0;
    double base = 0.1;
    ++p;

    if (vim_isdigit(*p))
    {
      while (*p && vim_isdigit(*p))
      {
        f += base * (*p - '0') ;
        base /= 10.0;
        ++p;
        ++n;
      }
    }
    d += f * sign;
    a = p;
  }

  /* exponential part */
  if ((*p == 'E') || (*p == 'e'))
  {
    int e = 0;
    ++p;

    sign = 1;
    if (*p == '-')
    {
      sign = -1;
      ++p;
    } else if (*p == '+')
    ++p;

    if (vim_isdigit(*p))
    {
      while (*p == '0')
        ++p;
      e = (int)(*p++ - '0');
      while (*p && vim_isdigit(*p))
      {
        e = e * 10 + (int)(*p - '0');
        ++p;
      }
      e *= sign;
    }
    else if (!vim_isdigit(*(a-1)))
    {
      a = str;
      goto done;
    }
    else if (*p == 0)
      goto done;

    if (d == 2.2250738585072011 && e == -308)
    {
      d = 0.0;
      a = p;
      errno = ERANGE;
      goto done;
    }
    if (d == 2.2250738585072012 && e <= -308)
    {
      d *= 1.0e-308;
      a = p;
      goto done;
    }
    d *= pow(10.0, (double) e);
    a = p;
  }
  else if (p > str && !vim_isdigit(*(p-1)))
  {
    a = str;
    goto done;
  }

done:
  if (end)
    *end = (char*)a;
  return d;
}

#include <stdint.h>

char * dtostrf_p (double val, unsigned char prec, char * sout, size_t soutSize)
{
  int whole = val;
  float mantissa = val - whole;

  int32_t frac = mantissa * powf(10, prec);
  if(frac < 0) frac = -frac;

  char fmt[20];
  snprintf(fmt,sizeof(fmt), "%%d.%%0%dd", prec);
  snprintf(sout,soutSize, fmt, whole, frac);
  return sout;
}
