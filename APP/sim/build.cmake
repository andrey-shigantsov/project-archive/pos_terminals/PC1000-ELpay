set(POS_PC1000_SIM_DIR $ENV{POS_PC1000_SDK_DIR}/TOOLS/Sim)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
# Find the QtWidgets library
find_package(Qt5Widgets)

# КОМПИЛЯТОР -------------------------------------------------

include_directories(
  ${POS_PC1000_SIM_DIR}/include

  ${CMAKE_CURRENT_LIST_DIR}
  ${CMAKE_CURRENT_LIST_DIR}/ports/include

  ${CMAKE_CURRENT_LIST_DIR}/../base
  ${CMAKE_CURRENT_LIST_DIR}/../base/ports/include

  ${CMAKE_CURRENT_LIST_DIR}/../..
  ${CMAKE_CURRENT_LIST_DIR}/../../LIBS
  ${CMAKE_CURRENT_LIST_DIR}/../../LIBS/yajl/api
)

## основные настройки
string(APPEND CMAKE_CXX_FLAGS " -std=gnu++11")
string(APPEND CMAKE_C_FLAGS " -std=gnu99")

set(SIM_OS_TYPE WIN32)

# ЛИНКЕР ------------------------------------------------------

link_directories(${POS_PC1000_SIM_DIR})

set(SIM_LIBS
  Qt5::Widgets
  -lPOS-PC1000-SimulatorD
)
