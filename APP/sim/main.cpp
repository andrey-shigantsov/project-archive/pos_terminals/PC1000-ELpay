#include "pos_pc1000_sim.h"
#include "elpayterminalapp.h"

int main(int argc, char *argv[])
{
  ELpayTerminal App;
  POS_PC1000::Simulator Sim(&App);
  return Sim.initAppAndExec(argc,argv);
}
