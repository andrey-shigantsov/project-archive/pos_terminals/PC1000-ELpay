#include "pos_pc1000_sim.h"
#include "testapp.h"

int main(int argc, char *argv[])
{
  TestBase App;
  POS_PC1000::Simulator Sim(&App);
  return Sim.initAppAndExec(argc,argv);
}
