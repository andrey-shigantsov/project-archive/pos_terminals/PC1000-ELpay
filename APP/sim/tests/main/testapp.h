#ifndef TESTAPP_H
#define TESTAPP_H

#include <QThread>

#include "josapi.h"
#include "josusrapp.h"
#include "tests/base.h"

class TestBase : public POS_PC1000::JUserApp
{
  Q_OBJECT

  int main()
  {
    int res;

    init_test_base();

    SYS_INFO("TEST", "init");
    res = init_test();
    SYS_INFO_RES(res);
    if (res) return res;

    res = test_exec();

    free_test();
    return res;
  }

  void stop()
  {
    system_exit();
  }
};

#endif // TESTAPP_H
