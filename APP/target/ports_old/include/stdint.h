#ifndef STDINT_H
#define STDINT_H

#include <stddef.h>

#define TYPE_INT(t) typedef signed t
#define TYPE_UINT(t) typedef unsigned t

TYPE_INT(char) int8_t;
TYPE_UINT(short) int16_t;
TYPE_INT(int) int32_t;
TYPE_INT(long long) int64_t;

TYPE_UINT(char) uint8_t;
TYPE_UINT(short) uint16_t;
TYPE_UINT(int) uint32_t;
TYPE_UINT(long long) uint64_t;

#undef TYPE_INT
#undef TYPE_UINT

#endif /* STDINT_H */
