/*
 * base.h
 *
 *  Created on: 1 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef PORTS_BASE_H_
#define PORTS_BASE_H_

#ifdef __cplusplus
extern "C"{
#endif

void init_ports_base();

#ifdef __cplusplus
}
#endif

#endif /* PORTS_BASE_H_ */
