/*
 * malloc.c
 *
 *  Created on: 9 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "SYS.MEMORY"
#define LOG_LEVEL WRN
#include "SYSTEM/logs.h"

#include "ports/memory.h"

char * heapBuf[MEMORY_HEAP_SIZE];

#include "libs/user-malloc.h"
static mspace heap;

void init_port_memory()
{
  heap = create_mspace_with_base(heapBuf, MEMORY_HEAP_SIZE, 0);
}

void * malloc(size_t size)
{
  void * p = mspace_malloc(heap, size);
  if (!p) LOG_FXN(ERR,FAIL);
  return p;
}
void * calloc(size_t count, size_t el_size)
{
  void * p = mspace_calloc(heap, count, el_size);
  if (!p) LOG_FXN(ERR,FAIL);
  return p;
}
void * realloc(void * ptr, size_t size)
{
  void *p = mspace_realloc(heap, ptr, size);
  if (!p) LOG_FXN(ERR,FAIL);
  return p;
}
void free(void * ptr)
{
  mspace_free(heap, ptr);
}
