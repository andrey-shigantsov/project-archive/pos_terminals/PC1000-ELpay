/*
 * malloc.h
 *
 *  Created on: 9 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef PORTS_MEMORY_H_
#define PORTS_MEMORY_H_

#include "base.h"

#define MEMORY_MODE_NONE 0
#define MEMORY_MODE_POOL 1
#define MEMORY_MODE_USERMALLOC 2

#define MEMORY_MODE MEMORY_MODE_USERMALLOC

#if MEMORY_MODE != MEMORY_MODE_NONE
#define MEMORY_HEAP_SIZE 0x100000
#endif

#ifdef __cplusplus
extern "C"{
#endif

void init_port_memory();

#ifdef __cplusplus
}
#endif

#endif /* PORTS_MEMORY_H_ */
