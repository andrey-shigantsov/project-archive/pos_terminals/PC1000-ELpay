/*
 * exceptions.h
 *
 *  Created on: 6 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_EXCEPTIONS_H_
#define SYSTEM_EXCEPTIONS_H_

#include <signal.h>
#include <setjmp.h>
#include <string.h>
#include <assert.h>

#ifdef __cplusplus
extern "C"{
#endif

//static void exception_handler(int ec)
//{
//  longjmp(__exit, ec);
//}

#define __TRY_ENABLE_ALL_EXCEPTIONS(fxn, ...) \
  fxn(SIGFPE, __VA_ARGS__); \
  fxn(SIGILL, __VA_ARGS__); \
  fxn(SIGSEGV, __VA_ARGS__); \
  fxn(SIGBUS, __VA_ARGS__); \
  fxn(SIGABRT, __VA_ARGS__); \
  fxn(SIGIOT, __VA_ARGS__);


#ifdef TRY_USING_SIGACTIONS
#define TRY \
{ \
  struct sigaction __sa; \
  memset(&__sa, 0, sizeof(__sa)); \
  static void __s_handler(int sig); \
  __sa.sa_handler = __s_handler; \
  sigemptyset(&__sa.sa_mask); \
  __sa.sa_flags = 0; \
  __TRY_ENABLE_ALL_EXCEPTIONS(sigaction, &__sa, NULL) \
  \
  jmp_buf __exit; \
  int __ec = setjmp(__exit); \
  if (!__ec) \
  {
#else
#define TRY \
{ \
  static void __s_handler(int sig); \
  __TRY_ENABLE_ALL_EXCEPTIONS(signal, __s_handler) \
  \
  jmp_buf __exit; \
  int __ec = setjmp(__exit); \
  if (!__ec) \
  {
#endif

#define CATCH(ec) \
  } \
  else \
  { \
    ec = __ec;

#define TRY_END \
  } \
  void __s_handler(int ec) \
  { \
    longjmp(__exit, ec); \
  } \
}

static inline const char * exception_code_str(int ec)
{
  switch(ec)
  {
  default: return "unknown signal";
  case SIGFPE: return "floating-point exception";
  case SIGILL: return "illegal instruction";
  case SIGSEGV: return "segmentation violation";
  case SIGBUS: return "bus error";
  case SIGABRT: return "abort signal";
  case SIGSYS: return "bad system call";
  }
}

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_EXCEPTIONS_H_ */
