/*
 * logs.c
 *
 *  Created on: 25 февр. 2017 г.
 *      Author: Svetozar
 */

#include "SYSTEM/base.h"
#include "SYSTEM/logs.h"

#include <string.h>

char sysLogInit = 0, sysLogFileName[16];

void init_logs(const char * LogFileName)
{
  strncpy(sysLogFileName, LogFileName, sizeof(sysLogFileName)-1);

  int oldLog = Lib_FileExist((char*)LogFileName);
  if (oldLog >= 0)
  {
    Lib_FileClose(oldLog);
    Lib_FileRemove((char*)LogFileName);
  }

  int Log = Lib_FileOpen((char*)LogFileName, O_CREATE);
  Lib_FileClose(Log);

  sysLogInit = 1;
}

#define LOG_BASE \
if (!sysLogInit) return; \
Lib_ComOpen(COM_DEBUG, "115200,8,n,1"); \
int f = Lib_FileOpen(sysLogFileName, O_RDWR);

#define LOG_WRITE(m) \
{ \
  int __len = strlen(m); \
  for(int __i = 0; __i < __len; ++__i) \
    Lib_ComSendByte(COM_DEBUG, (char)m[__i]); \
  Lib_FileWrite(f, (char*)m, __len); \
}

#define LOG_FREE \
Lib_FileClose(f); \
Lib_ComClose(COM_DEBUG);

void log_text_raw(const char * text)
{
  LOG_BASE
  LOG_WRITE(text)
  LOG_FREE
}

void log_msg_raw(const char * msg)
{
  LOG_BASE
  LOG_WRITE(msg)
  LOG_WRITE("\n")
  LOG_FREE
}
