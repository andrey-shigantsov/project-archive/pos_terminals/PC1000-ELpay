include(${MAIN_DIR}/APP/base/tests/base/cmake)
include(${CMAKE_CURRENT_LIST_DIR}/main/cmake)

set(TESTS_COMMON_HEADERS
  ${TEST_MAIN_HEADERS}
  ${TEST_BASE_HEADERS}
  ${PORTS_TARGET_HEADERS}
  ${SYS_HEADERS}
)
set(TESTS_COMMON_SOURCES
  ${APP_TARGET_INIT_SOURCES}
  ${TEST_MAIN_SOURCES}
  ${TEST_BASE_SOURCES}
  ${PORTS_TARGET_SOURCES}
  ${SYS_SOURCES}
)

macro (add_appTarget_test NAME)
	set(TEST_${NAME}_TARGET ${TEST_${NAME}}.elf)
	add_executable(${TEST_${NAME}_TARGET} ${TEST_${NAME}_HEADERS} ${TEST_${NAME}_SOURCES})
	add_elftobin_target(${TEST_${NAME}_TARGET} ${TEST_${NAME}}.bin)
	target_link_libraries(${TEST_${NAME}_TARGET} ${SYS_LIBS})
	target_compile_definitions(${TEST_${NAME}_TARGET} PUBLIC -DAPP_TEST_${NAME})
endmacro()
