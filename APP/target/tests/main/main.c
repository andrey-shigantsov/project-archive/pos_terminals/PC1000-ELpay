
#include "tests/base.h"

int main()
{
  int res;

  init_test_base();

  SYS_INFO("TEST", "init");
  res = init_test();
  SYS_INFO_RES(res);
  if (res) return res;

  res = test_exec();

  free_test();
  return res;
}
