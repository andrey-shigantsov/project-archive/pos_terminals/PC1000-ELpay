## elftobin
include(${POS_PC1000_TOOLCHAIN}/pos_pc1000_elftobin.cmake)
## api
include(${POS_PC1000_TOOLCHAIN}/pos_pc1000_api_3.cmake)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# КОМПИЛЯТОР -------------------------------------------------

include_directories(
  ${CMAKE_CURRENT_LIST_DIR}
  ${CMAKE_CURRENT_LIST_DIR}/ports/include
)
if(TOOLCHAIN_POS_PC1000_OLD_GCC)
include_directories(${CMAKE_CURRENT_LIST_DIR}/ports_old/include)
endif()
include_directories(
  ${CMAKE_CURRENT_LIST_DIR}/../base
  ${CMAKE_CURRENT_LIST_DIR}/../base/ports/include

  ${CMAKE_CURRENT_LIST_DIR}/../..
  ${CMAKE_CURRENT_LIST_DIR}/../../LIBS
  ${CMAKE_CURRENT_LIST_DIR}/../../LIBS/yajl/api
)

## основные настройки
string(APPEND CMAKE_C_FLAGS " -std=gnu99")

## отладочная информация
string(APPEND CMAKE_C_FLAGS " -g")

## оптимизация
string(APPEND CMAKE_C_FLAGS " -O3")

# ЛИНКЕР ------------------------------------------------------

link_directories(${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

set(LDSCRIPT ${CMAKE_CURRENT_LIST_DIR}/ldscript)
if (TOOLCHAIN_POS_PC1000_OLD_GCC)
string(APPEND CMAKE_EXE_LINKER_FLAGS " -T${LDSCRIPT} -Map current.map")
endif()

set(SYS_LIBS "-lpc1000api" "-lpc1000wlsapi" -lm -lc -lgcc)
