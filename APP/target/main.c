#include "app.h"

int main()
{
  if (init_Sys())
    return -1;

  if (init_Net())
    return -2;

  init_Terminal();
  init_GUI();

  int res = app_exec();

  close_Terminal();
  close_Net();

  return res;
}
