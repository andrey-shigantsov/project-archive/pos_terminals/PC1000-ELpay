/*
 * app.c
 *
 *  Created on: 14 апр. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "APP"
#define LOG_LEVEL INF

#include "logo.h"

#include "ports/all.h"
#include "ELpay_port.h"

#include "SYSTEM/control.h"
#include "SYSTEM/NETWORK/System.h"
#include "SYSTEM/GUI/System.h"
#include "SERVICES/ELpay/Terminal.h"
#include "GUI/MainWidget.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

DECLARE_APP_MSG("ELpay", APP_VER_STR("0","02"))

static ELpayTerminal_t Terminal;
static MainWidget_t MainWidget;

bool isTermID = 0, isAuthInfo = 0, isAuth = 0;

static int init_failure_handler()
{
  system_info_f(8,"\n  Continue?(YES/ESC)");
  while (1)
  {
    BYTE key = wait_keypress();
    switch (key)
    {
    case KEYENTER:
      return 0;

    case KEYCANCEL:
      return 1;

    default:
      break;
    }
  }
}

#define CHECK_AND_RETURN(res) \
  if (res) \
  { \
    SYS_INFO_RES(res); \
    if (init_failure_handler()) \
      return -1; \
  }

int init_Sys()
{
  Lib_AppInit();
  Lib_LcdCls();
  Lib_LcdSetFont(8,16,0);

  init_logs("elpay.log");
  log_printf(INF, "\n===={ app entry point }====\n");

  BYTE * logo = g_Display_logo;
  int logoSize = sizeof(g_Display_logo);
  BYTE env[16];
  env[0] = '\0';
  if (!Lib_FileGetEnv("NOLOGO", env))
  {
    if (strcmp(env, "yes") == 0)
      logo = 0;
#ifndef LOGO_IS_INVERT
    if (logo)
      for (int i = 2; i < logoSize; ++i)
        logo[i] = ~logo[i];
#endif
  }
  system_drawLogo(logo);
  if (logo)
  {
#ifdef LOGO_IS_INVERT
    BYTE textmode = 0x80;
#else
    BYTE textmode = 0x00;
#endif
    Lib_LcdPrintxy(10,55,textmode,"ver. %s", App_Msg.Version);
  }

  SYS_INFO("SYSTEM", "init");

  int res;

  init_ports();

  res = init_system();

  CHECK_AND_RETURN(res)
  return 0;
}

#define GETIP_CHECK_AND_RETURN(res) \
  if (res) \
  { \
    SYS_INFO_RES(res); \
    LOG_FXN(WRN, FAIL); \
    return res; \
  }

#define GETIP_HOST "httpbin.org"
#define GETIP_HOSTPATH "/ip"
static int get_ip()
{
  LOG_FXN(INF,START);
  SYS_INFO("NETWORK", "get IP");

  TcpSocket_t s;
  init_TcpSocket(&s, 2, 1);
  int res = TcpSocket_open(&s, GETIP_HOST, 80);
  GETIP_CHECK_AND_RETURN(res)

  char * request = "GET " GETIP_HOSTPATH " HTTP/1.1\r\n" "Host: " GETIP_HOST "\r\n";
  LOG_FXN_MSG(INF, NONE, "request:\n%s", request);
  res = TcpSocket_send(&s, request, strlen(request));
  GETIP_CHECK_AND_RETURN(res)

  char response[65535];
  uint16_t len = sizeof(response);
  res = TcpSocket_receive(&s, response, &len, 60000);
  GETIP_CHECK_AND_RETURN(res)

  response[len] = '\0';
  if (len)
    LOG_FXN_MSG(INF, NONE, "%s", response);

  res = TcpSocket_close(&s);
  GETIP_CHECK_AND_RETURN(res)

  SYS_INFO_RES(0);
  LOG_FXN(INF,OK);
  return 0;
}

#undef GETIP_CHECK_AND_RETURN

int init_Net()
{
  LOG_FXN(INF,START);
  SYS_INFO("NETWORK", "init");

  int res;

  res = init_network_sys();
  CHECK_AND_RETURN(res)

  BYTE dialNum[32], uid[32], pwd[32];
  dialNum[0] = '\0';
  uid[0] = '\0';
  pwd[0] = '\0';

  if (!Lib_FileGetEnv("NET_NUM", dialNum))
  {
    if (!Lib_FileGetEnv("NET_UID", uid))
      Lib_FileGetEnv("NET_PWD", pwd);
  }

  LOG_FXN_MSG(INF,NONE, "wait signal...");
  int lvl;
  TIMER_DO(USER_TIMER, NETWORK_WAIT_SIGNAL_TIMEOUT)
  {
    int res = network_SignalLevel(&lvl);
    if (res) break;
  } TIMER_WHILE(lvl == netSignal_No);
  if (lvl == netSignal_No)
    LOG_FXN_MSG(WRN,NONE, "no signal");
  else
    LOG_FXN_MSG(INF,NONE, "signal level = %s", network_SignalLevelStr(lvl));

  res = network_setDialData(dialNum, uid, pwd);
  CHECK_AND_RETURN(res)
  res = network_sys_dial(NETWORK_DIAL_TIMEOUT);
  CHECK_AND_RETURN(res)

  BYTE getipEnv[16];
  getipEnv[0] = '\0';
  if (!Lib_FileGetEnv("GETMYIP", getipEnv))
  {
    if (strcmp(getipEnv, "yes") == 0)
      get_ip();
  }

  LOG_FXN(INF,OK);
  return 0;
}

void close_Net()
{
  LOG_FXN(INF,START);
  SYS_INFO("NETWORK", "close");

  int res = network_sys_close();

  SYS_INFO_RES(res);
  if (res)
    LOG_FXN(INF,FAIL);
  else
    LOG_FXN(INF,OK);
}

void init_Terminal()
{
  LOG_FXN(INF,START);
  SYS_INFO("ELpay Terminal", "init");

  BYTE envBuf[256];

  envBuf[0] = '\0';
  if (Lib_FileGetEnv("EP_IP", envBuf) == FILE_NO_MATCH_RECORD)
  {
    snprintf(envBuf, sizeof(envBuf),"m100.el-pay.ru");
    Lib_FilePutEnv("EP_IP", envBuf);
  }
  init_ELpayTerminal(&Terminal, 1, envBuf, "elpay_oplist", "elpay_prodlist");

  if (!Lib_FileGetEnv("EP_TID", envBuf))
  {
    log_printf(INF, "ELpayTerm: id = %s", envBuf);

    isTermID = 1;
    ELpayTerminal_setId(&Terminal, envBuf);

    if (!Lib_FileGetEnv("EP_SK", envBuf))
    {
      log_printf(INF, "ELpayTerm: SK = %s", envBuf);
      ELpayTerminal_setSignKey_b64(&Terminal, envBuf);
    }
  }

  SYS_INFO_RES(0);
  LOG_FXN(INF,OK);
}

void close_Terminal()
{
  LOG_FXN(INF,START);
  SYS_INFO("ELpay Terminal", "close");

  close_ELpayTerminal(&Terminal);

  SYS_INFO_RES(0);
  LOG_FXN(INF,OK);
}

#define APP_PUT_ENV(name, val) \
  if (Lib_FilePutEnv(name, val)) \
  { \
    log_printf(INF, "APP: set env \"%s\" failure", name); \
    return; \
  }

static void terminal_changed_handler(void * This, char * id, char * key)
{
  APP_PUT_ENV("EP_TID", id)

  ELpaySignEnc_t sk_b64;
  elpay_base64_encode(key, ELPAY_JSON_SIGNSIZE, sk_b64, ELPAY_JSON_SIGNenc_BUFSIZE);
  APP_PUT_ENV("EP_SK", sk_b64)

  LOG(INF, "Terminal changed: id = %s, key = %s", id, sk_b64);
}
static void authdata_changed_handler(void * This, char * lgn, char * pwd)
{
  APP_PUT_ENV("EP_LGN", lgn)
  APP_PUT_ENV("EP_PWD", pwd)

  isAuth = 1;

  LOG(INF, "AuthData changed: login = %s, password = %s", lgn, pwd);
}
void init_GUI()
{
  LOG_FXN(INF,START);
  SYS_INFO("GUI", "prepare");

  init_MainWidget(&MainWidget, &Terminal);
  MainWidget.terminal_changed = (mainwidget_terminal_changed_f) terminal_changed_handler;
  MainWidget.authdata_changed = (mainwidget_authdata_changed_f) authdata_changed_handler;

  SYS_INFO_RES(0);
  LOG_FXN(INF,OK);
}

void prepare_GUI()
{
  gui_setCurrentObject(&MainWidget.obj);

  BYTE login[32], password[32];

  if (!Lib_FileGetEnv("EP_LGN", login))
  {
    LineEdit_setText(&MainWidget.formAuth.login, login);

    if (!Lib_FileGetEnv("EP_PWD", password))
    {
      isAuthInfo = 1;
      LineEdit_setText(&MainWidget.formAuth.password, password);
    }
  }
  if (isTermID)
  {
    if (isAuthInfo)
      isAuth = MainWidget_goAuthorization(&MainWidget, login, password);
  }
  else
    MainWidget_setMode(&MainWidget, mwReg);
}

int app_exec()
{
  prepare_GUI();
  return system_exec();
}

void app_quit()
{
  system_exit();
}
