/*
 * app.h
 *
 *  Created on: 14 апр. 2017 г.
 *      Author: Svetozar
 */

#ifndef APP_H_
#define APP_H_

#ifdef __cplusplus
extern "C"{
#endif

int init_Sys();

int init_Net();
void close_Net();

void init_Terminal();
void close_Terminal();

void init_GUI();

int app_exec();
void app_quit();

#ifdef __cplusplus
}
#endif

#endif /* APP_H_ */
