#ifndef LOGS_CONFIG_H
#define LOGS_CONFIG_H

#if defined(APP_TARGET)
#define SYSGUI_LOG_LEVEL WRN
#define GUI_LOG_LEVEL WRN
#elif defined(APP_TEST_GUI)
#define SYSGUI_LOG_LEVEL WRN
#define GUI_LOG_LEVEL DBG
#else
#warning unknown target
#define SYSGUI_LOG_LEVEL WRN
#define GUI_LOG_LEVEL WRN
#endif

#endif // LOGS_CONFIG_H
