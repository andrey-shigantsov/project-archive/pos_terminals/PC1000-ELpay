/*
 * port_json.h
 *
 *  Created on: 5 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef PORTS_JSON_PORT_H_
#define PORTS_JSON_PORT_H_

#include "ELpay/json.h"

#ifdef __cplusplus
extern "C"{
#endif

void init_port_json();

#ifdef __cplusplus
}
#endif

#endif /* PORTS_JSON_PORT_H_ */
