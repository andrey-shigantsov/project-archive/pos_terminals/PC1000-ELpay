/* 
 * File:   json_ELpay_port.h
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifndef JSON_ELPAY_PORT_H
#define JSON_ELPAY_PORT_H

#include "SYSTEM/output.h"

#ifdef __cplusplus
extern "C"
{
#endif

uint16_t elpay_crc16(const char * bytes, size_t count);

#define ELPAY_DES_ENC 1
#define ELPAY_DES_DEC 0
int elpay_des(int mode, char * key, char * bytes, size_t count, char * res, size_t res_size);
#define elpay_des_encrypt(key,bytes,count,res,res_size) elpay_des(ELPAY_DES_ENC, key,bytes,count,res,res_size)
#define elpay_des_decrypt(key,bytes,count,res,res_size) elpay_des(ELPAY_DES_DEC, key,bytes,count,res,res_size)

int elpay_base64_encsize(size_t count);
int elpay_base64_decsize(size_t count);
int elpay_base64_encode(const char * bytes, size_t count, char * res, size_t res_size);
int elpay_base64_decode(const char * bytes, size_t count, char * res, size_t res_size);

int elpay_http_post(const char * data, size_t len, const char * userHeader, char * postBuf, size_t postBufSize);

bool elpay_http_check(const char * buf, size_t len);

typedef enum
{
  elpayHTTP_con_Close,
  elpayHTTP_con_KeepAlive,

  elpayHTTP_con_Unknown = -1
} elpayHTTP_Connection_t;
elpayHTTP_Connection_t elpay_http_connection(const char * buf, size_t len);

typedef enum
{
  elpayHTTP_trsfr_Len,
  elpayHTTP_trsfr_Chunk,

  elpayHTTP_trsfr_Unknown = -1
} elpayHTTP_Transfer_t;
elpayHTTP_Transfer_t elpay_http_transfer_type(const char * buf, size_t len);

long elpay_http_content_len(const char * buf, size_t len);
int elpay_http_content_idx(const char * buf, size_t len);

#ifdef __cplusplus
}
#endif

#endif /* JSON_ELPAY_PORT_H */

