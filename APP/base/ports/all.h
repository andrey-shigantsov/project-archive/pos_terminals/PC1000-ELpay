/*
 * all.h
 *
 *  Created on: 5 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef PORTS_ALL_H_
#define PORTS_ALL_H_

#include "ports/base.h"

#ifdef __cplusplus
extern "C"{
#endif

void init_ports();

#ifdef __cplusplus
}
#endif

#endif /* PORTS_ALL_H_ */
