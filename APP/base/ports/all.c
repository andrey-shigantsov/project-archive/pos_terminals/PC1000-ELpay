/*
 * all.c
 *
 *  Created on: 5 мар. 2017 г.
 *      Author: Svetozar
 */

#include "all.h"

#include "APP/base/ports/include/json_port.h"

void init_ports()
{
  init_ports_base();

  init_port_json();
}
