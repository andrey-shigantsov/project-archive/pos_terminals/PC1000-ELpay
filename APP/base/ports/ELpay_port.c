/* 
 * File:   ELpay_port.cpp
 */

#include "ELpay_port.h"

#define LOG_HEADER "PORT/ELpay"
#define LOG_LEVEL DBG
#include "SYSTEM/logs.h"

#include "codecs/crc16.h"

uint16_t elpay_crc16(const char * bytes, size_t count)
{
  return crc_16(bytes, count);
}

#include "SYSTEM/base.h"
#include <memory.h>

int elpay_des(int mode, char * key, char * bytes, size_t count, char * res, size_t res_size)
{
  if (res_size < count + 1) return -1;

  size_t counter = 0;
  while (counter < count)
  {
    size_t n = count - counter;
    if (n < 8)
    {
      char inBuf[8];
      memcpy(inBuf, &bytes[counter], n);
      memset(inBuf, 0, 8 - n);

      size_t m = res_size - counter;
      if (m < 8)
      {
        char outBuf[8];
        Lib_Des(inBuf, outBuf, key, mode);
        memcpy(&res[counter], outBuf, n);
      }
      else
        Lib_Des(inBuf, &res[counter], key, mode);
    }
    else
      Lib_Des(&bytes[counter], &res[counter], key, mode);

    counter += 8;
  }
  res[counter] = '\0';
  return 0;
}

#include "codecs/b64/cencode.h"
#include "codecs/b64/cdecode.h"

int elpay_base64_encsize(size_t count)
{
  size_t lastBlkCount = count % 3;
  size_t blkCount = count/3 + (lastBlkCount?1:0);

  return blkCount*4;
}
int elpay_base64_encode(const char * bytes, size_t count, char * res, size_t res_size)
{
  size_t lastBlkCount = count % 3;
  size_t blkCount = count/3 + (lastBlkCount?1:0);

  if (res_size < blkCount*4 + 1)
  {
    LOG(WRN, "b64: encode failure: res size incorrect");
    return -1;
  }

  base64_encodestate state;
  base64_init_encodestate (&state);

  int len = base64_encode_block(bytes, count, res, &state);
  len += base64_encode_blockend(&res[len], &state);
  res[len] = '\0';
  return 0;
}

int elpay_base64_decsize(size_t count)
{
  size_t lastBlkCount = count % 4;
  size_t blkCount = count / 4;

  return blkCount*3;
}
int elpay_base64_decode(const char * bytes, size_t count, char * res, size_t res_size)
{
  size_t lastBlkCount = count % 4;
  size_t blkCount = count / 4;

  if (lastBlkCount)
  {
    LOG(WRN, "b64: decode failure: blocks count incorrect");
    return -1;
  }

  base64_decodestate state;
  base64_init_decodestate(&state);

  int len = base64_decode_block(bytes, count, res, &state);
//  size_t i;
//  for (i = 0; i < res_size - 1; ++i)
//    len += base64_decode_block(&bytes[i], 1, &res[i], &state);
  res[len] = '\0';
  return 0;
}

//------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>

#define ELPAY_SERVER_HOST "m100.el-pay.ru"
#define ELPAY_SERVER_URL_HTTP "http://" ELPAY_SERVER_HOST "/"
#define ELPAY_SERVER_URL ELPAY_SERVER_URL_HTTP

#define ELPAY_HTTP_POST "POST " ELPAY_SERVER_URL " HTTP/1.1\r\n"
#define ELPAY_HTTP_JSON_HEADER \
  "Accept: application/jsonrequest\r\n" \
  "Host: " ELPAY_SERVER_HOST "\r\n" \
  "Content-Encoding: identity\r\n" \
  "Content-Type: application/jsonrequest\r\n" \
  "Connection: keep-alive\r\n"
  
#define ELPAY_REG_HTTP_HEADER(uAgent,MayBeCompress) \
  "User-Agent: "uAgent"\r\n" \
  "X-ELpay-Compress: "MayBeCompress"\r\n"

int elpay_http_post(const char * data, size_t len, const char *  userHeader,
    char * postBuf, size_t postBufSize)
{
  snprintf(postBuf, postBufSize, "%s%sContent-Length: %d\r\n\r\n%s",
    ELPAY_HTTP_POST ELPAY_HTTP_JSON_HEADER ELPAY_REG_HTTP_HEADER("Android app","0"),/*"old uAgent M100"*/
    userHeader, len, data);
  return strlen(postBuf);
}


static int strncaseidx(const char * string, size_t len, const char * substring, size_t sublen)
{
  int i;
  for (i = 0; i < len - sublen; ++i)
  {
    if (strncasecmp(&string[i], substring, sublen) == 0)
      return i;
  }
  return -1;
}

static int strcaseidx(const char * string, const char * substring)
{
  size_t len = strlen(string);
  size_t sublen = strlen(substring);

  return strncaseidx(string, len, substring, sublen);
}

int elpay_http_read_header_data_idx(const char * buf, size_t len, const char * header_name)
{
  int idx = strcaseidx(buf, header_name);
  if (idx < 0)
    return -1;

  char * p = strstr(&buf[idx + strlen(header_name)], ":");
  if (!p)
    return -2;
  idx = p - buf;

  return idx + 1;
}
#define HTTP_IDX_IS_OK(idx,len) ((idx < len)||(idx >= 0))
#define HTTP_IDX_IS_ERR(idx,len) ((idx >= len)||(idx < 0))

bool elpay_http_check(const char * buf, size_t len)
{
  char * p = strstr(buf, "HTTP/1.");
  if (!p) return false;
  p = strstr(p, "200 OK\r\n");
  if (!p) return false;
  return true;
}

elpayHTTP_Connection_t elpay_http_connection(const char * buf, size_t len)
{
  int idx = elpay_http_read_header_data_idx(buf,len,"Connection");
  if (HTTP_IDX_IS_ERR(idx,len))
    return elpayHTTP_con_Unknown;

  int endidx = strcaseidx(&buf[idx], "\r\n");
  int validx, vallen = endidx - idx;

  validx = strcaseidx(&buf[idx], "close");
  if (HTTP_IDX_IS_OK(validx,vallen))
    return elpayHTTP_con_Close;

  validx = strcaseidx(&buf[idx], "keep-alive");
  if (HTTP_IDX_IS_OK(validx,vallen))
    return elpayHTTP_con_KeepAlive;

  return elpayHTTP_con_Unknown;
}

elpayHTTP_Transfer_t elpay_http_transfer_type(const char * buf, size_t len)
{
  int idx;

  idx = elpay_http_read_header_data_idx(buf,len,"Content-Length");
  if (HTTP_IDX_IS_OK(idx,len))
    return elpayHTTP_trsfr_Len;

  idx = elpay_http_read_header_data_idx(buf,len,"Transfer-Encoding");
  if (HTTP_IDX_IS_OK(idx,len))
  {
    int endidx = strcaseidx(&buf[idx], "\r\n");
    int vallen = endidx - idx;
    int validx = strcaseidx(&buf[idx], "chunked");
    if (HTTP_IDX_IS_OK(validx,vallen))
      return elpayHTTP_trsfr_Chunk;
  }

  return elpayHTTP_trsfr_Unknown;
}

long elpay_http_content_len(const char * buf, size_t len)
{
  int idx = elpay_http_read_header_data_idx(buf,len,"Content-Length");
  if (HTTP_IDX_IS_ERR(idx,len))
    return -1;
  return atol(&buf[idx]);
}

int elpay_http_content_idx(const char * buf, size_t len)
{
  char * p = strstr(buf, "HTTP/1.");
  p = strstr(p, "\r\n\r\n");
  if (!p) return -1;
  int idx = p - buf;
  idx += 4;
  if (idx >= len)
    return -2;
  return idx;
}
