/*
 * elpay.c
 *
 *  Created on: 9 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "TEST/ELPAY"
#include "tests/base.h"

#include "ELpay/protocol.h"

DECLARE_APP_MSG(LOG_HEADER, APP_VER_STR("0","0"))

int init_test()
{
  return 0;
}

void free_test()
{
//  LOG_FXN(INF,START);
//  LOG_FXN(INF,OK);
}

static int test_base()
{
  LOG(INF, "BASE64...");
  {
    ELpayKey_t key0 = "12345678", key;
    ELpayKeyEnc_t keyEnc0 = "MTIzNDU2Nzg=", keyEnc;

    elpay_base64_encode(key0, ELPAY_JSON_KEYSIZE, keyEnc, ELPAY_JSON_KEYenc_BUFSIZE);
    LOG(INF, "encoded key = %s", keyEnc);
    if (strncmp(keyEnc, keyEnc0, ELPAY_JSON_KEYenc_SIZE)) LOG(WRN, "encode failure");

    elpay_base64_decode(keyEnc, ELPAY_JSON_KEYenc_SIZE, key, ELPAY_JSON_KEYenc_BUFSIZE);
    LOG(INF, "decoded key = %s", key);
    if (strncmp(key, key0, ELPAY_JSON_KEYenc_SIZE)) LOG(WRN, "decode failure");
  }
  LOG(INF, "BASE64 finished");

//  LOG(INF, "DES...");
//  {
//
//  }
//  LOG(INF, "DES finished");
  return 0;
}

static int test_reg()
{
  int res;
  LOG(INF, "REGISTRATION...");
  {
    char errBuf[2048];
    char buf[] = "{\"mk\":\"68wKQTMefw8=\",\"sk\":\"A71w5UvXw/A=\",\"enccv\":\"OFRD1TyL7t0=\",\"term\":\"116024\"}";
    LOG(INF, "response:\n%s", buf);

    ELpayTermId_t id;
    ELpayKey_t signkey;
    res = elpay_reg_resp_parse(buf, "73228513", &id, &signkey, errBuf, sizeof(errBuf));
    if (res)
      LOG(WRN, "failure: %s", errBuf);
    else
    {
      LOG(INF, "ID = %s", id);

      ELpayKeyEnc_t signkeyEnc;
      elpay_base64_encode(signkey, ELPAY_JSON_KEYSIZE, signkeyEnc, ELPAY_JSON_KEYenc_BUFSIZE);
      LOG(INF, "Sign Key = %s", signkeyEnc);
    }
  }
  LOG(INF, "REGISTRATION finished");
  return res;
}

static int test_auth()
{
  int res;
  LOG(INF, "AUTHORIZATION...");
  {
    char errBuf[2048];
    char buf[] = "{\"session\":\"6E24451371C307A3CB4B38FF799FB64B\",\"version\":\"1.0.0\"}";
    LOG(INF, "response:\n%s", buf);

    ELpaySessionId_t session;
    res = elpay_auth_resp_parse(buf, &session, errBuf, sizeof(errBuf));
    if (res)
      LOG(WRN, "failure: %s", errBuf);
    else
    {
      LOG(INF, "session = %s", session);
    }
  }
  LOG(INF, "AUTHORIZATION finished");
  return res;
}

static int test_prodlist()
{
  int res;
  LOG(INF, "PRODUCTS LIST...");
  {
    char errBuf[2048];

    #include "elpay-prodlist.hh"
//    LOG(INF, "response:\n%s", prodlist);

    ELpayProdList_t list;
    res = elpay_prodlist_resp_parse(prodlist, &list, errBuf, sizeof(errBuf));
    if (res)
      LOG(WRN, "failure: %s", errBuf);
    else
    {
      for (size_t i = 0; i < list.ServicesCount; ++i)
        LOG(INF, "Service #%d \"%s\"", list.Services[i].id, list.Services[i].name[elpay_RU]);

      for (size_t i = 0; i < list.ProductsCount; ++i)
      {
        LOG(INF, "Product #%d \"%s\"", list.Products[i].id, list.Products[i].name[elpay_RU]);
        for (size_t j = 0; j < list.Products[i].fieldsCount; ++j)
          LOG(INF, "Product field #%d \"%s\": mask = \"%s\"",
            list.Products[i].fields[j].id, list.Products[i].fields[j].name[elpay_RU], list.Products[i].fields[j].mask);
      }
    }
  }
  LOG(INF, "PRODUCTS LIST finished");
  return res;
}

static int test_create_op()
{
  int res;
  LOG(INF, "CREATE OPERATION...");
  {
    char reqBuf[1024];
    ELpayCreateOpReq_t req;
    req.base.termId = "112233";
    req.base.session = "12345678";
    req.base.app_ver = "1.0";
    req.pid = 1002;
    req.act = 3456;
    req.msisdn = "0970002000";
    req.nominal = 123.05;
    req.commission = 0.05;
    res = elpay_create_operation_req_gen(&req, reqBuf, sizeof(reqBuf));
    if (res <= 0)
      LOG(WRN, "request failure");
    else
      LOG(INF, "request:\n%s", reqBuf);

    char errBuf[2048];
    char buf[] = "{\"error\":909,\"error-text\":\"\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043d\\u0435 \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442\"}";
    LOG(INF, "response:\n%s", buf);

    ELpayTransactionId_t tranId;
    res = elpay_create_operation_resp_parse(buf, &tranId, errBuf, sizeof(errBuf));
    if (res)
      LOG(WRN, "response failure: %s", errBuf);
    else
    {
      LOG(INF, "tranId = %s", tranId);
    }
  }
  LOG(INF, "CREATE OPERATION finished");
  return res;
}

typedef enum {elpayTestBase, elpayTestReg, elpayTestAuth, elpayTestProdList, elpayTestCreateOp} ELpayTest_t;
static ELpayTest_t test = elpayTestCreateOp;

typedef int (*test_fxn)();
static test_fxn tests[] = {test_base, test_reg, test_auth, test_prodlist, test_create_op};

int test_exec()
{
  LOG_FXN(INF,START);
  int res = tests[test]();
  LOG_FXN(INF,OK);
  return res;
}
