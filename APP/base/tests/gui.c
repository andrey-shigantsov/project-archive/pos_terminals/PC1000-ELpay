/*
 * gui.c
 *
 *  Created on: 17 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "TEST/GUI"
#include "tests/base.h"

#include "SYSTEM/control.h"
#include "SYSTEM/GUI/System.h"

#include "GUI/MainWidget.h"
#include "SERVICES/ELpay/Terminal.h"

DECLARE_APP_MSG(LOG_HEADER, APP_VER_STR("0","0"))

MainWidget_t MainWidget;
ELpayTerminal_t Terminal;

typedef enum {guiTest_Base, guiTest_All, guiTest_Font, guiTest_ServicesMenu} guiTest_t;
guiTest_t test = guiTest_ServicesMenu;

int init_test()
{
  init_system();

  init_ELpayTerminal(&Terminal, 1, "noaddr", "elpayT_oplist", "elpayT_prodlist");
  init_MainWidget(&MainWidget, &Terminal);
  return 0;
}

void free_test()
{
  close_ELpayTerminal(&Terminal);
}

#define RUS_TEST_MSG "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" "\n" "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

void showNativeFontMsg()
{
  keyboard_flush();

  StatusWidget_t * This = &MainWidget.StatusWidget;

  StatusWidget_setHeader(This, "ТЕСТ НАТИВНОГО ШРИФТА");

  int i = 0;
  for (int idx = 0xc0; idx <= 0xff; ++idx, ++i)
    This->Msg[i] = idx;
  This->Msg[i] = '\0';

  object_draw(&This->obj);
}

static void font_test()
{
  {
    char buf[256];
    int i = 0;
    for (int idx = 0xc0; idx <= 0xff; ++idx, ++i)
      buf[i] = idx;
    buf[i] = '\0';
    MainWidget_showNativeMsg(&MainWidget, "ТЕСТ ШРИФТА", buf);
  }
//  MainWidget_printMsg(&MainWidget, "ТЕСТ ШРИФТА", RUS_TEST_MSG);
//  MainWidget_showMsg(&MainWidget, "ТЕСТ ШРИФТА", RUS_TEST_MSG);
}

int test_exec()
{
  LOG_FXN(INF,START);
  gui_setCurrentObject(&MainWidget.obj);
  switch(test)
  {
  case guiTest_All:
    MainWidget_setMode(&MainWidget, mwReg);
    font_test();
    break;

  case guiTest_Base:
    MainWidget_setMode(&MainWidget, mwReg);
    break;

  case guiTest_Font:
    if(MainWidget_loadProdList(&MainWidget))
      MainWidget_setMode(&MainWidget, mwSrvcs);
    else
      MainWidget_updateProductsList(&MainWidget);
    font_test();
    break;

  case guiTest_ServicesMenu:
    if(MainWidget_loadProdList(&MainWidget)) break;
    MainWidget_updateProductsList(&MainWidget);
    break;

  default:
    break;
  }
  int res = system_exec();
  LOG_FXN(INF,OK);
  return res;
}
