set(TEST_GUI Test-GUI.PC1000)

set(TEST_GUI_HEADERS ${TESTS_COMMON_HEADERS}
  ${MAIN_DIR}/SERVICES/ELpay/Terminal.h
  ${MAIN_DIR}/SERVICES/ELpay/OperationList.h
  ${MAIN_DIR}/SERVICES/ELpay/internal/operationStatus.h

  ${MAIN_DIR}/APP/base/ports/include/json_port.h
  ${MAIN_DIR}/APP/base/ports/include/ELpay_port.h

  ${LIB_CODECS_HEADERS}
  ${LIB_YAJL_HEADERS}
  ${LIB_ELPAY_HEADERS}

  ${GUI_HEADERS}
)

set(TEST_GUI_SOURCES ${TESTS_COMMON_SOURCES}
  ${CMAKE_CURRENT_LIST_DIR}/gui.c 
  ${CMAKE_CURRENT_LIST_DIR}/dummies/ELpayTerminal.c
  ${MAIN_DIR}/SERVICES/ELpay/internal/operationStatus.c
  ${MAIN_DIR}/SERVICES/ELpay/internal/prodlistFileOperation.c
  ${MAIN_DIR}/SERVICES/ELpay/OperationList.c

  ${MAIN_DIR}/APP/base/ports/json_port.c
  ${MAIN_DIR}/APP/base/ports/ELpay_port.c

  ${LIB_CODECS_SOURCES}
  ${LIB_YAJL_SOURCES}
  ${LIB_ELPAY_SOURCES}
  
  ${GUI_SOURCES}
)
