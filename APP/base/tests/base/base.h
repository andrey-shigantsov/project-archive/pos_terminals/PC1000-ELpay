/*
 * base.h
 *
 *  Created on: 4 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef TESTS_BASE_H_
#define TESTS_BASE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

#include "SYSTEM/output.h"
#include "SYSTEM/control.h"

#ifdef __cplusplus
extern "C"{
#endif

void init_test_base();

extern int init_test();
extern int test_exec();
extern void free_test();

#ifdef __cplusplus
}
#endif

#endif /* TESTS_BASE_H_ */
