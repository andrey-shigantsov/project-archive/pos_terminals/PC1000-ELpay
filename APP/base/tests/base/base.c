
#include "tests/base.h"
#include "ports/base.h"

void init_test_base()
{
  Lib_AppInit();
  Lib_LcdCls();
  Lib_LcdSetFont(8,16,0);

  init_logs("test.log");

  log_printf(INF, "\n===={ test entry point }====\n");

  init_ports_base();
}
