/*
 * pool.c
 *
 *  Created on: 6 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "TEST/GENERIC"

#include "tests/base.h"
#include "ports/exceptions.h"

DECLARE_APP_MSG(LOG_HEADER, APP_VER_STR("0","0"))

int init_test()
{
//  LOG_FXN(INF,START);
//  LOG_FXN(INF,OK);
  return 0;
}

void free_test()
{
//  LOG_FXN(INF,START);
//  LOG_FXN(INF,OK);
}

int test_exec()
{
  LOG_FXN(INF,START);

  TRY
  {
    LOG(DBG,"try");
    //    *((uint8_t * )0x30700000) = 0;
  }
  CATCH(int ec)
  {
    LOG(DBG,"catch: %d", ec);
  }
  TRY_END

  LOG_FXN(INF,OK);
  return 0;
}
