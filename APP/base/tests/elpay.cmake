set(TEST_ELPAY Test-ELpay.PC1000)

set(TEST_ELPAY_HEADERS ${TESTS_COMMON_HEADERS}
  ${TEST_HEADERS}
  ${CMAKE_CURRENT_LIST_DIR}/elpay-prodlist.hh 
  ${MAIN_DIR}/APP/base/ports/include/json_port.h 
  ${MAIN_DIR}/APP/base/ports/include/ELpay_port.h
  
  ${LIB_CODECS_HEADERS}
  ${LIB_YAJL_HEADERS}
  ${LIB_ELPAY_HEADERS}
)
set(TEST_ELPAY_SOURCES ${TESTS_COMMON_SOURCES}
  ${CMAKE_CURRENT_LIST_DIR}/elpay.c
  ${MAIN_DIR}/APP/base/ports/json_port.c
  ${MAIN_DIR}/APP/base/ports/ELpay_port.c
  
  ${LIB_CODECS_SOURCES}
  ${LIB_YAJL_SOURCES}
  ${LIB_ELPAY_SOURCES}
)
