/*
 * ELpayTerminal.c
 *
 *  Created on: 17 мар. 2017 г.
 *      Author: Svetozar
 */

 #define LOG_HEADER "ELpayTermDummy"
 #define LOG_LEVEL DBG
 #include "SYSTEM/logs.h"

#include "SERVICES/ELpay/Terminal.h"
#include <string.h>

#include "LIBS/ELpay/protocol.h"

void init_ELpayTerminal(ELpayTerminal_t * This, int SocketNum, const char * addr, const char * oplistFile, const char* ProdListFile)
{
  LOG_FXN(DBG,START);
  (void)SocketNum;
  (void)addr;
  (void)oplistFile;

  ELpayTerminal_setId(This, "115200");

  strncpy(This->session, "x-session", ELPAY_JSON_SESSION_ID_BUFSIZE);

  strncpy(This->ProdListFileName, ProdListFile, sizeof(This->ProdListFileName));

  init_ELpay_OperationList(&This->opList, oplistFile);

  if (!ELpay_OperationList_ItemsCount(&This->opList))
    for (int i = 0; i < ELPAY_OPLIST_MAX_ITEMS_COUNT; ++i)
      ELpay_OperationList_append(&This->opList,"z-session",i+1,true);

  This->ErrBufSize = sizeof(This->ErrBuf);
  This->ErrBuf[0] = '\0';
  LOG_FXN(DBG,OK);
}

void close_ELpayTerminal(ELpayTerminal_t * This)
{
  LOG_FXN(DBG,START);
  (void)This;
  LOG_FXN(DBG,OK);
}

void ELpayTerminal_setId(ELpayTerminal_t * This, const char * id)
{
  strncpy(This->id, id, ELPAY_JSON_TERM_ID_BUFSIZE);
}

int ELpayTerminal_setSignKey_b64(ELpayTerminal_t * This, const char * sign_key)
{
  (void)This;
  (void)sign_key;
  return 0;
}

ELpayOperationStatus_t ELpayTerminal_registration(ELpayTerminal_t * This, const char * code)
{
  (void)This;
  (void)code;
  return 0;
}

ELpayOperationStatus_t ELpayTerminal_authorization(ELpayTerminal_t* This, const char * login, const char * password)
{
  (void)This;
  (void)login;
  (void)password;
  return 0;
}

#define AUTO_SNPRINTF_FOR_STATBUF(buf,...) \
  snprintf(buf, sizeof(buf), __VA_ARGS__)

ELpayOperationStatus_t ELpayTerminal_get_balance(ELpayTerminal_t* This, ELpayBalance_t * balance)
{
  (void)This;

  AUTO_SNPRINTF_FOR_STATBUF(balance->date, "2017.03.30 09:59");
  AUTO_SNPRINTF_FOR_STATBUF(balance->net, "100.00 RUR");
  AUTO_SNPRINTF_FOR_STATBUF(balance->client, "1000.00 RUR");
  AUTO_SNPRINTF_FOR_STATBUF(balance->credit, "-900.00 RUR");
  AUTO_SNPRINTF_FOR_STATBUF(balance->limit.day, "100.00 RUR");
  AUTO_SNPRINTF_FOR_STATBUF(balance->limit.day24, "200.00 RUR");
  AUTO_SNPRINTF_FOR_STATBUF(balance->limit.operation, "10.00 RUR");

  return 0;
}

ELpayOperationStatus_t ELpayTerminal_get_products_list(ELpayTerminal_t* This, ELpayProdList_t * list)
{
  LOG_FXN(DBG,START);

  (void)This;

  int prodToServ = 8;
  list->ServicesCount = 8;
  list->ProductsCount = prodToServ * list->ServicesCount;

  for (size_t i = 0; i < list->ServicesCount; ++i)
  {
    list->Services[i].id = i;
    list->Services[i].sort_order = i;
    snprintf(list->Services[i].name[elpay_RU], ELPAY_NAME_BUFSIZE, "сервис-%03d", i);
    snprintf(list->Services[i].name[elpay_UA], ELPAY_NAME_BUFSIZE, "srv%03d-ua", i);
  }

  for (size_t i = 0; i < list->ProductsCount; ++i)
  {
    list->Products[i].id = i;
    list->Products[i].sort_order = i;
    list->Products[i].sid = i/prodToServ;
    snprintf(list->Products[i].name[elpay_RU], ELPAY_NAME_BUFSIZE, "продукт-%03d: длинное имя, весьма длинное", i);
    snprintf(list->Products[i].name[elpay_UA], ELPAY_NAME_BUFSIZE, "product-%03d-ua: long name, very long", i);
    snprintf(list->Products[i].name_short[elpay_RU], ELPAY_NAME_BUFSIZE, "прд%03d", i);
    snprintf(list->Products[i].name_short[elpay_UA], ELPAY_NAME_BUFSIZE, "prd%03d-ua", i);

    list->Products[i].fieldsCount = 2;
    for(size_t j = 0; j < list->Products[i].fieldsCount; ++j)
    {
      list->Products[i].fields[j].id = j;
      snprintf(list->Products[i].fields[j].name[elpay_RU], ELPAY_NAME_BUFSIZE, "поле-%03d", j);
      snprintf(list->Products[i].fields[j].name[elpay_UA], ELPAY_NAME_BUFSIZE, "field-%03d-ua", j);
      strncpy(list->Products[i].fields[j].mask, "(___)___-__-__", ELPAY_MASK_BUFSIZE);
      list->Products[i].fields[j].charMin = 9;
      list->Products[i].fields[j].charMax = 13;
    }
    memset(&list->Products[i].commission,0,sizeof(ELpayProductCommission_t));
  }

  LOG_FXN(DBG,OK);
  return 0;
}

ELpayOperationStatus_t ELpayTerminal_get_receipt_template(ELpayTerminal_t* This, const char * name)
{
  (void)This;
  (void)name;
  return 0;
}

ELpayOperationStatus_t ELpayTerminal_create_operation(ELpayTerminal_t* This, ELpayProductData_t * ProdData)
{
  (void)This;
  LOG_FXN_MSG(DBG,NONE,
    "\n - pid = %d"
    "\n - fid = %d"
    "\n - val = \"%s\""
    "\n - nominal = %d"
    "\n - commission = %d",
    ProdData->pid,
    ProdData->fields[0].id, ProdData->fields[0].value,
    (int)ProdData->nominal, (int)ProdData->commission);

  This->tran.opListIdx = ELpay_OperationList_append(&This->opList,This->session,ProdData->nominal,false);
  ELpay_OperationList_update_file(&This->opList);

  return 0;
}

static void update_operation_status(ELpayTerminal_t* This)
{
  if(This->tran.state.id == elpay_tnst_Successfull)
  {
    ELpay_OperationListItem_t * item = ELpay_OperationList_Item(&This->opList, This->tran.opListIdx);
    assert(item);
    item->status = true;
    ELpay_OperationList_update_file(&This->opList);
  }
}

ELpayOperationStatus_t ELpayTerminal_confirm_operation(ELpayTerminal_t* This)
{
  This->tran.state.id = elpay_tnst_Processing;
  This->tran.state.errMsg[0] = '\0';

  update_operation_status(This);
  return 0;
}

ELpayOperationStatus_t ELpayTerminal_get_operation_status(ELpayTerminal_t * This)
{
  This->tran.state.id = elpay_tnst_Successfull;
  This->tran.state.errMsg[0] = '\0';

  update_operation_status(This);
  return 0;
}

void ELpayTerminal_cancel_operation(ELpayTerminal_t * This)
{
  (void)This;
}

static const char reportSales[] = "<!DOCTYPE html><html><head><title>\u041e\u0442\u0447\u0435\u0442</title><style type=\"text/css\">html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}body{font-size:25pt;font-family: Helvetica,arial,freesans,clean,sans-serif;padding: 0.5em;}table{width: 100%;border-collapse:collapse;}td:first-child{color:red;text-align: left;color: #8E8E8E;font-weight: normal;}td{text-align: right;font-weight: bold;border-bottom: 1px dotted #8E8E8E;}h3{margin-top: 1em;margin-bottom: 0.5em;color: #4183C4;font-size: 1.3em;white-space:nowrap;}</style></head><body><table><center><h3>\u041e\u0442\u0447\u0451\u0442 \u043f\u043e \u043f\u0440\u043e\u0434\u0430\u0436\u0430\u043c</h3></center><tr><td>\u0417\u0430 \u043f\u0435\u0440\u0438\u043e\u0434</td><td>01.04.2018 - 14.05.2018</td></tr><tr><td>\u0421\u0444\u043e\u0440\u043c\u0438\u0440\u043e\u0432\u0430\u043d</td><td>14.05.2018 12:43:28</td></tr></table><h3>\u041f\u043e \u0442\u0435\u0440\u043c\u0438\u043d\u0430\u043b\u0443 #117266</h3><table><tr><td>\u041a\u043e\u043b-\u0432\u043e \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0439</td><td>14</td></tr><tr><td>\u041d\u0430 \u0441\u0447\u0451\u0442</td><td>0,17</td></tr><tr><td>\u041a\u043e\u043c\u0438\u0441\u0441\u0438\u044f</td><td>-0,02</td></tr><tr><td>\u041e\u0442 \u043a\u043b\u0438\u0435\u043d\u0442\u0430</td><td>0,15</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441\u044b</td><td>0,00</td></tr></table>";
ELpayOperationStatus_t ELpayTerminal_get_report_sales(ELpayTerminal_t* This,
                                                      char * date1, char * date2, bool isDetailed,
                                                      char* text, size_t size)
{
  This->tran.state.id = elpay_tnst_Successfull;
  This->tran.state.errMsg[0] = '\0';

  strncpy(text,reportSales,size);

  update_operation_status(This);

  return 0;
}
static const char reportFinancial[] = "<!DOCTYPE html><html><head><title>\u041e\u0442\u0447\u0435\u0442</title><style type=\"text/css\">html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}body{font-size:25pt;font-family: Helvetica,arial,freesans,clean,sans-serif;padding: 0.5em;}table{width: 100%;border-collapse:collapse;}td:first-child{color:red;text-align: left;color: #8E8E8E;font-weight: normal;}td{text-align: right;font-weight: bold;border-bottom: 1px dotted #8E8E8E;}h3{margin-top: 1em;margin-bottom: 0.5em;color: #4183C4;font-size: 1.3em;white-space:nowrap;}</style></head><body><table><center><h3>\u0424\u0438\u043d\u0430\u043d\u0441\u043e\u0432\u044b\u0439 \u043e\u0442\u0447\u0451\u0442</h3></center><tr><td>\u0414\u0438\u043b\u0435\u0440</td><td><b>dil1</b></td></tr><tr><td>\u041d\u0430</td><td>22.05.2018</td></tr><tr><td>\u0421\u0444\u043e\u0440\u043c\u0438\u0440\u043e\u0432\u0430\u043d</td><td>22.05.2018 06:44:39</td></tr></table><h3>\u0411\u0430\u043b\u0430\u043d\u0441 \u0434\u0438\u043b\u0435\u0440\u0430</h3><table><tr><td>\u041d\u0430 \u043d\u0430\u0447\u0430\u043b\u043e</td><td>-3,06</td></tr><tr><td>\u041d\u0430 \u043a\u043e\u043d\u0435\u0446</td><td>-3,06</td></tr><tr><td>\u041e\u0432\u0435\u0440\u0434\u0440\u0430\u0444\u0442</td><td>10,00</td></tr></table><h3>\u041f\u043e \u0442\u0435\u0440\u043c\u0438\u043d\u0430\u043b\u0443 #116809</h3><table><tr><td>\u041a\u043e\u043b-\u0432\u043e \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0439</td><td>0</td></tr><tr><td>\u041d\u0430 \u0441\u0447\u0451\u0442</td><td>0,00</td></tr><tr><td>\u041a\u043e\u043c\u0438\u0441\u0441\u0438\u044f</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441\u044b</td><td>0,00</td></tr></table><h3>\u041f\u043e \u0434\u0438\u043b\u0435\u0440\u0443: <b>dil1</b></h3><table><tr><td>\u041a\u043e\u043b-\u0432\u043e \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0439</td><td>0</td></tr><tr><td>\u041d\u0430 \u0441\u0447\u0451\u0442</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u043f\u0440\u0438\u0445\u043e\u0434</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u0440\u0430\u0441\u0445\u043e\u0434</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u0438\u0442\u043e\u0433\u043e</td><td>0,00</td></tr><tr><td>\u041f\u0440\u0438\u0445\u043e\u0434/\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435</td><td>0,00</td></tr></table></body></html>";
ELpayOperationStatus_t ELpayTerminal_get_report_financial(ELpayTerminal_t* This, char* date, char * text, size_t size)
{
  This->tran.state.id = elpay_tnst_Successfull;
  This->tran.state.errMsg[0] = '\0';

  strncpy(text,reportSales,size);

  update_operation_status(This);

  return 0;
}

static const char reportTurn[] = "<!DOCTYPE html><html><head><title>\u041e\u0442\u0447\u0435\u0442</title><style type=\"text/css\">html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}body{font-size:25pt;font-family: Helvetica,arial,freesans,clean,sans-serif;padding: 0.5em;}table{width: 100%;border-collapse:collapse;}td:first-child{color:red;text-align: left;color: #8E8E8E;font-weight: normal;}td{text-align: right;font-weight: bold;border-bottom: 1px dotted #8E8E8E;}h3{margin-top: 1em;margin-bottom: 0.5em;color: #4183C4;font-size: 1.3em;white-space:nowrap;}</style></head><body><table><center><h3>\u0424\u0438\u043d\u0430\u043d\u0441\u043e\u0432\u044b\u0439 \u043e\u0442\u0447\u0451\u0442</h3></center><tr><td>\u0414\u0438\u043b\u0435\u0440</td><td><b>dil1</b></td></tr><tr><td>\u041d\u0430</td><td>22.05.2018</td></tr><tr><td>\u0421\u0444\u043e\u0440\u043c\u0438\u0440\u043e\u0432\u0430\u043d</td><td>22.05.2018 06:49:56</td></tr></table><h3>\u0411\u0430\u043b\u0430\u043d\u0441 \u0434\u0438\u043b\u0435\u0440\u0430</h3><table><tr><td>\u041d\u0430 \u043d\u0430\u0447\u0430\u043b\u043e</td><td>-3,06</td></tr><tr><td>\u041d\u0430 \u043a\u043e\u043d\u0435\u0446</td><td>-3,06</td></tr><tr><td>\u041e\u0432\u0435\u0440\u0434\u0440\u0430\u0444\u0442</td><td>10,00</td></tr></table><h3>\u041f\u043e \u0442\u0435\u0440\u043c\u0438\u043d\u0430\u043b\u0443 #116809</h3><table><tr><td>\u041a\u043e\u043b-\u0432\u043e \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0439</td><td>0</td></tr><tr><td>\u041d\u0430 \u0441\u0447\u0451\u0442</td><td>0,00</td></tr><tr><td>\u041a\u043e\u043c\u0438\u0441\u0441\u0438\u044f</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441\u044b</td><td>0,00</td></tr></table><h3>\u041f\u043e \u0434\u0438\u043b\u0435\u0440\u0443: <b>dil1</b></h3><table><tr><td>\u041a\u043e\u043b-\u0432\u043e \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0439</td><td>0</td></tr><tr><td>\u041d\u0430 \u0441\u0447\u0451\u0442</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u043f\u0440\u0438\u0445\u043e\u0434</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u0440\u0430\u0441\u0445\u043e\u0434</td><td>0,00</td></tr><tr><td>\u0411\u043e\u043d\u0443\u0441 \u0438\u0442\u043e\u0433\u043e</td><td>0,00</td></tr><tr><td>\u041f\u0440\u0438\u0445\u043e\u0434/\u0441\u043f\u0438\u0441\u0430\u043d\u0438\u0435</td><td>0,00</td></tr></table></body></html>";
ELpayOperationStatus_t ELpayTerminal_get_report_turn(ELpayTerminal_t* This, char * text, size_t size)
{
  This->tran.state.id = elpay_tnst_Successfull;
  This->tran.state.errMsg[0] = '\0';

  strncpy(text,reportSales,size);

  update_operation_status(This);

  return 0;
}

static const char reportEncashment[] = "";
ELpayOperationStatus_t ELpayTerminal_get_report_encashment(ELpayTerminal_t* This, char* date, char * text, size_t size)
{
  This->tran.state.id = elpay_tnst_Successfull;
  This->tran.state.errMsg[0] = '\0';

  strncpy(text,reportSales,size);

  update_operation_status(This);

  return 0;
}
