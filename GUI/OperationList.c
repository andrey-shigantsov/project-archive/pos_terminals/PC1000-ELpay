#define LOG_HEADER "GUI/OperationList"
#if 0
#define LOG_LEVEL GUI_LOG_LEVEL
#else
#define LOG_LEVEL DBG
#endif

#include "OperationList.h"

#include "SYSTEM/logs.h"

#include <stdio.h>
#include <math.h>

static void item_draw(OperationListItem_t * This)
{
  Object_clearDrawBox(&This->obj);
  object_draw(&This->Layout.obj);
}

static void item_resize(OperationListItem_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

static const char * status_str(bool status)
{
  return (status ? "ok" : "X");
}

void init_OperationListItem(OperationListItem_t* This, const int Num, const char* Account, double Nominal, bool Status)
{
  init_Object(&This->obj, This, "OpListItem");
  This->obj.draw = (GUI_fxn_t)item_draw;
  This->obj.resize = (GUI_fxn_t)item_resize;

  init_Label(&This->Num);
  Object_setVisiable(&This->Num.obj, false);
  Label_setTextAlignment(&This->Status,alHLeft|alVCenter);
  snprintf(Label_Text(&This->Num),GUI_LABEL_BUFSIZE, "%d.", Num);

  init_Label(&This->Account);
  Label_setTextAlignment(&This->Account,alHLeft|alVCenter);
  Label_setTextCyr(&This->Account, Account);

  init_Label(&This->Nominal);
  Label_setTextAlignment(&This->Nominal,alHRight|alVCenter);
  dtostrf_p(Nominal,2,Label_Text(&This->Nominal), GUI_LABEL_BUFSIZE);

  init_Label(&This->Status);
  Label_setTextAlignment(&This->Status,alHRight|alVCenter);
  Label_setTextCyr(&This->Status, status_str(Status));

  This->Objs[0] = &This->Num.obj;
  This->LayoutStretchs[0] = 1;
  This->Objs[1] = &This->Account.obj;
  This->LayoutStretchs[1] = 4;
  This->Objs[2] = &This->Nominal.obj;
  This->LayoutStretchs[2] = 3;
  This->Objs[3] = &This->Status.obj;
  This->LayoutStretchs[3] = 1;

  init_ListLayout(&This->Layout, orHorizontal, 1, This->Objs, GUI_OPLIST_ITEM_OBJSCOUNT);
  ListLayout_setStretch(&This->Layout, This->LayoutStretchs);
}

/*-----------------------------------------------------------------------*/

static void draw(OperationList_t * This)
{
  Object_clearDrawBox(&This->obj);
  object_draw(&This->Header.obj);
  object_draw(&This->Layout.obj);
}

static void resize(OperationList_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);

  BYTE wH = This->Header.obj.font.size+This->Header.obj.margins.top+This->Header.obj.margins.bottom;
  Object_setGeometry(&This->Header.obj, g.x,g.y,g.w, wH);
  g.y += wH;
  g.h -= wH;

  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

void refresh_objs(OperationList_t* This)
{
  for (int i = 0; i < GUI_OPLIST_OBJSCOUNT; ++i)
  {
    int I = This->ItemsCount - This->currentItemIdx - i - 1;
    if ((I >= 0)&&(I < This->ItemsCount))
      This->Objs[i] = &This->Items[I].obj;
    else
      This->Objs[i] = 0;
  }
  listlayout_exec(&This->Layout);
}

static void refresh_currentItemIdx(OperationList_t * This)
{
  if (This->currentItemIdx + This->pageItemsCount >= This->ItemsCount)
    This->currentItemIdx = This->ItemsCount - This->pageItemsCount;
  if (This->currentItemIdx < 0)
    This->currentItemIdx = 0;
  refresh_objs(This);
}

static void key_handler(OperationList_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYUP:
    This->currentItemIdx -= This->pageItemsCount;
    refresh_currentItemIdx(This);
    object_draw(&This->obj);
    break;

  case KEYDOWN:
    This->currentItemIdx += This->pageItemsCount;
    refresh_currentItemIdx(This);
    object_draw(&This->obj);
    break;

  default:
//    object_key_handler(&This->menu.obj, key, isNew);
    break;
  }
}

static void current_handler(OperationList_t * This)
{
//  object_current_handler(&This->menu.obj);
}

void init_OperationList(OperationList_t* This)
{
  init_Object(&This->obj, This, "OpList");
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;

  init_Label(&This->Header);
  This->Header.obj.font.isInverseColor = true;
  This->Header.obj.font.size = 8;
  Object_setMargins(&This->Header.obj, 1,0,0,1);
  Label_setTextCyr(&This->Header, " СПИСОК ОПЕРАЦИЙ");

  This->currentItemIdx = 0;
  This->pageItemsCount = GUI_OPLIST_PAGE_ITEMS_COUNT;

  This->ItemsCount = 0;
  for (int i = 0; i < GUI_OPLIST_OBJSCOUNT; ++i)
    This->Objs[i] = 0;

  init_ListLayout(&This->Layout, orVertical, 0, This->Objs, GUI_OPLIST_OBJSCOUNT);
}

void OperationList_clear(OperationList_t * This)
{
  This->currentItemIdx = 0;
  This->ItemsCount = 0;

  refresh_objs(This);
}

void OperationList_appendItem(OperationList_t* This, const char* Account, double Nominal, bool Status)
{
  assert(This->ItemsCount < GUI_OPLIST_MAX_ITEMS_COUNT);

  init_OperationListItem(&This->Items[This->ItemsCount],This->ItemsCount+1,Account,Nominal,Status);
  ++This->ItemsCount;

  refresh_objs(This);
}
