/*
 * ControlMenu.c
 *
 *  Created on: 30 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "GUI/ControlMenu"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "ControlMenu.h"
#include "MainWidget.h"

#include "SYSTEM/logs.h"

static void draw(ControlMenu_t * This)
{
  object_draw(&This->currentMenu->obj);
}

static void key_handler(ControlMenu_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYCANCEL:
    if (This->currentMenu == &This->menuReports)
    {
      This->currentMenu = &This->menu;
      object_draw(&This->obj);
      break;
    }
    MainWidget_setMode(This->mw, MainWidget_prevMode(This->mw));
    break;
  default:
    object_key_handler(&This->currentMenu->obj, key, isNew);
    break;
  }
}

static void current_handler(ControlMenu_t * This)
{
  object_current_handler(&This->currentMenu->obj);
}

static void resize(ControlMenu_t * This)
{
  Object_copySizeParams(&This->menu.obj, &This->obj);
  Object_copySizeParams(&This->menuReports.obj, &This->obj);
}

//-------------------------------------------------------------

static void action_show_oplist(ControlMenu_t * This)
{
  MainWidget_showOperationList((MainWidget_t*)This->mw);
}

static void action_show_balance(ControlMenu_t * This)
{
  MainWidget_showBalance((MainWidget_t*)This->mw);
}

static void action_get_products(ControlMenu_t * This)
{
  MainWidget_updateProductsList((MainWidget_t*)This->mw);
}

static void action_show_reports_menu(ControlMenu_t * This)
{
  This->currentMenu = &This->menuReports;
  object_current_handler(&This->currentMenu->obj);
  object_draw(&This->obj);
}

static Action_t actions[GUI_CONTROLMENU_ITEMSBUFSIZE] =
{
  {"список операций", (GUI_fxn_t)action_show_oplist},
  {"отчеты", (GUI_fxn_t)action_show_reports_menu},
  {"баланс диллера", (GUI_fxn_t)action_show_balance},
  {"обновить продукты", (GUI_fxn_t)action_get_products}
};

static void menu_item_action(ControlMenu_t * This, int idx)
{
  if ((idx < 0)||(idx >= This->ItemsCount))
  {
    LOG_FXN_MSG(WRN,FAIL, "idx = %d incorrect", idx);
    return;
  }
  if(actions[idx].fxn)
    actions[idx].fxn(This);
}

static void action_get_report_sales(ControlMenu_t * This)
{
  MainWidget_getReportSales(This->mw,false);
}

static void action_get_report_sales_detailed(ControlMenu_t * This)
{
  MainWidget_getReportSales(This->mw,true);
}

static void action_get_report_financial(ControlMenu_t * This)
{
  MainWidget_getReportFinancial(This->mw);
}

static void action_get_report_turn(ControlMenu_t * This)
{
  MainWidget_getReportTurn(This->mw);
}

static void action_get_report_encashment(ControlMenu_t * This)
{
  MainWidget_getReportEncashment(This->mw);
}

static Action_t report_actions[GUI_REPORTSMENU_ITEMSBUFSIZE] =
{
  {"продажи", (GUI_fxn_t)action_get_report_sales},
  {"детал. продажи", (GUI_fxn_t)action_get_report_sales_detailed},
  {"финансовый", (GUI_fxn_t)action_get_report_financial},
  {"за смену", (GUI_fxn_t)action_get_report_turn},
  {"инкасация", (GUI_fxn_t)action_get_report_encashment}
};

static void menu_report_item_action(ControlMenu_t * This, int idx)
{
  if ((idx < 0)||(idx >= This->reportItemsCount))
  {
    LOG_FXN_MSG(WRN,FAIL, "idx = %d incorrect", idx);
    return;
  }
  if(report_actions[idx].fxn)
    report_actions[idx].fxn(This);
}

void init_ControlMenu(ControlMenu_t * This, void * mw)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "CtrlMenu");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;

  init_Menu(&This->menu);
  Menu_setLayoutSize(&This->menu, menuNormal);
  tr_cyr(" МЕНЮ", Menu_Header(&This->menu), GUI_LABEL_BUFSIZE);
  Menu_enableHeader(&This->menu, true);
  Object_setParent(&This->menu.obj, This);
  This->menu.item_action = (menu_item_action_fxn_t)menu_item_action;

  This->ItemsCount = GUI_CONTROLMENU_ITEMSBUFSIZE;
  MenuItems_FromActions(This->Items, &This->menu, actions, This->ItemsCount);
  Menu_setItems(&This->menu, This->Items, This->ItemsCount);

  init_Menu(&This->menuReports);
  Menu_setLayoutSize(&This->menuReports, menuNormal);
  tr_cyr(" ОТЧЕТЫ", Menu_Header(&This->menuReports), GUI_LABEL_BUFSIZE);
  Menu_enableHeader(&This->menuReports, true);
  Object_setParent(&This->menuReports.obj, This);
  This->menuReports.item_action = (menu_item_action_fxn_t)menu_report_item_action;

  This->reportItemsCount = GUI_REPORTSMENU_ITEMSBUFSIZE;
  MenuItems_FromActions(This->reportItems, &This->menuReports, report_actions, This->reportItemsCount);
  Menu_setItems(&This->menuReports, This->reportItems, This->reportItemsCount);

  This->currentMenu = &This->menu;

  This->mw = mw;

  LOG_FXN(DBG,OK);
}

void reset_ControlMenu(ControlMenu_t* This)
{
  This->currentMenu = &This->menu;
}
