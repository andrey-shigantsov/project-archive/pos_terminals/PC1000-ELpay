/*
 * ControlMenu.c
 *
 *  Created on: 30 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "GUI/ExitMenu"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "ExitMenu.h"
#include "MainWidget.h"

#include "SYSTEM/logs.h"
#include "SYSTEM/control.h"

static void draw(ExitMenu_t * This)
{
  object_draw(&This->menu.obj);
}

static void key_handler(ExitMenu_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  default:
    object_key_handler(&This->menu.obj, key, isNew);
    break;
  }
}

static void current_handler(ExitMenu_t * This)
{
  object_current_handler(&This->menu.obj);
}

static void resize(ExitMenu_t * This)
{
  Object_copySizeParams(&This->menu.obj, &This->obj);
}

//-------------------------------------------------------------

static void action_reg(ExitMenu_t * This)
{
  MainWidget_setMode((MainWidget_t*)This->mw, mwReg);
}

static void action_logout(ExitMenu_t * This)
{
  MainWidget_setMode((MainWidget_t*)This->mw, mwAuth);
}

static void action_exit(ExitMenu_t * This)
{
  system_exit();
}

static Action_t actions[GUI_EXITMENU_ITEMSBUFSIZE] =
{
  {"регистрация", (GUI_fxn_t)action_reg},
  {"сменить ползователя", (GUI_fxn_t)action_logout},
  {"выход", (GUI_fxn_t)action_exit}
};

static void menu_item_action(ExitMenu_t * This, int idx)
{
  if ((idx < 0)||(idx >= This->ItemsCount))
  {
    LOG_FXN_MSG(WRN,FAIL, "idx = %d incorrect", idx);
    return;
  }
  if(actions[idx].fxn)
    actions[idx].fxn(This);
}

void init_ExitMenu(ExitMenu_t * This, void * mw)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "ExitMenu");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;

  init_Menu(&This->menu);
  Menu_setLayoutSize(&This->menu, menuNormal);
  tr_cyr(" ВЫХОД", Menu_Header(&This->menu), GUI_LABEL_BUFSIZE);
  Menu_enableHeader(&This->menu, true);
  Object_setParent(&This->menu.obj, This);
  This->menu.item_action = (menu_item_action_fxn_t)menu_item_action;

  This->ItemsCount = GUI_EXITMENU_ITEMSBUFSIZE;
  MenuItems_FromActions(This->Items, &This->menu, actions, This->ItemsCount);
  Menu_setItems(&This->menu, This->Items, This->ItemsCount);

  This->mw = mw;

  LOG_FXN(DBG,OK);

}
