/* 
 * File:   ProductsMenu.h
 */

#ifndef GUI_PRODUCTSMENU_H
#define GUI_PRODUCTSMENU_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/Menu.h"

#include "ELpay/types.h"

#ifdef __cplusplus
extern "C" {
#endif
  
#define GUI_PRODUCTSMENU_ITEMSBUFSIZE 100
  
typedef void (*elpay_product_action_fxn_t)(void * Parent, int id);

typedef struct
{
  Object_t obj;
  
  Menu_t menu;
  MenuItem_t Items[GUI_PRODUCTSMENU_ITEMSBUFSIZE];
  ELpayProduct_t * ItemsProducts[GUI_PRODUCTSMENU_ITEMSBUFSIZE];
  size_t ItemsCount;
  
  ELpayLang_t lang;
  ELpayProduct_t * elpayProducts;
  size_t elpayProductsCount;

  elpay_product_action_fxn_t elpay_product_action;
} ProductsMenu_t;

void init_ProductsMenu(ProductsMenu_t * This, ELpayProduct_t * elpayProducts);

void ProductsMenu_setProductsCount(ProductsMenu_t * This, size_t count);
void ProductsMenu_refreshItemsData(ProductsMenu_t * This, int service_id);

#ifdef __cplusplus
}
#endif

#endif /* GUI_PRODUCTSMENU_H */
