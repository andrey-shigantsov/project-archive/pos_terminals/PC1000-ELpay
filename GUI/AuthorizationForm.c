/* 
 * File:   AuthorizationForm.c
 */

#include "AuthorizationForm.h"
#define LOG_HEADER "GUI/AuthorizationForm"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

static void draw(AuthorizationForm_t * This)
{
  Object_clearDrawBox(&This->obj);

  object_draw(&This->Layout.obj);
}

static void refresh_selected(AuthorizationForm_t * This)
{
  if (This->flag_dataChanged)
  {
    if (This->data_changed)
      This->data_changed(This->obj.Parent);
    This->flag_dataChanged = 0;
  }

  if (This->Selected < 0)
    This->Selected = 1;
  else
    This->Selected %= 2;
  
  LineEdit_setSelection(&This->login, This->Selected == 0);
  LineEdit_setSelection(&This->password, This->Selected == 1);
}

static void key_handler(AuthorizationForm_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYENTER:
    if (This->login.textLen == 0)
    {
      This->Selected = 0;
      refresh_selected(This);
      object_draw(&This->obj);
      break;
    }
    if (This->password.textLen == 0)
    {
      This->Selected = 1;
      refresh_selected(This);
      object_draw(&This->obj);
      break;
    }
    if (This->go)
      This->go(This->obj.Parent);
    break;
    
  case KEYUP:
    --This->Selected;
    refresh_selected(This);
    object_draw(&This->obj);
    break;
    
  case KEYDOWN:
  case KEYOK:
    ++This->Selected;
    refresh_selected(This);
    object_draw(&This->obj);
    break;
    
  default:{
    Object_t * objSelected = 0;
    switch (This->Selected)
    {
    case 0:
      objSelected = &This->login.obj;
      break;
      
    case 1:
      objSelected = &This->password.obj;
      break;
    }
    This->flag_dataChanged = 1;
    object_key_handler(objSelected, key, isNew);
    break;}
  }
}

static void tick_handler(AuthorizationForm_t * This)
{
  switch (This->Selected)
  {
  case 0:
    object_tick_handler(&This->login.obj);
    break;
    
  case 1:
    object_tick_handler(&This->password.obj);
    break;
  }
}

static void key_edit_finished(AuthorizationForm_t * This)
{
  switch (This->Selected)
  {
  case 0:
    object_key_edit_finished(&This->login.obj);
    break;
    
  case 1:
    object_key_edit_finished(&This->password.obj);
    break;
  }
}

static void current_handler(AuthorizationForm_t * This)
{
  (void)This;
  setKeysMode(keysMode_All);
}

static void resize(AuthorizationForm_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

void init_AuthorizationForm(AuthorizationForm_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "AuthForm");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;

  This->obj.font.size = 8;

  init_Label(&This->labelLogin);
  This->labelLogin.obj.font.size = 8;
  Label_setTextCyr(&This->labelLogin, "логин");

  init_Label(&This->labelPassword);
  This->labelPassword.obj.font.size = 8;
  Label_setTextCyr(&This->labelPassword, "пароль");

  init_LineEdit(&This->login);
  LineEdit_setTextBuf(&This->login, This->loginBuf, GUI_AUTH_BUFSIZE);
  LineEdit_setTextAlignment(&This->login, alHLeft|alVCenter);
  This->login.obj.font.size = 8;
  
  init_LineEdit(&This->password);
//  LineEdit_enablePasswordMode(&This->password);
  LineEdit_setTextBuf(&This->password, This->passwordBuf, GUI_AUTH_BUFSIZE);
  LineEdit_setTextAlignment(&This->password, alHLeft|alVCenter);
  This->password.obj.font.size = 8;
  
  This->Objs[0][0] = &This->labelLogin.obj;
  This->Objs[0][1] = &This->login.obj;
  This->Objs[1][0] = &This->labelPassword.obj;
  This->Objs[1][1] = &This->password.obj;

  Object_setSizePolicy(This->Objs[0][0], szMin, szMin);
  Object_setSizePolicy(This->Objs[0][1], szExpand, szMin);
  Object_setSizePolicy(This->Objs[1][0], szMin, szMin);
  Object_setSizePolicy(This->Objs[1][1], szExpand, szMin);

  This->ObjsAlign[0][0] = alHRight | alVCenter;
  This->ObjsAlign[0][1] = alHLeft | alVCenter;
  This->ObjsAlign[1][0] = alHRight | alVCenter;
  This->ObjsAlign[1][1] = alHLeft | alVCenter;

  This->ColStretch[0] = 1;
  This->ColStretch[1] = 2;

  for (int i = 0; i < GUI_AUTHFORM_ROWCOUNT; ++i)
  {
    This->ObjBuf[i] = This->Objs[i];
    This->ObjAlignBuf[i] = This->ObjsAlign[i];
  }

  init_TableLayout(&This->Layout, 4,2, This->ObjBuf, GUI_AUTHFORM_ROWCOUNT, GUI_AUTHFORM_COLCOUNT);
  TableLayout_setChildrensAlignment(&This->Layout, This->ObjAlignBuf);
  TableLayout_setColumnsStretch(&This->Layout, This->ColStretch);

  Object_setMargins(&This->obj, 10,2,2,10);

  This->flag_dataChanged = 0;

  This->Selected = 0;
  refresh_selected(This);
  
  This->data_changed = 0;
  This->go = 0;

  LOG_FXN(DBG,OK);
}
