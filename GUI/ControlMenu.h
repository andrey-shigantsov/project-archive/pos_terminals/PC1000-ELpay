/*
 * ControlMenu.h
 *
 *  Created on: 30 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef GUI_CONTROLMENU_H_
#define GUI_CONTROLMENU_H_

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/Menu.h"

typedef struct ControlMenu_s ControlMenu_t;

#ifdef __cplusplus
extern "C"{
#endif

#define GUI_CONTROLMENU_ITEMSBUFSIZE 4
#define GUI_REPORTSMENU_ITEMSBUFSIZE 5

struct ControlMenu_s
{
  Object_t obj;

  Menu_t menu;
  MenuItem_t Items[GUI_CONTROLMENU_ITEMSBUFSIZE];
  size_t ItemsCount;

  Menu_t menuReports;
  MenuItem_t reportItems[GUI_REPORTSMENU_ITEMSBUFSIZE];
  size_t reportItemsCount;

  Menu_t * currentMenu;

  void * mw;
};

void init_ControlMenu(ControlMenu_t * This, void * mw);

void reset_ControlMenu(ControlMenu_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_CONTROLMENU_H_ */
