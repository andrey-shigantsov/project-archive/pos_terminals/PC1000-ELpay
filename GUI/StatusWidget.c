/*
 * File:   StatusWidget.c
 */

#include "StatusWidget.h"
#define LOG_HEADER "GUI/StatusWidget"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"

#include "SYSTEM/tr.h"

#include <string.h>

static void draw(StatusWidget_t * This)
{
  Object_clearDrawBox(&This->obj);

  Font_t f;
  f.isInverseColor = 1;
  f.size = 8;
  Box_t b = Object_Box(&This->obj);
  Box_t bH0 = b;
  bH0.y = b.y0+f.size+1;
  Lib_LcdDrawBox(bH0.x0,bH0.y0, bH0.x,bH0.y);

  Box_t bH = bH0;
  bH.x0 += 1; bH.y0 += 1; bH.x -= 1; bH.y -= 1;
  Lib_LcdPrintxy(bH.x0,bH.y0, font_attr(&f), (char*)This->Header);

  BYTE x0 = bH.x0 + strlen(This->Header)*font_width(&f);
  BYTE y;
  for (y = bH.y0; y <= bH.y; ++y)
    Lib_LcdDrawLine(x0, y, bH.x, y, 1);

  b = Object_contentBox(&This->obj);
  Lib_LcdSetFont(This->obj.font.size,16,0);
  Lib_LcdGotoxy(b.x0,b.y0);
  Lib_Lcdprintf(This->Msg);
}

static void key_handler(StatusWidget_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYCANCEL:
    if (This->cancel)
      This->cancel(This->obj.Parent);
    break;

  case KEYOK:
    if (This->ok_handler)
      This->ok_handler(This->obj.Parent);
    break;

  case KEYENTER:
    if (This->confirm)
      This->confirm(This->obj.Parent);
    break;

  default:
    break;
  }
}

void StatusWidget_setHeader(StatusWidget_t * This, const char * header)
{
  tr_cyr(header, This->Header, sizeof(This->Header));
}

void StatusWidget_setMsg(StatusWidget_t * This, const char * msg)
{
  tr_cyr(msg, This->Msg, sizeof(This->Msg));
}

void StatusWidget_showNewNativeMsg(StatusWidget_t * This, const char * header, const char * nativeMsg)
{
  keyboard_flush();

  StatusWidget_setHeader(This, header);
  strncpy(This->Msg, nativeMsg, sizeof(This->Msg));

  object_draw(&This->obj);
}

void StatusWidget_showNewMsg(StatusWidget_t * This, const char * header, const char * msg)
{
  keyboard_flush();

  StatusWidget_setHeader(This, header);
  StatusWidget_setMsg(This, msg);

  object_draw(&This->obj);
}

void StatusWidget_showMsg(StatusWidget_t * This, const char * msg)
{
  keyboard_flush();

  StatusWidget_setMsg(This, msg);
  object_draw(&This->obj);
}

void init_StatusWidget(StatusWidget_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "StatusWidget");
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;

  Object_setMargins(&This->obj, 11,1,1,1);

  This->obj.font.size = 8;

  This->cancel = 0;
  This->confirm = 0;

  LOG_FXN(DBG,OK);
}
