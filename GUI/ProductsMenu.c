/*
 * File:   CategoriesMenu.c
 */

#include "GUI/ProductsMenu.h"
#define LOG_HEADER "GUI/ProductsMenu"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

#include <stdio.h>
#include <assert.h>

static void draw(ProductsMenu_t * This)
{
	object_draw(&This->menu.obj);
}

static void key_handler(ProductsMenu_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  default:
    object_key_handler(&This->menu.obj, key, isNew);
    break;
  }
}

static void menu_item_action(ProductsMenu_t * This, int idx)
{
  if ((idx < 0) || (idx >= This->elpayProductsCount))
  {
    LOG_FXN_MSG(WRN,FAIL, "idx = %d incorrect, count = %d", idx, This->elpayProductsCount);
    return;
  }
  if (This->elpay_product_action)
    This->elpay_product_action(This->obj.Parent, This->ItemsProducts[idx]->id);
}

static void current_handler(ProductsMenu_t * This)
{
  object_current_handler(&This->menu.obj);
}

static void resize(ProductsMenu_t * This)
{
  Object_copySizeParams(&This->menu.obj, &This->obj);
}

void ProductsMenu_setProductsCount(ProductsMenu_t * This, size_t count)
{
  assert(count <= GUI_PRODUCTSMENU_ITEMSBUFSIZE);

  This->elpayProductsCount = count;
}
void ProductsMenu_refreshItemsData(ProductsMenu_t * This, int service_id)
{
  LOG_FXN_MSG(DBG, NONE, "sid = %d, processing...", service_id);

  This->ItemsCount = 0;
  for (BYTE i = 0; i < This->elpayProductsCount; ++i)
  {
#ifdef PRODUCTSMENU_OUTPUT_CHECK_LOG
    LOG_FXN_MSG(DBG, NONE, "check: pid = %04d, sid = %03d", This->elpayProducts[i].id, This->elpayProducts[i].sid);
#endif
    if (This->elpayProducts[i].sid != service_id) continue;

    This->ItemsProducts[This->ItemsCount] = &This->elpayProducts[i];
    MenuItem_t * item = &This->Items[This->ItemsCount++];
    init_MenuItem(item, &This->menu);
    char name[32];
    tr_cyr(This->elpayProducts[i].name[This->lang], name, sizeof(name));
    snprintf(MenuItem_Text(item), GUI_MENU_LABEL_BUFLEN+1,
      //"pid=%04d", This->elpayProducts[i].id);
      name);
  }
  Menu_setItems(&This->menu, This->Items, This->ItemsCount);

  LOG_FXN(DBG,OK);
}

void init_ProductsMenu(ProductsMenu_t * This, ELpayProduct_t * elpayProducts)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "ProdsMenu");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;

//  Object_setMargins(&This->obj, 1,1,1,1);

  init_Menu(&This->menu);
  tr_cyr(" ПРОДУКТЫ", Menu_Header(&This->menu), GUI_LABEL_BUFSIZE);
//  Menu_enableHeader(&This->menu, true);
  Object_setParent(&This->menu.obj, This);
  This->menu.item_action = (menu_item_action_fxn_t)menu_item_action;

  This->ItemsCount = 0;

  This->lang = elpay_RU;
  This->elpayProducts = elpayProducts;
  This->elpayProductsCount = 0;

  This->elpay_product_action = 0;

  LOG_FXN(DBG,OK);
}
