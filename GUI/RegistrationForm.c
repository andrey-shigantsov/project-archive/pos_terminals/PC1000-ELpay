/* 
 * File:   RegistrationForm.c
 */

#include "RegistrationForm.h"
#define LOG_HEADER "GUI/RegistrationForm"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

static void draw(RegistrationForm_t * This)
{
  Object_clearDrawBox(&This->obj);

  object_draw(&This->Layout.obj);

}

static void key_handler(RegistrationForm_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYENTER:
    if (This->go)
      This->go(This->obj.Parent);
    break;
    
  default:
    object_key_handler(&This->regKey.obj, key, isNew);
    break;
  }
}

static void tick_handler(RegistrationForm_t * This)
{
  object_tick_handler(&This->regKey.obj);
}

static void key_edit_finished(RegistrationForm_t * This)
{
  object_key_edit_finished(&This->regKey.obj);
}

static void current_handler(RegistrationForm_t * This)
{
  (void)This;
  setKeysMode(keysMode_Int);
}

static void resize(RegistrationForm_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

void init_RegistrationForm(RegistrationForm_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "RegForm");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;
  
  Object_setMargins(&This->obj, 1,1,1,10);
  This->obj.font.size = 8;

  init_Label(&This->labelRegKey);
  Label_setTextAlignment(&This->labelRegKey, alVBottom|alHCenter);
  This->labelRegKey.obj.font.size = 8;
  Label_setTextCyr(&This->labelRegKey, "Код регистрации");

  init_LineEdit(&This->regKey);
  Object_setSizePolicy(&This->regKey.obj, szMin, szMin);
  This->regKey.obj.font.size = 8;
  This->regKey.maxDispLen = 8;
  LineEdit_setTextBuf(&This->regKey, This->regKeyBuf, REGKEY_BUFSIZE);
  LineEdit_setSelection(&This->regKey, 1);
  LineEdit_setTextAlignment(&This->regKey, alHCenter|alVCenter);
  
  This->Objs[0] = &This->labelRegKey.obj;
  This->Objs[1] = &This->regKey.obj;
  init_ListLayout(&This->Layout, orVertical, 4, This->Objs, GUI_REGFORM_OBJSCOUNT);
  This->ObjsAlign[0] = alHCenter|alVBottom;
  This->ObjsAlign[1] = alHCenter|alVTop;
  ListLayout_setAlignment(&This->Layout, This->ObjsAlign);

  This->go = 0;

  LOG_FXN(DBG,OK);
}
