/* 
 * File:   ReportParamsForm.h
 */

#ifndef GUI_REPORTPARAMSFORM_H
#define GUI_REPORTPARAMSFORM_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/ListLayout.h"
#include "SYSTEM/GUI/Label.h"
#include "SYSTEM/GUI/LineEdit.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  Object_t obj;
  ListLayout_t Layout;

  Label_t Format;
  LineEdit_t Date1, Date2;

  int Selected, SelectedCount;
  
  #define GUI_REPPARAMFORM_ITEMSCOUNT 3
  Object_t * Objs[GUI_REPPARAMFORM_ITEMSCOUNT];
  Alignment_t  ObjsAlign[GUI_REPPARAMFORM_ITEMSCOUNT];
  BYTE ObjsStretch[GUI_REPPARAMFORM_ITEMSCOUNT];

  #define GUI_REPPARAMSFORM_DATE_BUFSIZE (32+1)
  char bufDate1[GUI_REPPARAMSFORM_DATE_BUFSIZE], bufDate2[GUI_REPPARAMSFORM_DATE_BUFSIZE];
  
  GUI_fxn_t go;
} ReportParamsForm_t;

void init_ReportParamsForm(ReportParamsForm_t * This);

void ReportParamsForm_setMode(ReportParamsForm_t * This, bool isTwoDates);
void ReportParamsForm_setFormat(ReportParamsForm_t * This, char* Format);

#ifdef __cplusplus
}
#endif

#endif /* GUI_REPORTPARAMSFORM_H */
