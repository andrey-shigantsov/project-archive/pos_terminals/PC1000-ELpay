/* 
 * File:   ReportParamsForm.c
 */

#include "ReportParamsForm.h"
#define LOG_HEADER "GUI/ReportParamsForm"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

#include <string.h>

static void draw(ReportParamsForm_t * This)
{
  Object_clearDrawBox(&This->obj);

  object_draw(&This->Layout.obj);
}

static void refresh_selected(ReportParamsForm_t * This)
{
  if (This->Selected < 0)
    This->Selected = This->SelectedCount-1;
  else
    This->Selected %= This->SelectedCount;
  
  LineEdit_setSelection(&This->Date1, This->Selected == 0);
  LineEdit_setSelection(&This->Date2, This->Selected == 1);
}

static void key_handler(ReportParamsForm_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYENTER:
    if (This->go)
      This->go(This->obj.Parent);
    break;
    
  case KEYUP:
    --This->Selected;
    refresh_selected(This);
    object_draw(&This->obj);
    break;
    
  case KEYDOWN:
  case KEYOK:
    ++This->Selected;
    refresh_selected(This);
    object_draw(&This->obj);
    break;
    
  default:{
    Object_t * objSelected = 0;
    switch (This->Selected)
    {
    case 0:
      objSelected = &This->Date1.obj;
      break;
      
    case 1:
      objSelected = &This->Date2.obj;
      break;
    }
    object_key_handler(objSelected, key, isNew);
    break;}
  }
}

static void tick_handler(ReportParamsForm_t * This)
{
  switch (This->Selected)
  {
  case 0:
    object_tick_handler(&This->Date1.obj);
    break;
    
  case 1:
    object_tick_handler(&This->Date2.obj);
    break;
  }
}

static void key_edit_finished(ReportParamsForm_t * This)
{
  switch (This->Selected)
  {
  case 0:
    object_key_edit_finished(&This->Date1.obj);
    break;
    
  case 1:
    object_key_edit_finished(&This->Date2.obj);
    break;
  }
}

static void current_handler(ReportParamsForm_t * This)
{
  (void)This;
  setKeysMode(keysMode_Int);
}

static void resize(ReportParamsForm_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

void init_ReportParamsForm(ReportParamsForm_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "RepProdForm");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;

  init_Label(&This->Format);
  Label_setTextCyr(&This->Format, "");
  Label_setTextAlignment(&This->Format, alHCenter|alVCenter);
  Object_setVisiable(&This->Format.obj,false);

  init_LineEdit(&This->Date1);
  LineEdit_setTextBuf(&This->Date1, This->bufDate1, GUI_REPPARAMSFORM_DATE_BUFSIZE);
  LineEdit_setTextAlignment(&This->Date1, alHCenter|alVCenter);
  
  init_LineEdit(&This->Date2);
  LineEdit_setTextBuf(&This->Date2, This->bufDate2, GUI_REPPARAMSFORM_DATE_BUFSIZE);
  LineEdit_setTextAlignment(&This->Date2, alHCenter|alVCenter);
  
  This->Objs[0] = &This->Format.obj;
  This->Objs[1] = &This->Date1.obj;
  This->Objs[2] = &This->Date2.obj;

  Object_setSizePolicy(This->Objs[0], szExpand, szMin);
  Object_setSizePolicy(This->Objs[1], szExpand, szMin);
  Object_setSizePolicy(This->Objs[2], szExpand, szMin);

  This->ObjsAlign[0] = alHCenter | alVBottom;
  This->ObjsAlign[1] = alHCenter | alVCenter;
  This->ObjsAlign[2] = alHCenter | alVTop;

  This->ObjsStretch[0] = 1;
  This->ObjsStretch[1] = 1;
  This->ObjsStretch[2] = 1;

  init_ListLayout(&This->Layout, orVertical, 2, This->Objs, GUI_REPPARAMFORM_ITEMSCOUNT);
  ListLayout_setAlignment(&This->Layout, This->ObjsAlign);
  ListLayout_setStretch(&This->Layout, This->ObjsStretch);

  Object_setMargins(&This->obj, 2,2,2,2);

  This->Selected = 0;
  This->SelectedCount = 2;
  refresh_selected(This);
  
  This->go = 0;

  LOG_FXN(DBG,OK);
}

void ReportParamsForm_setMode(ReportParamsForm_t* This, bool isTwoDates)
{
  Object_setVisiable(&This->Date2.obj, isTwoDates);
  This->Selected = 0;
  This->SelectedCount = isTwoDates ? 2 : 1;
}

void ReportParamsForm_setFormat(ReportParamsForm_t* This, char * Format)
{
  bool isFormat = strlen(Format) > 0;
  Label_setTextCyr(&This->Format, Format);
  Object_setVisiable(&This->Format.obj,isFormat);
}
