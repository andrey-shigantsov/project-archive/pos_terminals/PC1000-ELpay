/*
 * IndicatorsPanel.c
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */

#include "IndicatorsPanel.h"
#define LOG_HEADER "GUI/IndicatorsPanel"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"

static void draw(IndicatorsPanel_t * This)
{
  Object_clearDrawBox(&This->obj);
  object_draw(&This->Layout.obj);
}

static void resize(IndicatorsPanel_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

static void tick_handler(IndicatorsPanel_t * This)
{
  if (This->RefreshTicksCounter == 0)
  {
    NetworkIndicator_update(&This->Network);
  }
  ++This->RefreshTicksCounter;
  This->RefreshTicksCounter %= This->RefreshTicksCount;
}

void IndicatorsPanel_update(IndicatorsPanel_t * This)
{
  NetworkIndicator_update(&This->Network);
}

void init_IndicatorsPanel(IndicatorsPanel_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "IndPanel");
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;

  init_Label(&This->LabelTermId);
  Label_setTextAlignment(&This->LabelTermId, alHRight);
  This->LabelTermId.obj.font.size = 8;
  tr_cyr("терм.", This->LabelTermId.text, GUI_LABEL_BUFSIZE);

  init_Label(&This->TermId);
  Label_setTextAlignment(&This->TermId, alHLeft);

  init_NetworkIndicator(&This->Network);

  This->Objs[0] = &This->LabelTermId.obj;
  This->Objs[1] = &This->TermId.obj;
  This->Objs[2] = &This->Network.obj;

  Object_setSizePolicy(This->Objs[0], szMin, szMin);
  Object_setSizePolicy(This->Objs[1], szExpand, szMin);
  Object_setSizePolicy(This->Objs[2], szMin, szMin);

  init_ListLayout(&This->Layout, orHorizontal, 0, This->Objs, GUI_INDPAN_OBJSCOUNT);

  Object_setMargins(&This->obj, 0,0,0,0);

  This->RefreshTicksCount = 60;
  This->RefreshTicksCounter = 0;

  LOG_FXN(DBG,OK);
}
