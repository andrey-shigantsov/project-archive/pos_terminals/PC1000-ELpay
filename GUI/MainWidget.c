/*
 * MainWidget.c
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */

 #define LOG_HEADER "GUI/MainWidget"
 #if 1
 #define LOG_LEVEL GUI_LOG_LEVEL
 #else
 #define LOG_LEVEL DBG
 #endif

#include "MainWidget.h"

#include "SYSTEM/logs.h"
#include "SYSTEM/control.h"
#include "SYSTEM/printer.h"
#include "SYSTEM/tr.h"

#include <string.h>

#define PARAMS(...) __VA_ARGS__
#define AUTO_SNPRINTF_FOR_STATBUF(buf,...) \
  snprintf(buf, sizeof(buf), __VA_ARGS__)

#define SET_OUTMSG(This,h,m) \
  reset_OutMsg(This); \
  AUTO_SNPRINTF_FOR_STATBUF(This->OutMsg.header, h); \
  AUTO_SNPRINTF_FOR_STATBUF(This->OutMsg.msg, m);

static void status_msg_append(char * buf, size_t size, char * msg)
{
  if (!strlen(msg))
    return;
  size_t len = strlen(buf);
  snprintf(&buf[len], size-len, ":\n %s", msg);
}

static void reset_OutMsg(MainWidget_t * This)
{
  This->OutMsg.header[0] = '\0';
  This->OutMsg.msg[0] = '\0';
}

#define PRINTER_SEP "========================"
#define PRINTER_SCROLL "\n\n\n\n\n\n\n\n"

static void printMsg(MainWidget_t * This)
{
  StatusWidget_showNewMsg(&This->StatusWidget, This->OutMsg.header, "\n   печать...");

  char header[64], msg[256];
  tr_cyr(This->OutMsg.header, header, sizeof(header));
  tr_cyr(This->OutMsg.msg, msg, sizeof(msg));
  printer_setFont(16);
  printer_print(
    "%s\n"
    PRINTER_SEP"\n"
    "%s\n"
    PRINTER_SEP"\n"
    PRINTER_SCROLL,
    header, msg);

  StatusWidget_showNewMsg(&This->StatusWidget, This->OutMsg.header, "\n печать завершена");
}

static void printCurrentBalance(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  if ((This->Process.etcType != pShowBalance)||(This->elpayStatus))
  {
    LOG_FXN(WRN, ABORT);
    return;
  }

  SET_OUTMSG(This, " БАЛАНС",
    PARAMS("%s\n\nсетевой: %s\nклиентский: %s\nкредит: %s\n"
      "\nсуточное ограничение:\n %s\nдневное ограничение:\n %s\nограничение на операцию:\n %s",
      This->balance.date, This->balance.net, This->balance.client, This->balance.credit,
      This->balance.limit.day24, This->balance.limit.day, This->balance.limit.operation));

  printMsg(This);

  LOG_FXN(DBG, OK);
}

static const char * process_name[] =
{
  " РЕГИСТРАЦИЯ",
  " АВТОРИЗАЦИЯ",
  " ПОЛУЧЕНИЕ ПРОДУКТОВ",
  " СОЗДАНИЕ ОПЕРАЦИИ",
  " ПДТВРЖДНИЕ ОПЕРАЦИИ",
  " СТАТУС ОПЕРАЦИИ",
  " БАЛАНС",
  " ОТЧЁТ ПО ПРОДАЖАМ",
  " ФИНАНСОВЫЙ ОТЧЁТ",
  " ОТЧЁТ ЗА СМЕНУ",
  " ОТЧЁТ ПО ИНКАСАЦИИ",
  " СООБЩЕНИЕ",
  " ПЕЧАТЬ",
  ""
};

static void init_Process(MainWidget_t * This, ProcessType_t type, MainWidgetMode_t next, MainWidgetMode_t prev)
{
  IndicatorsPanel_update(&This->IndicatorsPanel);

  This->Process.type = type;
  This->Process.etcType = type;

  This->elpayStatus = elpay_status_GeneralFailure;

  if (next == mwCurrentMode)
    This->Process.NextMode = This->mode;
  else
    This->Process.NextMode = next;
  if (prev == mwCurrentMode)
    This->Process.PrevMode = This->mode;
  else
    This->Process.PrevMode = prev;

  StatusWidget_setHeader(&This->StatusWidget, process_name[type]);
  StatusWidget_setMsg(&This->StatusWidget,  "\n   обработка...");

  MainWidget_setMode(This, mwStatus);
}

static void process_cancel(MainWidget_t * This)
{
  This->Process.type = pEnd;
  MainWidget_setMode(This, This->Process.PrevMode);
}

static void process_set_next_mode(MainWidget_t * This, bool isError)
{
  MainWidgetMode_t mode = (isError? This->Process.PrevMode : This->Process.NextMode);
  MainWidget_setMode(This, mode);
}

static void process_confirm(MainWidget_t * This)
{
  bool isError = false;
  switch(This->Process.type)
  {
  case pAuth:
    if(MainWidget_loadProdList(This)) break;
    MainWidget_updateProductsList(This);
    return;

  case pCreateOp:
    if (This->elpayStatus)
    {
      isError = true;
      break;
    }
    MainWidget_ConfirmOperation(This);
    return;

  case pConfirmOp:
  case pGetOpStatus:
    if (This->elpayStatus)
    {
      isError = true;
      break;
    }
    switch(This->elpayTerminal->tran.state.id)
    {
    case elpay_tnst_Processing:
      Lib_DelayMs(1000);
      MainWidget_getOperationStatus(This);
      return;

    case elpay_tnst_NoConfirm:
      MainWidget_ConfirmOperation(This);
      return;

    default:
    case elpay_tnst_Canceled:
    case elpay_tnst_Canceled2:
    case elpay_tnst_Error:
      isError = true;
      break;

    case elpay_tnst_Successfull:
      MainWidget_printReceipt(This, &This->formProd.elpayProductData);
      break;
    }
    break;

  case pShowBalance:
  case pShowMsg:
    LOG(DBG, "show msg:\n - header:\n%s\n - msg:\n%s", This->OutMsg.header, This->OutMsg.msg);
    StatusWidget_showNewMsg(&This->StatusWidget, This->OutMsg.header, This->OutMsg.msg);
    This->Process.type = pEnd;
    return;

  case pPrintMsg:{
    printMsg(This);
    This->Process.type = pEnd;
    return;}

  default:
  case pEnd:
    break;
  }

  process_set_next_mode(This,isError);
}

static void process_ok_handler(MainWidget_t * This)
{
  switch(This->Process.etcType)
  {
  default:
    break;
  case pShowBalance:
    printCurrentBalance(This);
    process_confirm(This);
    break;
  }
}

static void updateStatusWidget_fromELpayRes(MainWidget_t * This, ELpayOperationStatus_t res)
{
  LOG_FXN(DBG, START);

  if (res)
  {
    char msg[256];
    switch (res)
    {
    default:
      snprintf(msg, sizeof(msg), "\n   %s", elpay_status_str_rus(res));
      break;

    case elpay_status_ServerResult:
      snprintf(msg, sizeof(msg), "\n %s #%d", elpay_status_str_rus(res), This->elpayTerminal->last_res);
      break;
    }
    StatusWidget_showMsg(&This->StatusWidget, msg);
  }
#ifndef GUI_MAINWIDGET_MANUAL_CONFIRM
  else
    process_confirm(This);
#endif

  LOG_FXN(DBG, OK);
}

static int find_prodIdx_byId(MainWidget_t * This, int pid)
{
  for (size_t i = 0; i < This->elpayProdList.ProductsCount; ++i)
    if (This->elpayProdList.Products[i].id == pid)
      return i;
  return -1;
}

static void tran_state_err_subhandler(MainWidget_t * This, char * statusMsg, size_t statusMsgSize)
{
//  status_msg_append(statusMsg, statusMsgSize, This->elpayTerminal->tran.state.errMsg);
}
static void tran_state_handler(MainWidget_t * This)
{
  char statusMsg[256];
  snprintf(statusMsg, sizeof(statusMsg), "\n ответ сервера:\n\n %s (#%d)",
    elpay_transtate_str_rus(This->elpayTerminal->tran.state.id),
    This->elpayTerminal->tran.state.id);

  switch (This->elpayTerminal->tran.state.id)
  {
  case elpay_tnst_Unknown:
    LOG_FXN_MSG(WRN,NONE, "Unknown transaction state");
    This->elpayStatus = elpay_status_ResponseFailure;
    break;

  case elpay_tnst_Created:
  case elpay_tnst_NoConfirm:
  case elpay_tnst_Processing:
    tran_state_err_subhandler(This, statusMsg, sizeof(statusMsg));
    break;

  case elpay_tnst_Canceled:
  case elpay_tnst_Canceled2:
    tran_state_err_subhandler(This, statusMsg, sizeof(statusMsg));
    break;

  case elpay_tnst_Successfull:
    break;
  }

  StatusWidget_showMsg(&This->StatusWidget, statusMsg);
}

//-------------------------------------------------------------

static void reg_go(MainWidget_t * This)
{
  MainWidget_goRegistration(This, This->formReg.regKeyBuf);
}

static void auth_go(MainWidget_t * This)
{
  MainWidget_goAuthorization(This, This->formAuth.loginBuf, This->formAuth.passwordBuf);
}

static void operation_go(MainWidget_t * This)
{
  MainWidget_CreateOperation(This);
}

static void authdata_changed_handler(MainWidget_t * This)
{
  This->authdataChanged = 1;
}

static void get_report_sales_go(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetRepSales, mwSrvcs, mwSrvcs);

  char report[65536];
  This->elpayStatus = ELpayTerminal_get_report_sales(This->elpayTerminal,
                                 This->repParams.bufDate1, This->repParams.bufDate2, This->repSalesIsDetailed,
                                 report, sizeof(report));

  if (!This->elpayStatus)
    MainWidget_printHtmlReport(This, process_name[pGetRepSales], report);
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return;
  }
  LOG_FXN(DBG, OK);
}

static void get_report_financial_go(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetRepFinancial, mwSrvcs, mwSrvcs);

  char report[65536];
  This->elpayStatus = ELpayTerminal_get_report_financial(This->elpayTerminal,
                                 This->repParams.bufDate1,
                                 report, sizeof(report));

  if (!This->elpayStatus)
    MainWidget_printHtmlReport(This, process_name[pGetRepFinancial], report);
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return;
  }
  LOG_FXN(DBG, OK);
}

static void get_report_encashment_go(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetRepEncashment, mwSrvcs, mwSrvcs);

  char report[65536];
  This->elpayStatus = ELpayTerminal_get_report_encashment(This->elpayTerminal,
                                 This->repParams.bufDate1,
                                 report, sizeof(report));

  if (!This->elpayStatus)
    MainWidget_printHtmlReport(This, process_name[pGetRepEncashment], report);
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return;
  }
  LOG_FXN(DBG, OK);
}

static void get_report_turn_go(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetRepTurn, mwSrvcs, mwSrvcs);

  char report[65536];
  This->elpayStatus = ELpayTerminal_get_report_turn(This->elpayTerminal,
                                 report, sizeof(report));

  if (!This->elpayStatus)
    MainWidget_printHtmlReport(This, process_name[pGetRepTurn], report);
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return;
  }
  LOG_FXN(DBG, OK);
}

//-------------------------------------------------------------

bool MainWidget_goRegistration(MainWidget_t * This, const char * regkey)
{
  LOG_FXN(DBG, START);

  init_Process(This, pReg, mwAuth, mwReg);

  This->elpayStatus =
      ELpayTerminal_registration(This->elpayTerminal,
          regkey);

  if ((!This->elpayStatus) && (This->terminal_changed))
    This->terminal_changed(This->obj.Parent, This->elpayTerminal->id, This->elpayTerminal->SignKey);

  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return false;
  }
  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_goAuthorization(MainWidget_t * This, const char * login, const char * password)
{
  LOG_FXN(DBG, START);

  init_Process(This, pAuth, mwSrvcs, mwAuth);

  This->elpayStatus =
      ELpayTerminal_authorization(This->elpayTerminal,
         login, password);

  if ((!This->elpayStatus) && (This->authdataChanged) && (This->authdata_changed))
    This->authdata_changed(This->obj.Parent, This->formAuth.loginBuf, This->formAuth.passwordBuf);

  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return false;
  }

  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_loadProdList(MainWidget_t * This)
{
  bool res = ELpayTerminal_loadProdList(This->elpayTerminal, &This->elpayProdList);
  if (res)
  {
    ServicesMenu_setCount(&This->menuServices, This->elpayProdList.ServicesCount);
    ProductsMenu_setProductsCount(&This->menuProducts, This->elpayProdList.ProductsCount);
  }
  return res;
}

bool MainWidget_updateProductsList(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetProdList, mwSrvcs, mwSrvcs);
  MainWidget_setMode(This, mwStatus);

  This->elpayStatus =
    ELpayTerminal_get_products_list(This->elpayTerminal, &This->elpayProdList);
  if (!This->elpayStatus)
  {
    ServicesMenu_setCount(&This->menuServices, This->elpayProdList.ServicesCount);
    ProductsMenu_setProductsCount(&This->menuProducts, This->elpayProdList.ProductsCount);
  }
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return false;
  }

#ifdef MAINWIDGET_OUTPUT_PRODUCTS_TO_LOG
  LOG(DBG, "services count = %d, products count = %d", This->elpayProdList.ServicesCount, This->elpayProdList.ProductsCount);
  for (size_t i = 0; i < This->elpayProdList.ProductsCount; ++i)
    LOG(DBG, "Product: id = %d, name_ru = %s", This->elpayProdList.Products[i].id, This->elpayProdList.Products[i].name[elpay_RU]);
#endif

  LOG_FXN(DBG, OK);
}

bool MainWidget_CreateOperation(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pCreateOp, mwSrvcs, mwProd);

  This->elpayStatus =
    ELpayTerminal_create_operation(This->elpayTerminal, &This->formProd.elpayProductData);
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return false;
  }

  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_ConfirmOperation(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pConfirmOp, mwSrvcs, mwProd);

  This->elpayStatus =
    ELpayTerminal_confirm_operation(This->elpayTerminal);
  if (This->elpayStatus)
  {
    updateStatusWidget_fromELpayRes(This, This->elpayStatus);
    LOG_FXN(DBG, FAIL);
    return false;
  }

  tran_state_handler(This);
  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_getOperationStatus(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  init_Process(This, pGetOpStatus, mwSrvcs, mwProd);

  This->elpayStatus =
    ELpayTerminal_get_operation_status(This->elpayTerminal);
  if (This->elpayStatus)
  {
    updateStatusWidget_fromELpayRes(This, This->elpayStatus);
    LOG_FXN(DBG, FAIL);
    return false;
  }

  tran_state_handler(This);
  LOG_FXN(DBG, OK);
  return true;
}

void MainWidget_getReportSales(MainWidget_t * This, bool isDetailed)
{
  This->repSalesIsDetailed = isDetailed;
  ReportParamsForm_setMode(&This->repParams,true);
  ReportParamsForm_setFormat(&This->repParams,ELPAY_REPORT_SALES_DATE_FORMAT);
  This->repParams.go = (GUI_fxn_t)get_report_sales_go;
  MainWidget_setMode(This, mwRepParams);
}

void MainWidget_getReportFinancial(MainWidget_t * This)
{
  ReportParamsForm_setMode(&This->repParams,false);
  ReportParamsForm_setFormat(&This->repParams,ELPAY_REPORT_FINANCIAL_DATE_FORMAT);
  This->repParams.go = (GUI_fxn_t)get_report_financial_go;
  MainWidget_setMode(This, mwRepParams);
}

void MainWidget_getReportTurn(MainWidget_t * This)
{
  get_report_turn_go(This);
}

void MainWidget_getReportEncashment(MainWidget_t * This)
{
  ReportParamsForm_setMode(&This->repParams,false);
  ReportParamsForm_setFormat(&This->repParams,ELPAY_REPORT_ENCASHMENT_DATE_FORMAT);
  This->repParams.go = (GUI_fxn_t)get_report_encashment_go;
  MainWidget_setMode(This, mwRepParams);
}

#define PRINTCYR_INIT \
  char buf[1024]; \
  char val_name[32];

#define PRINTCYR_bufX_WITH_SIZE(X) X,sizeof(X)
#define PRINTCYR_BUF_WITH_SIZE PRINTCYR_bufX_WITH_SIZE(buf)
#define PRINTCYR_VAL_WITH_SIZE PRINTCYR_bufX_WITH_SIZE(buf)
#define PRINTCYR_VALNAME_WITH_SIZE PRINTCYR_bufX_WITH_SIZE(val_name)

#define PRINTCYR_BUF buf
#define PRINTCYR_VAL buf
#define PRINTCYR_VALNAME val_name

static int IndexOf(char * str, char sym)
{
  size_t i;
  for (i = 0; i < strlen(str); ++i)
    if (str[i] == sym)
      return i;
  return -1;
}

bool MainWidget_printHtmlReport(MainWidget_t * This, const char * name, char * report)
{
  LOG_FXN(DBG, START);

  StatusWidget_showNewMsg(&This->StatusWidget, name, "\n печать...");

  PRINTCYR_INIT

  printer_setFont(16);
#if 0
  tr_cyr(name,PRINTCYR_BUF_WITH_SIZE);
  printer_print(PRINTCYR_BUF);
#endif
  printer_print("\n"PRINTER_SEP"\n");

  char * line;
  char *result = strstr(report, "<body>");
  int position = result - report;
  int substringLength = strlen(report) - position;
  int i = 0;
  for (i = 0; i < substringLength - 6; i++)
    report[i] = report[i + position + 6];
  memset(report + substringLength - 6, 0, position + 6);
  result = NULL;
  result = strstr(report, "<tr>");
  while (result != NULL)
  {
    position = result - report;
    substringLength = strlen(report) - position;
    report[position] = '\n';
    for (i = 0; i < substringLength - 4; i++)
      report[position + i + 1] = report[position + i + 4];
    memset(report + strlen(report) - 3, 0, 3);
    result = NULL;
    result = strstr(report, "<tr>");
  }
  result = NULL;
  result = strstr(report, "</table>");
  while (result != NULL)
  {
    position = result - report;
    substringLength = strlen(report) - position;
    report[position] = '\n';
    for (i = 0; i < substringLength - 8; i++)
      report[position + i + 1] = report[position + i + 8];
    memset(report + strlen(report) - 7, 0, 7);
    result = NULL;
    result = strstr(report, "</table>");
  }
  result = NULL;
  result = strstr(report, "</td><td>");
  while (result != NULL)
  {
    position = result - report;
    substringLength = strlen(report) - position;
    char * sep = ":\n";
    strcpy(report + position, sep);
    size_t sepSize = strlen(sep);
    for (i = 0; i < substringLength - 9; i++)
      report[position + i + sepSize] = report[position + i + 9];
    memset(report + strlen(report) - 9 + sepSize, 0, 9 - sepSize);
    result = NULL;
    result = strstr(report, "</td><td>");
  }
  LOG(INF, "html report:\n%s", (report));
  while (report[0] != 0)
  {
    int endOfLine = IndexOf(report,'\n');
    if (endOfLine == -1)
    {
      line = NULL;
      break;
    }

    line = strndup(report, endOfLine);
    if (line == NULL)
      break;

    report += strlen(line) + 1;
    char center = 0;
    char big = 0;
    int startOfTag = 0;
    while (IndexOf(line, '<') != -1)
    {
      startOfTag = IndexOf(line, '<');
      int endOfTag = IndexOf(line, '>');
      if (endOfTag == -1) break; // bad string
      char * fn = strndup(line + startOfTag + 1, endOfTag - startOfTag - 1);
      if (strcmp(fn, "center") == 0)
      { center = 1; }
      else if (strcmp(fn, "b") == 0)
      { big = 1; }
      else if (strcmp(fn, "h3") == 0)
      { big = 1; }
      int i = 0;
      for (i = 0; i < strlen(line) - endOfTag - 1; i++)
        line[startOfTag + i] = line[endOfTag + i + 1];
      memset(line + strlen(line) - (endOfTag - startOfTag + 1), 0, endOfTag - startOfTag + 1);
    }
    if (line != NULL)
    {
      LOG(DBG, "html report parse line: %s", (line));

      printer_setFont(/*big ? 24 :*/ 16);
      tr_cyr(line,PRINTCYR_BUF_WITH_SIZE);
      printer_print(PRINTCYR_BUF);

      free(line);
    }
  }
  printer_setFont(16);
  printer_print(PRINTER_SEP"\n"PRINTER_SCROLL);

  StatusWidget_showMsg(&This->StatusWidget, "\n  готово");
  This->Process.type = pEnd;

  LOG_FXN(DBG, OK);
  return 0;
}

#define PRINT_RCPT_VAL(name, val) \
{ \
  printer_setFont(8); \
  PRINTER_RAW_ADDSTR(" "); \
  PRINTER_RAW_ADDSTR(name); \
  printer_setFont(16); \
  PRINTER_RAW_ADDSTR(" "); \
  PRINTER_RAW_ADDSTR(val); \
  PRINTER_RAW_PRINT; \
}

bool MainWidget_printReceipt(MainWidget_t * This, ELpayProductData_t * data)
{
  LOG_FXN(DBG, START);

  StatusWidget_showNewMsg(&This->StatusWidget, " ТОВАРНЫЙ ЧЕК", "\n печать...");

  PRINTCYR_INIT

  printer_setFont(16);
  tr_cyr(" ТОВАРНЫЙ ЧЕК\n"PRINTER_SEP"\n"
         "КАРТА\n" "ПОПОЛНЕНИЕ СЧЕТА\n",
         PRINTCYR_BUF_WITH_SIZE);
  printer_print(PRINTCYR_BUF);

  tr_cyr("Терминал:", PRINTCYR_VALNAME_WITH_SIZE);
  PRINT_RCPT_VAL(PRINTCYR_VALNAME, PARAMS("%s", This->elpayTerminal->id));

  int pIdx = find_prodIdx_byId(This, data->pid);

  tr_cyr("Продукт:", PRINTCYR_VALNAME_WITH_SIZE);
  tr_cyr(This->elpayProdList.Products[pIdx].name[elpay_RU], PRINTCYR_VAL_WITH_SIZE);
  PRINT_RCPT_VAL(PRINTCYR_VALNAME, PRINTCYR_VAL);

  for (size_t i = 0; i < data->fieldsCount; ++i)
  {
    tr_cyr(This->elpayProdList.Products[pIdx].fields[i].name[elpay_RU], PRINTCYR_VALNAME_WITH_SIZE);
    PRINT_RCPT_VAL(PRINTCYR_VALNAME, data->fields[i].value);
  }
  printer_print("\n");

  char tmpbuf[33];
  tr_cyr("Cумма:", PRINTCYR_VALNAME_WITH_SIZE);
  dtostrf_p(data->nominal,2,tmpbuf,sizeof(tmpbuf));
  PRINT_RCPT_VAL(PRINTCYR_VALNAME, tmpbuf);

  tr_cyr("Комиссия:", PRINTCYR_VALNAME_WITH_SIZE);
  dtostrf_p(data->commission,2,tmpbuf,sizeof(tmpbuf));
  PRINT_RCPT_VAL(PRINTCYR_VALNAME, tmpbuf);

  printer_print("----------\n");

  tr_cyr("Всего:", PRINTCYR_VALNAME_WITH_SIZE);
  dtostrf_p(data->nominal+data->commission,2,tmpbuf,sizeof(tmpbuf));
  PRINT_RCPT_VAL(PRINTCYR_VALNAME, tmpbuf);

  tr_cyr("Сохраните чек\nдо зачисления средств", PRINTCYR_BUF_WITH_SIZE);
  printer_print(PRINTCYR_BUF);
  printer_print(PRINTER_SEP"\n"PRINTER_SCROLL);

  StatusWidget_showMsg(&This->StatusWidget, "\n  готово");
  This->Process.type = pEnd;

  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_showBalance(MainWidget_t * This)
{
  LOG_FXN(DBG, START);

  MainWidgetMode_t mode = This->mode == mwCtrlMenu ? This->modePrev : mwCurrentMode;
  init_Process(This, pShowBalance, mode, mode);

  This->elpayStatus =
    ELpayTerminal_get_balance(This->elpayTerminal, &This->balance);

  if (!This->elpayStatus)
  {
    SET_OUTMSG(This, " БАЛАНС",
      PARAMS(" %s\n\n сетевой: %s\n клиентский: %s\n кредит: %s",
        This->balance.date, This->balance.net, This->balance.client, This->balance.credit))
  }
  updateStatusWidget_fromELpayRes(This, This->elpayStatus);
  if (This->elpayStatus)
  {
    LOG_FXN(DBG, FAIL);
    return false;
  }
  LOG_FXN(DBG, OK);
  return true;
}

//-------------------------------------------------------------

static void draw(MainWidget_t * This)
{
  Lib_LcdCls();

  IndicatorsPanel_setTermId(&This->IndicatorsPanel, This->elpayTerminal->id);

  object_draw(This->currentObj);
  object_draw(&This->IndicatorsPanel.obj);
}

static void resize(MainWidget_t * This)
{
  object_resize(This->currentObj);
}

static void current_handler(MainWidget_t * This)
{
  object_current_handler(This->currentObj);
}

static void tick_handler(MainWidget_t * This)
{
  object_tick_handler(&This->IndicatorsPanel.obj);
  object_tick_handler(This->currentObj);
}

static void key_handler(MainWidget_t * This, BYTE key, BYTE isNew)
{
  switch(key)
  {
  case KEYCANCEL:
    switch(This->mode)
    {
    default:
      MainWidget_setMode(This, mwExitMenu);
      return;

    case mwReg:
      system_exit();
      return;

    case mwAuth:
      MainWidget_setMode(This, mwReg);
      return;

    case mwStatus:
      process_cancel(This);
      return;

    case mwProds:
      MainWidget_setMode(This, mwSrvcs);
      return;

    case mwProd:
      MainWidget_setMode(This, mwProds);
      return;

    case mwRepParams:
    case mwCtrlMenu:
    case mwOpList:
    case mwExitMenu:
      MainWidget_setMode(This, This->modePrev);
      return;
    }
    break;

  case KEYENTER:
    switch(This->mode)
    {
    default:
      break;

    case mwSrvcs:
      MainWidget_updateProductsList(This);
      return;
    }
    break;

  case KEYMENU:
    switch(This->mode)
    {
    default:
      break;

    case mwProds:
    case mwSrvcs:
      MainWidget_setMode(This, mwCtrlMenu);
      return;
    }
    break;
  }
  object_key_handler(This->currentObj, key, isNew);
}

static void key_edit_finished(MainWidget_t * This)
{
  object_key_edit_finished(This->currentObj);
}

void init_MainWidget(MainWidget_t * This, ELpayTerminal_t * elpayTerm)
{
  LOG_FXN(DBG, START);

  init_Object(&This->obj, This, "MainWidget");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;

  This->currentObj = 0;

  This->elpayTerminal = elpayTerm;

  init_IndicatorsPanel(&This->IndicatorsPanel);
  init_StatusWidget(&This->StatusWidget);
  init_ExitMenu(&This->menuExit, This);
  init_ControlMenu(&This->menuCtrl, This);
  init_RegistrationForm(&This->formReg);
  init_AuthorizationForm(&This->formAuth);
  init_ServicesMenu(&This->menuServices, This->elpayProdList.Services);
  init_ProductsMenu(&This->menuProducts, This->elpayProdList.Products);
  init_ProductForm(&This->formProd);
  init_OperationList(&This->opList);
  init_ReportParamsForm(&This->repParams);

  LOG_FXN_MSG(DBG,NONE, "set parents...");
  Object_setParent(&This->IndicatorsPanel.obj, This);
  Object_setParent(&This->StatusWidget.obj, This);
  Object_setParent(&This->menuExit.obj, This);
  Object_setParent(&This->menuCtrl.obj, This);
  Object_setParent(&This->formReg.obj, This);
  Object_setParent(&This->formAuth.obj, This);
  Object_setParent(&This->menuServices.obj, This);
  Object_setParent(&This->menuProducts.obj, This);
  Object_setParent(&This->formProd.obj, This);
  Object_setParent(&This->opList.obj, This);
  Object_setParent(&This->repParams.obj, This);
  LOG_FXN_MSG(DBG,NONE, "set parents successfull");

  LOG_FXN_MSG(DBG,NONE, "set geometries...");
  BYTE w,h;
  Lib_LcdGetSize(&w, &h);
  BYTE h0 = 8, h1 = h0+1;
  Object_setGeometry(&This->IndicatorsPanel.obj, 0,0,w,h0);
  Object_setGeometry(&This->StatusWidget.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->menuExit.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->menuCtrl.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->formReg.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->formAuth.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->menuServices.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->menuProducts.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->formProd.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->opList.obj, 0,h1,w,h-h1);
  Object_setGeometry(&This->repParams.obj, 0,h1,w,h-h1);
  LOG_FXN_MSG(DBG,NONE, "set geometries successfull");

  This->authdataChanged = 0;
  This->currentProdIdx = -1;

  This->formReg.go = (GUI_fxn_t)reg_go;
  This->formAuth.go = (GUI_fxn_t)auth_go;
  This->formAuth.data_changed = (GUI_fxn_t)authdata_changed_handler;
  This->StatusWidget.cancel = (GUI_fxn_t)process_cancel;
  This->StatusWidget.confirm = (GUI_fxn_t)process_confirm;
  This->StatusWidget.ok_handler = (GUI_fxn_t)process_ok_handler;
  This->menuServices.elpay_service_action = (elpay_service_action_fxn_t) MainWidjet_openProductsList;
  This->menuProducts.elpay_product_action = (elpay_product_action_fxn_t) MainWidjet_openProductForm;
  This->formProd.eplay_product_data_ready = (GUI_fxn_t)operation_go;
  This->repParams.go = (GUI_fxn_t)0;

  This->terminal_changed = 0;
  This->authdata_changed = 0;

  This->mode = -1;
  This->modePrev = -1;

  reset_OutMsg(This);

  IndicatorsPanel_setTermId(&This->IndicatorsPanel, This->elpayTerminal->id);

  LOG_FXN(DBG, OK);
}

//-------------------------------------------------------------

MainWidgetMode_t MainWidget_prevMode(MainWidget_t* This)
{
  return This->modePrev;
}

MainWidgetMode_t MainWidget_Mode(MainWidget_t* This)
{
  return This->mode;
}

void MainWidget_setMode(MainWidget_t * This, MainWidgetMode_t mode)
{
  LOG_FXN_PREMSG(DBG, START, " mode = %d", mode);

  if (mode != mwCurrentMode)
  if (This->mode != mode)
  {
    if (This->mode != mwCtrlMenu)
      This->modePrev = This->mode;
    This->mode = mode;
  }
  switch (mode)
  {
  default:
    LOG_FXN(WRN,FAIL);
    return;

  case mwReg:
    This->currentObj = &This->formReg.obj;
    break;

  case mwAuth:
    This->currentObj = &This->formAuth.obj;
    break;

  case mwStatus:
    This->currentObj = &This->StatusWidget.obj;
    break;

  case mwSrvcs:
    This->currentObj = &This->menuServices.obj;
    break;

  case mwProds:
    This->currentObj = &This->menuProducts.obj;
    break;

  case mwProd:
    This->currentObj = &This->formProd.obj;
    break;

  case mwExitMenu:
    This->currentObj = &This->menuExit.obj;
    break;

  case mwCtrlMenu:
    reset_ControlMenu(&This->menuCtrl);
    This->currentObj = &This->menuCtrl.obj;
    break;

  case mwOpList:
    This->currentObj = &This->opList.obj;
    break;

  case mwRepParams:
    This->currentObj = &This->repParams.obj;
    break;
  }
  object_current_handler(This->currentObj);
  object_draw(&This->obj);

  LOG_FXN(DBG, OK);
}

bool MainWidget_setCurrentObj(MainWidget_t * This, Object_t * obj)
{
  LOG_FXN(DBG, START);

  if (obj == &This->formReg.obj)
    This->mode = mwReg;
  else if (obj == &This->formAuth.obj)
    This->mode = mwAuth;
  else if (obj == &This->StatusWidget.obj)
    This->mode = mwStatus;
  else if (obj == &This->menuServices.obj)
    This->mode = mwSrvcs;
  else if (obj == &This->menuProducts.obj)
    This->mode = mwProds;
  else if (obj == &This->formProd.obj)
    This->mode = mwProd;
  else if (obj == &This->menuExit.obj)
    This->mode = mwExitMenu;
  else if (obj == &This->menuCtrl.obj)
    This->mode = mwCtrlMenu;
  else if (obj == &This->opList.obj)
    This->mode = mwOpList;
  else if (obj == &This->repParams.obj)
    This->mode = mwRepParams;
  else
  {
    LOG_FXN_MSG(WRN, FAIL, "invalid obj = %p", obj);
    return false;
  }
  LOG_FXN_MSG(DBG,NONE, "mode=%d", This->mode);

  This->currentObj = obj;
  object_current_handler(This->currentObj);
  object_draw(&This->obj);

  LOG_FXN(DBG, OK);
  return true;
}

void MainWidjet_openProductsList(MainWidget_t * This, int service_id)
{
  LOG_FXN(DBG,START);
  ProductsMenu_refreshItemsData(&This->menuProducts, service_id);
  MainWidget_setMode(This, mwProds);
  LOG_FXN(DBG,OK);
}

void MainWidjet_openProductForm(MainWidget_t * This, int product_id)
{
  LOG_FXN_PREMSG(DBG,START, " pid #%03d", product_id);
  This->currentProdIdx = find_prodIdx_byId(This, product_id);
  if (This->currentProdIdx < 0)
  {
    LOG_FXN(WRN,FAIL);
    return;
  }
  ProductForm_setELpayProduct(&This->formProd, &This->elpayProdList.Products[This->currentProdIdx]);
  MainWidget_setMode(This, mwProd);
  LOG_FXN(DBG,OK);
}

bool MainWidget_showNativeMsg(MainWidget_t * This, char * header, char * nativeMsg)
{
  LOG_FXN(DBG, START);

  init_Process(This, pShowMsg, mwCurrentMode, mwCurrentMode);

  StatusWidget_showNewNativeMsg(&This->StatusWidget, header, nativeMsg);
  This->Process.type = pEnd;

  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_showMsg(MainWidget_t * This, char * header, char * msg)
{
  LOG_FXN(DBG, START);

  init_Process(This, pShowMsg, mwCurrentMode, mwCurrentMode);

  SET_OUTMSG(This, header, msg)

  process_confirm(This);

  LOG_FXN(DBG, OK);
  return true;
}

bool MainWidget_printMsg(MainWidget_t * This, char * header, char * msg)
{
  LOG_FXN(DBG, START);

  init_Process(This, pPrintMsg, mwCurrentMode, This->mode);

  SET_OUTMSG(This, header, msg)

  process_confirm(This);

  LOG_FXN(DBG, OK);
  return true;
}

void MainWidget_showOperationList(MainWidget_t* This)
{
  OperationList_clear(&This->opList);
  for (int i = 0; i < ELpay_OperationList_ItemsCount(&This->elpayTerminal->opList); ++i)
  {
    ELpay_OperationListItem_t * item = ELpay_OperationList_Item(&This->elpayTerminal->opList,i);
    OperationList_appendItem(&This->opList, item->account, item->nominal, item->status);
  }
  MainWidget_setMode(This, mwOpList);
}
