/*
 * File:   StatusWidget.h
 */

#ifndef GUI_STATUSWIDGET_H
#define GUI_STATUSWIDGET_H

#include "SYSTEM/GUI/Object.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  Object_t obj;

  char Header[32], Msg[256];

  GUI_fxn_t cancel, confirm, ok_handler;
} StatusWidget_t;

void init_StatusWidget(StatusWidget_t * This);

void StatusWidget_setHeader(StatusWidget_t * This, const char * header);
void StatusWidget_setMsg(StatusWidget_t * This, const char * msg);

INLINE void StatusWidget_setOkHandler(StatusWidget_t * This, GUI_fxn_t handler){This->ok_handler = handler;}

void StatusWidget_showNewNativeMsg(StatusWidget_t * This, const char * header, const char * nativeMsg);
void StatusWidget_showNewMsg(StatusWidget_t * This, const char * header, const char * msg);
void StatusWidget_showMsg(StatusWidget_t * This, const char * msg);

#ifdef __cplusplus
}
#endif

#endif /* GUI_STATUSWIDGET_H */
