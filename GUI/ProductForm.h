/*
 * ProductForm.h
 *
 *  Created on: 23 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef GUI_PRODUCTFORM_H_
#define GUI_PRODUCTFORM_H_

#include "ELpay/types.h"

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/ListLayout.h"
#include "SYSTEM/GUI/Label.h"
#include "SYSTEM/GUI/LineEdit.h"

#ifdef __cplusplus
extern "C"{
#endif

#define GUI_NOMINAL_TEXT_BUFSIZE 16
#define GUI_PRODFORM_OBJSCOUNT 2

typedef struct
{
  Object_t obj;

  Label_t Header, Label;

  LineEdit_t Field;

  bool isNominalField;
  char NominalBuf[GUI_NOMINAL_TEXT_BUFSIZE];

  ListLayout_t Layout;
  Object_t * Objs[GUI_PRODFORM_OBJSCOUNT];

  ELpayProduct_t * elpayProduct;
  ELpayProductData_t elpayProductData;

  int currentFieldIdx;

  GUI_fxn_t eplay_product_data_ready;
} ProductForm_t;

void init_ProductForm(ProductForm_t * This);
void ProductForm_setELpayProduct(ProductForm_t * This, ELpayProduct_t * product);

#ifdef __cplusplus
}
#endif

#endif /* GUI_PRODUCTFORM_H_ */
