/* 
 * File:   CategoriesMenu.h
 */

#ifndef GUI_SERVICESMENU_H
#define GUI_SERVICESMENU_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/Menu.h"

#include "ELpay/types.h"

#ifdef __cplusplus
extern "C" {
#endif
  
#define GUI_SERVICESMENU_ITEMSBUFSIZE 100

typedef void (*elpay_service_action_fxn_t)(void * Parent, int id);

typedef struct
{
  Object_t obj;
  
  Menu_t menu;
  MenuItem_t Items[GUI_SERVICESMENU_ITEMSBUFSIZE];
  size_t ItemsCount;
  
  ELpayLang_t lang;
  ELpayService_t * elpayServices;

  elpay_service_action_fxn_t elpay_service_action;
} ServicesMenu_t;

void init_ServicesMenu(ServicesMenu_t * This, ELpayService_t * elpayServices);

void ServicesMenu_setCount(ServicesMenu_t * This, size_t count);
void ServicesMenu_refreshItemsData(ServicesMenu_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_SERVICESMENU_H */
