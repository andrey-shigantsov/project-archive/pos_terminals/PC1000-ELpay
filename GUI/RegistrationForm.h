/* 
 * File:   RegistrationForm.h
 */

#ifndef GUI_REGISTRATIONFORM_H
#define GUI_REGISTRATIONFORM_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/ListLayout.h"
#include "SYSTEM/GUI/Label.h"
#include "SYSTEM/GUI/LineEdit.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct
{
  Object_t obj;
  ListLayout_t Layout;
  Label_t labelRegKey;
  LineEdit_t regKey;
  
  #define GUI_REGFORM_OBJSCOUNT 2
  Object_t * Objs[GUI_REGFORM_OBJSCOUNT];
  Alignment_t  ObjsAlign[GUI_REGFORM_OBJSCOUNT];

  #define REGKEY_BUFSIZE (8+1)
  char regKeyBuf[REGKEY_BUFSIZE];
  
  GUI_fxn_t go;
} RegistrationForm_t;

void init_RegistrationForm(RegistrationForm_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_REGISTRATIONFORM_H */
