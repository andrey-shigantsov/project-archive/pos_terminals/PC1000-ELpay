#ifndef OPERATIONLIST_H
#define OPERATIONLIST_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/ListLayout.h"
#include "SYSTEM/GUI/Label.h"

#ifdef __cplusplus
extern "C"{
#endif

#define GUI_OPLIST_ITEM_OBJSCOUNT 4
typedef struct
{
  Object_t obj;

  Label_t Num, Account, Nominal, Status;

  ListLayout_t Layout;
  Object_t * Objs[GUI_OPLIST_ITEM_OBJSCOUNT];
  BYTE LayoutStretchs[GUI_OPLIST_ITEM_OBJSCOUNT];
} OperationListItem_t;

void init_OperationListItem(OperationListItem_t * This, const int Num, const char * Account, double Nominal, bool Status);

#define GUI_OPLIST_PAGE_ITEMS_COUNT 4
#define GUI_OPLIST_OBJSCOUNT GUI_OPLIST_PAGE_ITEMS_COUNT

#define GUI_OPLIST_MAX_ITEMS_COUNT 256u

typedef struct
{
  Object_t obj;
  ListLayout_t Layout;
  Object_t * Objs[GUI_OPLIST_OBJSCOUNT];

  Label_t Header;

  OperationListItem_t Items[GUI_OPLIST_MAX_ITEMS_COUNT];
  int ItemsCount, currentItemIdx, pageItemsCount;
} OperationList_t;

void init_OperationList(OperationList_t * This);

INLINE int OperationList_ItemsCount(OperationList_t * This){return This->ItemsCount;}

void OperationList_clear(OperationList_t * This);

void OperationList_appendItem(OperationList_t * This, const char * Account, double Nominal, bool Status);

#ifdef __cplusplus
}
#endif

#endif // OPERATIONLIST_H
