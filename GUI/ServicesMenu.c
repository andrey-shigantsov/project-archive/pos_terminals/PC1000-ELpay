/* 
 * File:   CategoriesMenu.c
 */

#include "GUI/ServicesMenu.h"

#define LOG_HEADER "GUI/ServicesMenu"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

#include <stdio.h>
#include <assert.h>

static void draw(ServicesMenu_t * This)
{
	object_draw(&This->menu.obj);
}

static void key_handler(ServicesMenu_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  default:
    object_key_handler(&This->menu.obj, key, isNew);
    break;
  }
}

static void menu_item_action(ServicesMenu_t * This, int idx)
{
  LOG_FXN_MSG(DBG,NONE, "idx = %d, service id = %d", idx, This->elpayServices[idx].id);
  if (This->elpay_service_action && (idx >= 0) && (idx <= This->ItemsCount))
    This->elpay_service_action(This->obj.Parent, This->elpayServices[idx].id);
}

static void current_handler(ServicesMenu_t * This)
{
  object_current_handler(&This->menu.obj);
}

static void resize(ServicesMenu_t * This)
{
  Object_copySizeParams(&This->menu.obj, &This->obj);
}

void ServicesMenu_setCount(ServicesMenu_t * This, size_t count)
{
  assert(count <= GUI_SERVICESMENU_ITEMSBUFSIZE);

  This->ItemsCount = count;
  ServicesMenu_refreshItemsData(This);
}
void ServicesMenu_refreshItemsData(ServicesMenu_t * This)
{
  for (BYTE i = 0; i < This->ItemsCount; ++i)
  {
    MenuItem_t * item = &This->Items[i];
    init_MenuItem(item, &This->menu);
    char name[32];
    tr_cyr(This->elpayServices[i].name[This->lang], name, sizeof(name));
    snprintf(MenuItem_Text(item), GUI_MENU_LABEL_BUFLEN+1,
      //"sid=%03d", This->elpayServices[i].id);
      name);
  }
  Menu_setItems(&This->menu, This->Items, This->ItemsCount);
}

void init_ServicesMenu(ServicesMenu_t * This, ELpayService_t * elpayServices)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "ServMenu");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  
//  Object_setMargins(&This->obj, 1,1,1,1);
  
  init_Menu(&This->menu);
  tr_cyr(" СЕРВИСЫ", Menu_Header(&This->menu), GUI_LABEL_BUFSIZE);
//  Menu_enableHeader(&This->menu, true);
  Object_setParent(&This->menu.obj, This);
  This->menu.item_action = (menu_item_action_fxn_t)menu_item_action;

  This->ItemsCount = 0;

  This->lang = elpay_RU;
  This->elpayServices = elpayServices;

  This->elpay_service_action = 0;

  LOG_FXN(DBG,OK);
}
