/*
 * ControlMenu.h
 *
 *  Created on: 30 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef GUI_EXITMENU_H_
#define GUI_EXITMENU_H_

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/Menu.h"

typedef struct ExitMenu_s ExitMenu_t;

#ifdef __cplusplus
extern "C"{
#endif

#define GUI_EXITMENU_ITEMSBUFSIZE 3

struct ExitMenu_s
{
  Object_t obj;

  Menu_t menu;
  MenuItem_t Items[GUI_EXITMENU_ITEMSBUFSIZE];
  size_t ItemsCount;

  void * mw;
};

void init_ExitMenu(ExitMenu_t * This, void * mw);
#ifdef __cplusplus
}
#endif

#endif /* GUI_EXITMENU_H_ */
