/*
 * ProductForm.c
 *
 *  Created on: 23 мар. 2017 г.
 *      Author: Svetozar
 */

#include "ProductForm.h"
#define LOG_HEADER "GUI/ProductForm"
#define LOG_LEVEL GUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/tr.h"

#include <string.h>

static void draw(ProductForm_t * This)
{
  Object_clearDrawBox(&This->obj);
  object_draw(&This->Header.obj);
  object_draw(&This->Layout.obj);
}

static void refresh_field_keysmode(ProductForm_t * This)
{
  if (This->isNominalField)
    setKeysMode(keysMode_Float);
  else if (This->elpayProduct->fieldsCount > 0)
    switch(This->elpayProduct->fields[This->currentFieldIdx].id)
    {
    case 1:
      setKeysMode(keysMode_Int);
      break;
    default:
      setKeysMode(keysMode_All);
      break;
    }
}

static void refresh_field(ProductForm_t * This)
{
  if (This->isNominalField)
  {
    Label_setTextCyr(&This->Label, "Сумма");
    LineEdit_setTextBufWithLen(&This->Field, This->NominalBuf, GUI_NOMINAL_TEXT_BUFSIZE, strlen(This->NominalBuf));
  }
  else if (This->elpayProduct->fieldsCount > 0)
  {
    Label_setTextCyr(&This->Label, This->elpayProduct->fields[This->currentFieldIdx].name[elpay_RU]);
    assert(This->elpayProduct->fields[This->currentFieldIdx].charMax <= ELPAY_FIELD_VALUE_BUFSIZE);
    LineEdit_setTextBufWithLen(&This->Field, This->elpayProductData.fields[This->currentFieldIdx].value, This->elpayProduct->fields[This->currentFieldIdx].charMax, strlen(This->elpayProductData.fields[This->currentFieldIdx].value));
  }
  refresh_field_keysmode(This);
}

static void refresh_currentFieldIdx(ProductForm_t * This)
{
  if (This->currentFieldIdx >= This->elpayProduct->fieldsCount)
    This->currentFieldIdx = This->elpayProduct->fieldsCount - 1;
  if (This->currentFieldIdx < 0)
    This->currentFieldIdx = 0;
}

static void key_handler(ProductForm_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYUP:
    if (This->isNominalField)
    {
      This->isNominalField = false;
      This->currentFieldIdx = This->elpayProduct->fieldsCount - 1;
    }
    else
      --This->currentFieldIdx;
    if (This->elpayProduct->fieldsCount <= 0) break;
    refresh_currentFieldIdx(This);
    refresh_field(This);
    object_draw(&This->obj);
    break;

  case KEYENTER:
  case KEYOK:
    if (This->isNominalField)
    {
      char * pEnd;
      This->elpayProductData.nominal = strtod(This->NominalBuf, &pEnd);
      if (pEnd == This->NominalBuf)
      {
        LOG_FXN_MSG(INF, NONE, "Nominal read failure");
        break;
      }
      if (This->eplay_product_data_ready)
        This->eplay_product_data_ready(This->obj.Parent);
      break;
    }
  case KEYDOWN:
    if ((This->currentFieldIdx == This->elpayProduct->fieldsCount-1)||(This->elpayProduct->fieldsCount == 0))
      This->isNominalField = true;
    else
    {
      ++This->currentFieldIdx;
      refresh_currentFieldIdx(This);
    }
    refresh_field(This);
    object_draw(&This->obj);
    break;

  default:{
    object_key_handler(&This->Field.obj, key, isNew);
    break;}
  }
}

static void tick_handler(ProductForm_t * This)
{
  object_tick_handler(&This->Field.obj);
}

static void key_edit_finished(ProductForm_t * This)
{
  object_key_edit_finished(&This->Field.obj);
}

static void current_handler(ProductForm_t * This)
{
  refresh_field_keysmode(This);
}

static void resize(ProductForm_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);

  BYTE hH = This->Header.obj.font.size+This->Header.obj.margins.top+This->Header.obj.margins.bottom;
  Object_setGeometry(&This->Header.obj, g.x,g.y,g.w,hH);

  g.y += hH; g.h -= hH;
  Object_setGeometry(&This->Layout.obj, g.x,g.y,g.w,g.h);
}

static void clear_elpayProdData(ProductForm_t * This, int count)
{
  This->NominalBuf[0] = '\0';
  for (int i = 0; i < count; ++i)
    This->elpayProductData.fields[i].value[0] = '\0';
  This->elpayProductData.nominal = 0.0;
}

void init_ProductForm(ProductForm_t * This)
{
  LOG_FXN(DBG,START);

  init_Object(&This->obj, This, "ProdForm");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.current_handler = (GUI_fxn_t)current_handler;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;

  init_Label(&This->Header);
  Object_setMargins(&This->Header.obj,1,3,0,2);
  Label_setTextCyr(&This->Header, "ПРОДУКТ");
  This->Header.obj.font.isInverseColor = true;
  This->Header.obj.font.size = 8;

  init_Label(&This->Label);
  Object_setMargins(&This->Label.obj,3,0,0,0);

  init_LineEdit(&This->Field);
  LineEdit_setTextBuf(&This->Field, This->elpayProductData.fields[0].value, ELPAY_FIELD_VALUE_BUFSIZE);
  LineEdit_setSelection(&This->Field, 1);

  This->Objs[0] = &This->Label.obj;
  This->Objs[1] = &This->Field.obj;
  init_ListLayout(&This->Layout, orVertical, 0, This->Objs, GUI_PRODFORM_OBJSCOUNT);
  Object_setMargins(&This->Layout.obj, 0,1,1,0);

  This->elpayProduct = 0;
  This->currentFieldIdx = 0;
  This->isNominalField = false;
  clear_elpayProdData(This, ELPAY_MAX_FIELDS_COUNT);

  This->eplay_product_data_ready = 0;

  LOG_FXN(DBG,OK);
}

static void refresh_elpayProdDataBase(ProductForm_t * This)
{
  This->elpayProductData.pid = This->elpayProduct->id;
  This->elpayProductData.commission = This->elpayProduct->commission.value;
}

static void refresh_elpayProdDataFields(ProductForm_t * This)
{
  clear_elpayProdData(This, This->elpayProduct->fieldsCount);

  This->elpayProductData.fieldsCount = This->elpayProduct->fieldsCount;
  for (size_t i = 0; i < This->elpayProduct->fieldsCount; ++i)
    This->elpayProductData.fields[i].id = This->elpayProduct->fields[i].id;
}

void ProductForm_setELpayProduct(ProductForm_t * This, ELpayProduct_t * product)
{
  This->elpayProduct = product;
  This->currentFieldIdx = 0;

  Label_setTextCyr(&This->Header, product->name[elpay_RU]);

  refresh_elpayProdDataBase(This);

  This->isNominalField = (product->fieldsCount <= 0);
  assert(product->fieldsCount <= ELPAY_MAX_FIELDS_COUNT);
  if (product->fieldsCount)
  {
    refresh_elpayProdDataFields(This);
    refresh_currentFieldIdx(This);
  }
  refresh_field(This);
}
