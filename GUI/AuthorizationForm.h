/* 
 * File:   AuthorizationForm.h
 */

#ifndef GUI_AUTHORIZATIONFORM_H
#define GUI_AUTHORIZATIONMENU_H

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/TableLayout.h"
#include "SYSTEM/GUI/Label.h"
#include "SYSTEM/GUI/LineEdit.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  Object_t obj;
  TableLayout_t Layout;

  Label_t labelLogin, labelPassword;
  LineEdit_t login, password;

  int Selected;
  
  #define GUI_AUTHFORM_ROWCOUNT 2
  #define GUI_AUTHFORM_COLCOUNT 2
  Object_t * Objs[GUI_AUTHFORM_ROWCOUNT][GUI_AUTHFORM_COLCOUNT];
  Alignment_t  ObjsAlign[GUI_AUTHFORM_ROWCOUNT][GUI_AUTHFORM_COLCOUNT];
  Object_t ** ObjBuf[GUI_AUTHFORM_ROWCOUNT];
  Alignment_t * ObjAlignBuf[GUI_AUTHFORM_ROWCOUNT];
  BYTE ColStretch[GUI_AUTHFORM_COLCOUNT];

  #define GUI_AUTH_BUFSIZE (32+1)
  char loginBuf[GUI_AUTH_BUFSIZE], passwordBuf[GUI_AUTH_BUFSIZE];
  char flag_dataChanged;
  
  GUI_fxn_t data_changed;
  GUI_fxn_t go;
} AuthorizationForm_t;

void init_AuthorizationForm(AuthorizationForm_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_AUTHORIZATIONMENU_H */
