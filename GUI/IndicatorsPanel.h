/*
 * IndicatorsPanel.h
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef GUI_INDICATORSPANEL_H_
#define GUI_INDICATORSPANEL_H_

#include "SYSTEM/GUI/Object.h"
#include "SYSTEM/GUI/ListLayout.h"
#include "SYSTEM/GUI/Label.h"
#include "SYSTEM/GUI/NetworkIndicator.h"

#ifdef __cplusplus
extern "C"{
#endif

#define GUI_INDPAN_OBJSCOUNT 3

typedef struct
{
  Object_t obj;

  ListLayout_t Layout;
  Object_t * Objs[GUI_INDPAN_OBJSCOUNT];

  Label_t LabelTermId, TermId;
  NetworkIndicator_t Network;

  int RefreshTicksCount, RefreshTicksCounter;
} IndicatorsPanel_t;

void init_IndicatorsPanel(IndicatorsPanel_t * This);
INLINE void IndicatorsPanel_setTermId(IndicatorsPanel_t * This, const char * id){Label_setTextCyr(&This->TermId, id);}

void IndicatorsPanel_update(IndicatorsPanel_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_INDICATORSPANEL_H_ */
