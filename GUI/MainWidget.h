/*
 * MainWidget.h
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef GUI_MAINWIDGET_H_
#define GUI_MAINWIDGET_H_

#include "SYSTEM/GUI/Object.h"

typedef struct MainWidget_s MainWidget_t;

#include "GUI/IndicatorsPanel.h"
#include "GUI/StatusWidget.h"
#include "GUI/ExitMenu.h"
#include "GUI/ControlMenu.h"
#include "GUI/RegistrationForm.h"
#include "GUI/AuthorizationForm.h"
#include "GUI/ServicesMenu.h"
#include "GUI/ProductsMenu.h"
#include "GUI/ProductForm.h"
#include "GUI/OperationList.h"
#include "GUI/ReportParamsForm.h"

#include "SERVICES/ELpay/Terminal.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef void (*mainwidget_terminal_changed_f)(void * Parent, char * id, char * key);
typedef void (*mainwidget_authdata_changed_f)(void * Parent, char * lgn, char * pwd);

typedef enum {mwCurrentMode = -1, mwStatus, mwReg, mwAuth, mwExitMenu, mwCtrlMenu, mwSrvcs, mwProds, mwProd, mwOpList, mwRepParams} MainWidgetMode_t;
typedef enum {pReg, pAuth, pGetProdList, pCreateOp, pConfirmOp, pGetOpStatus, pShowBalance, pGetRepSales, pGetRepFinancial, pGetRepTurn, pGetRepEncashment, pShowMsg, pPrintMsg, pEnd} ProcessType_t;
typedef struct
{
  ProcessType_t type, etcType;
  MainWidgetMode_t NextMode, PrevMode;
} Process_t;

struct MainWidget_s
{
  Object_t obj, * currentObj;

  MainWidgetMode_t mode, modePrev;

  IndicatorsPanel_t IndicatorsPanel;
  StatusWidget_t StatusWidget;
  ControlMenu_t menuCtrl;
  ExitMenu_t menuExit;
  RegistrationForm_t formReg;
  AuthorizationForm_t formAuth;
  ServicesMenu_t menuServices;
  ProductsMenu_t menuProducts;
  ProductForm_t formProd;
  OperationList_t opList;
  ReportParamsForm_t repParams;

  ELpayTerminal_t * elpayTerminal;
  ELpayProdList_t elpayProdList;
  int currentProdIdx;

  char authdataChanged;
  ELpayOperationStatus_t elpayStatus;

  ELpayBalance_t balance;

  Process_t Process;

  struct
  {
    char header[256];
    char msg[512];
  } OutMsg;

  bool repSalesIsDetailed;

  mainwidget_terminal_changed_f terminal_changed;
  mainwidget_authdata_changed_f authdata_changed;
};

void init_MainWidget(MainWidget_t * This, ELpayTerminal_t * elpayTerm);

MainWidgetMode_t MainWidget_prevMode(MainWidget_t* This);
MainWidgetMode_t MainWidget_Mode(MainWidget_t * This);

void MainWidget_setMode(MainWidget_t * This, MainWidgetMode_t mode);
bool MainWidget_setCurrentObj(MainWidget_t * This, Object_t * obj);

bool MainWidget_goRegistration(MainWidget_t * This, const char * regkey);
bool MainWidget_goAuthorization(MainWidget_t * This, const char * login, const char * password);
bool MainWidget_loadProdList(MainWidget_t * This);
bool MainWidget_updateProductsList(MainWidget_t * This);
bool MainWidget_CreateOperation(MainWidget_t * This);
bool MainWidget_ConfirmOperation(MainWidget_t * This);
bool MainWidget_getOperationStatus(MainWidget_t * This);

void MainWidget_getReportSales(MainWidget_t * This, bool isDetailed);
void MainWidget_getReportFinancial(MainWidget_t * This);
void MainWidget_getReportTurn(MainWidget_t * This);
void MainWidget_getReportEncashment(MainWidget_t * This);

bool MainWidget_printReceipt(MainWidget_t * This, ELpayProductData_t * data);
bool MainWidget_printHtmlReport(MainWidget_t * This, const char* name, char * report);

void MainWidget_showOperationList(MainWidget_t * This);

bool MainWidget_showBalance(MainWidget_t * This);

void MainWidjet_openProductsList(MainWidget_t * This, int service_id);
void MainWidjet_openProductForm(MainWidget_t * This, int product_id);

bool MainWidget_showNativeMsg(MainWidget_t * This, char * header, char * nativeMsg);
bool MainWidget_showMsg(MainWidget_t * This, char * header, char * msg);
bool MainWidget_printMsg(MainWidget_t * This, char * header, char * msg);

#ifdef __cplusplus
}
#endif

#endif /* GUI_MAINWIDGET_H_ */
