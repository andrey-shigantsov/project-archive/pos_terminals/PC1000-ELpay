/*
 * types.h
 *
 *  Created on: 21 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef LIBS_ELPAY_TYPES_H_
#define LIBS_ELPAY_TYPES_H_

#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  const char * model;
  const char * os_ver;
  const char * imei;
  const char * phone_num;
  const char * lac;
  const char * cid;
} ELpayMVI_t;

typedef struct
{
  const char * code;
  ELpayMVI_t mvi;
  const char * pos_os_ver;
  const char * app_ver;
} ELpayRegReq_t;

typedef struct
{
  const char * login;
  const char * password;

  const char * termId;
  const char * pos_os_ver;
  const char * app_ver;
  ELpayMVI_t mvi;
} ELpayAuthReq_t;

typedef struct
{
  const char * termId;
  const char * session;
  const char * app_ver;
} ELpayBaseReq_t;

typedef struct
{
  ELpayBaseReq_t base;
} ELpayBalanceReq_t;

typedef struct
{
  ELpayBaseReq_t base;
} ELpayProdListReq_t;

typedef struct
{
  ELpayBaseReq_t base;
  const char * name;
} ELpayReceiptTemplReq_t;

typedef struct
{
  ELpayBaseReq_t base;
  long pid;
  long act;
  const char * msisdn;
  double nominal;
  double commission;
} ELpayCreateOpReq_t;

typedef struct
{
  ELpayBaseReq_t base;
  long tranId;
} ELpayConfirmOpReq_t;

typedef struct
{
  ELpayBaseReq_t base;
  long tranId;
} ELpayOpStatusReq_t;

#define ELPAY_YEAR_SIZE 4
#define ELPAY_MOUNTH_SIZE 2
#define ELPAY_DAY_SIZE 2
typedef struct
{
  char year[ELPAY_YEAR_SIZE+1];
  char mounth[ELPAY_MOUNTH_SIZE+1];
  char day[ELPAY_DAY_SIZE+1];
} ELpayStrDate_t;

#define ELPAY_REPORT_SALES_DATE_FORMAT "ггггммдд"
typedef struct
{
  ELpayBaseReq_t base;
  bool isDetailed;
#ifdef ELPAY_REPORT_DATE_IS_STRUCT
  ELpayStrDate_t date[2];
#else
  char * date1, * date2;
#endif
} ELpayReportSalesReq_t;

#define ELPAY_REPORT_FINANCIAL_DATE_FORMAT "ггггммдд"
typedef struct
{
  ELpayBaseReq_t base;
#ifdef ELPAY_REPORT_DATE_IS_STRUCT
  ELpayStrDate_t date;
#else
  char * date;
#endif
} ELpayReportFinancialReq_t;

typedef struct
{
  ELpayBaseReq_t base;
} ELpayReportTurnReq_t;

#define ELPAY_REPORT_ENCASHMENT_DATE_FORMAT "гггммддччМиМисс"
typedef struct
{
  ELpayBaseReq_t base;
#ifdef ELPAY_REPORT_DATE_IS_STRUCT
  ELpayStrDate_t date;
#else
  char * date;
#endif
}  ELpayReportEncashmentReq_t;

//-------------------------------------------------------------

#define ELPAY_JSON_KEYenc_SIZE 12
#define ELPAY_JSON_KEYenc_BUFSIZE (ELPAY_JSON_KEYenc_SIZE+1)
typedef char ELpayKeyEnc_t[ELPAY_JSON_KEYenc_BUFSIZE];

#define ELPAY_JSON_KEYSIZE 8
#define ELPAY_JSON_KEY_BUFSIZE (ELPAY_JSON_KEYSIZE+1)
typedef char ELpayKey_t[ELPAY_JSON_KEY_BUFSIZE];

#define ELPAY_JSON_TERM_IDSIZE 16
#define ELPAY_JSON_TERM_ID_BUFSIZE (ELPAY_JSON_TERM_IDSIZE+1)
typedef char ELpayTermId_t[ELPAY_JSON_TERM_ID_BUFSIZE];

#define ELPAY_JSON_SESSION_IDSIZE 32
#define ELPAY_JSON_SESSION_ID_BUFSIZE (ELPAY_JSON_SESSION_IDSIZE+1)
typedef char ELpaySessionId_t[ELPAY_JSON_SESSION_ID_BUFSIZE];

#define ELPAY_JSON_SIGNSIZE 8
#define ELPAY_JSON_SIGN_BUFSIZE (ELPAY_JSON_SIGNSIZE+1 /*для b64*/+1)
typedef char ELpaySign_t[ELPAY_JSON_SIGN_BUFSIZE];

#define ELPAY_JSON_SIGNenc_SIZE 12
#define ELPAY_JSON_SIGNenc_BUFSIZE (ELPAY_JSON_SIGNenc_SIZE+1)
typedef char ELpaySignEnc_t[ELPAY_JSON_SIGNenc_BUFSIZE];

typedef long long ELpayTransactionId_t;

typedef enum
{
  elpay_tnst_Unknown = -1,
  elpay_tnst_Created = 1,
  elpay_tnst_Canceled = 2,
  elpay_tnst_NoConfirm = 25,
  elpay_tnst_Successfull = 26,
  elpay_tnst_Canceled2 = 27,
  elpay_tnst_Processing = 28,
  elpay_tnst_Error = 29
} ELpayTransactionStateId_t;
#define ELPAY_JSON_TRANSTATE_ERRMSG_BUFSIZE 256
#define ELPAY_JSON_TRANSTATE_INFOMSG_BUFSIZE 512
typedef struct
{
  ELpayTransactionStateId_t id;
  char errMsg[ELPAY_JSON_TRANSTATE_ERRMSG_BUFSIZE];
  char infoMsg[ELPAY_JSON_TRANSTATE_INFOMSG_BUFSIZE];
} ELpayTransactionState_t;

typedef enum {elpay_RU = 0, elpay_UA, ELPAY_LANGS_COUNT} ELpayLang_t;

#define ELPAY_NAME_BUFSIZE 255u
#define ELPAY_NAMESHORT_BUFSIZE 32u

typedef struct
{
  int id, sort_order;

  char name[ELPAY_LANGS_COUNT][ELPAY_NAME_BUFSIZE];
} ELpayService_t;

#define ELPAY_MASK_BUFSIZE 255u
typedef struct
{
  int id;
  char name[ELPAY_LANGS_COUNT][ELPAY_NAME_BUFSIZE];
  char mask[ELPAY_MASK_BUFSIZE];
  int charMin, charMax;
} ELpayProductField_t;

typedef struct
{
  int nominalMin, nominalMax;
  int value, percent;
} ELpayProductCommission_t;

#define ELPAY_MAX_FIELDS_COUNT 16
typedef struct
{
  int id, sort_order;
  int sid;

  char name[ELPAY_LANGS_COUNT][ELPAY_NAME_BUFSIZE];
  char name_short[ELPAY_LANGS_COUNT][ELPAY_NAMESHORT_BUFSIZE];

  ELpayProductField_t fields[ELPAY_MAX_FIELDS_COUNT];
  int fieldsCount;

  ELpayProductCommission_t commission;
  char receipt_templ_name[ELPAY_NAME_BUFSIZE];
} ELpayProduct_t;
#define ELPAY_FIELD_VALUE_BUFSIZE 255u
typedef struct
{
  int id;
  char value[ELPAY_FIELD_VALUE_BUFSIZE];
} ELpayProductFieldData_t;
typedef struct
{
  int pid;
  ELpayProductFieldData_t fields[ELPAY_MAX_FIELDS_COUNT];
  size_t fieldsCount;
  double nominal, commission;
} ELpayProductData_t;

#define ELPAY_MAX_SERVCOUNT 100
#define ELPAY_MAX_PRODCOUNT 200
typedef struct
{
  ELpayService_t Services[ELPAY_MAX_SERVCOUNT];
  ELpayProduct_t Products[ELPAY_MAX_PRODCOUNT];

  size_t ServicesCount, ProductsCount;
} ELpayProdList_t;

#define ELPAY_JSON_DATE_BUFSIZE 24u
#define ELPAY_JSON_BALANCE_BUFSIZE 32u
typedef struct
{
  char
    date[ELPAY_JSON_DATE_BUFSIZE],
    net[ELPAY_JSON_BALANCE_BUFSIZE],
    client[ELPAY_JSON_BALANCE_BUFSIZE],
    credit[ELPAY_JSON_BALANCE_BUFSIZE];
    struct
    {
      char
        day24[ELPAY_JSON_BALANCE_BUFSIZE],
        day[ELPAY_JSON_BALANCE_BUFSIZE],
        operation[ELPAY_JSON_BALANCE_BUFSIZE];
    } limit;
} ELpayBalance_t;

const char * elpay_transtate_str(ELpayTransactionStateId_t state);
const char * elpay_transtate_str_rus(ELpayTransactionStateId_t state);

#ifdef __cplusplus
}
#endif

#endif /* LIBS_ELPAY_TYPES_H_ */
