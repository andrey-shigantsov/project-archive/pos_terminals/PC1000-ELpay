/* 
 * File:   json.h
 */

#ifndef JSON_H
#define JSON_H

#include <yajl_gen.h>
#include <yajl_tree.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define JSON_CHAR unsigned char

#define JSON_PATH_NULL (const char *)0
  
//-------------------------------------------------------------

typedef struct
{
  yajl_gen Generator;
} jsonGen_t;

int init_jsonGen(jsonGen_t* This);
void free_jsonGen(jsonGen_t* This);

void jsonGen_clear(jsonGen_t* This);

int json_gen_object_open(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_object_close(jsonGen_t* This);

int json_gen_array_open(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_array_close(jsonGen_t* This);

int json_gen_write_null(jsonGen_t* This, const JSON_CHAR * name);
int json_gen_write_bool(jsonGen_t* This, const JSON_CHAR * name, int val);
int json_gen_write_str(jsonGen_t* This, const JSON_CHAR * name, const JSON_CHAR * str);
int json_gen_write_llint(jsonGen_t* This, const JSON_CHAR * name, long long val);
int json_gen_write_double(jsonGen_t* This, const JSON_CHAR * name, double val);
int json_gen_write_number(jsonGen_t* This, const JSON_CHAR * name, JSON_CHAR * val);

int jsonGen_internal_buf(jsonGen_t* This, const JSON_CHAR ** buf, size_t * len);
size_t jsonGen_copy_to_buf(jsonGen_t* This, char * buf, size_t size);

//-------------------------------------------------------------

typedef struct
{
  yajl_val node;
  char * errBuf;
  size_t errBufSize;
} jsonTree_t;

void init_jsonTree(jsonTree_t* This, char * errBuf, size_t errBufSize);
void free_jsonTree(jsonTree_t* This);

int json_tree_parse(jsonTree_t* This, const char * buf);

yajl_val json_tree_get(jsonTree_t* This, const char ** path, yajl_type type);

//-------------------------------------------------------------

#ifdef  __cplusplus
}
#endif

#endif /* JSON_H */
