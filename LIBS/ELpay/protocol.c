/* 
 * File:   json_ELpay.c
 */

#include "protocol.h"
#include "json.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "ELpay_port.h"

#define MONEY_CONVERT(val) ((long long)(val*100))

int elpay_sign(const char * key, char * packet, char * sign, char * b64_sign)
{
	memset(sign, '\0', 9);
	int p = 0;
	size_t len = strlen(packet);
	int i, j;
	for (i = 0; i < (len + 7) / 8; i++) {
		if ((i == (((len + 7) / 8) - 1)) && ((len % 8) > 0)) 
    {
			for (j = 0; j < (len % 8); j++)
				sign[j] ^= packet[p++];
			for (j = len % 8; j < 8; j++)
				sign[j] ^= 0;
			elpay_des_encrypt((char*)key, sign, 8, sign, 9);
			break;
		}
		for (j = 0; j < 8; j++)
			sign[j] ^= packet[p++];
		elpay_des_encrypt((char*)key, sign, 8, sign, 9);
	}

	return elpay_base64_encode(sign, 8, b64_sign, 13);
}

const char * elpay_transtate_str(ELpayTransactionStateId_t state)
{
  switch(state)
  {
  case elpay_tnst_Unknown:
    return "unknown";
  case elpay_tnst_Created:
    return "created";
  case elpay_tnst_Canceled:
    return "canseled";
  case elpay_tnst_NoConfirm:
    return "no confirm";
  case elpay_tnst_Successfull:
    return "successfull";
  case elpay_tnst_Canceled2:
    return "canceled2";
  case elpay_tnst_Processing:
    return "processing";
  case elpay_tnst_Error:
    return "error";

  default:
    return "invalid";
  }
}

const char * elpay_transtate_str_rus(ELpayTransactionStateId_t state)
{
  switch(state)
  {
  case elpay_tnst_Unknown:
    return "неизвестно";
  case elpay_tnst_Created:
    return "создано";
  case elpay_tnst_Canceled:
    return "отменено";
  case elpay_tnst_NoConfirm:
    return "нет подтверждения";
  case elpay_tnst_Successfull:
    return "успешно";
  case elpay_tnst_Canceled2:
    return "отменено2";
  case elpay_tnst_Processing:
    return "обработка";
  case elpay_tnst_Error:
    return "ошибка";

  default:
    return "не корректно";
  }
}

#define GEN_BASE \
  jsonGen_t Gen; \
  if (init_jsonGen(&Gen)) return 0; \
  jsonGen_clear(&Gen);

#define GEN_CLOSE \
  size_t len = jsonGen_copy_to_buf(&Gen, buf, size); \
  free_jsonGen(&Gen); \
  return len;

#define GEN_and_CHECK(func) \
{ \
  if (func) \
  { \
    free_jsonGen(&Gen); \
    return 0; \
  } \
}

static inline void * write_mvi(ELpayMVI_t * mvi, char * buf, size_t size)
{
  snprintf(buf, size,
    /*Model*/"%s" /*os_ver*/";%s" 
    /*imei*/";%s" /*phone_num*/";%s" /*lac*/";%s" /*cid*/";%s",
    mvi->model, mvi->os_ver,
    mvi->imei, mvi->phone_num, mvi->lac, mvi->cid);
}

size_t elpay_reg_req_gen(ELpayRegReq_t* data,
  char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "rcode", 
      elpay_crc16(data->code, strlen(data->code))));

    JSON_CHAR tmp_buf[1024];
    write_mvi(&data->mvi, tmp_buf, sizeof(tmp_buf));
    GEN_and_CHECK(json_gen_write_str(&Gen, "m-v-i", tmp_buf));

    GEN_and_CHECK(json_gen_write_str(&Gen, "pos", data->pos_os_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1000));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->app_ver));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_auth_req_gen(ELpayAuthReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1));

    GEN_and_CHECK(json_gen_write_str(&Gen, "login", data->login));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "pwd", data->password));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "pos", data->pos_os_ver));

    JSON_CHAR tmp_buf[1024];
    write_mvi(&data->mvi, tmp_buf, sizeof(tmp_buf));
    GEN_and_CHECK(json_gen_write_str(&Gen, "m-v-i", tmp_buf));
 
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->app_ver));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_prodlist_req_gen(ELpayProdListReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1004));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "last-prod-ver", "0"));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_receipt_template_req_gen(ELpayReceiptTemplReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1016));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "template-name", data->name));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "last-prod-ver", "0"));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_create_operation_req_gen(ELpayCreateOpReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 10));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "pid", data->pid));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "act", data->act));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "msisdn", data->msisdn));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "nominal", MONEY_CONVERT(data->nominal)));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "comm", MONEY_CONVERT(data->commission)));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_confirm_operation_req_gen(ELpayConfirmOpReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 11));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "tran-id", data->tranId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_operation_status_req_gen(ELpayOpStatusReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 15));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "tran-id", data->tranId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_balance_req_gen(ELpayBalanceReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE
  
  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1017));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));
    
    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));
    
    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 3));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));
  
  GEN_CLOSE
}

int elpay_report_sales_req_gen(ELpayReportSalesReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE

  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1305));

    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "report-id", data->isDetailed ? 1 : 0));

    GEN_and_CHECK(json_gen_array_open(&Gen,"param"));
      for (int i = 0; i < 2; ++i)
      {
        GEN_and_CHECK(json_gen_object_open(&Gen,0));

          GEN_and_CHECK(json_gen_write_str(&Gen,"param-tag","date"));

#ifdef ELPAY_REPORT_DATE_IS_STRUCT
          char date[ELPAY_YEAR_SIZE+ELPAY_MOUNTH_SIZE+ELPAY_DAY_SIZE+1];
          snprintf(date,sizeof(date),"%s%s%s", data->date[i].year, data->date[i].mounth, data->date[i].day);
#else
          char * date = i ? data->date2 : data->date1;
#endif
          GEN_and_CHECK(json_gen_write_str(&Gen,"param",date));

        GEN_and_CHECK(json_gen_object_close(&Gen));
      }
    GEN_and_CHECK(json_gen_array_close(&Gen));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));

  GEN_CLOSE
}

int elpay_report_financial_req_gen(ELpayReportFinancialReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE

  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1305));

    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "report-id", 2));

    GEN_and_CHECK(json_gen_array_open(&Gen,"param"));

      GEN_and_CHECK(json_gen_object_open(&Gen,0));

        GEN_and_CHECK(json_gen_write_str(&Gen,"param-tag","date"));

#ifdef ELPAY_REPORT_DATE_IS_STRUCT
        char date[ELPAY_YEAR_SIZE+ELPAY_MOUNTH_SIZE+ELPAY_DAY_SIZE+1];
        snprintf(date,sizeof(date),"%s%s%s", data->date.year, data->date.mounth, data->date.day);
#else
        char * date = data->date;
#endif
        GEN_and_CHECK(json_gen_write_str(&Gen,"param",date));

      GEN_and_CHECK(json_gen_object_close(&Gen));

    GEN_and_CHECK(json_gen_array_close(&Gen));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));

  GEN_CLOSE
}

int elpay_report_turn_req_gen(ELpayReportTurnReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE

  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1306));

    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));

  GEN_CLOSE
}

int elpay_report_encashment_req_gen(ELpayReportEncashmentReq_t* data,
    char * buf, size_t size)
{
  GEN_BASE

  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_write_llint(&Gen, "msg", 1307));

    GEN_and_CHECK(json_gen_write_str(&Gen, "term", data->base.termId));

    GEN_and_CHECK(json_gen_write_str(&Gen, "session", data->base.session));

    GEN_and_CHECK(json_gen_write_str(&Gen, "version", data->base.app_ver));

    GEN_and_CHECK(json_gen_write_llint(&Gen, "proto", 4));

#ifdef ELPAY_REPORT_DATE_IS_STRUCT
    char date[ELPAY_YEAR_SIZE+ELPAY_MOUNTH_SIZE+ELPAY_DAY_SIZE+1];
    snprintf(date,sizeof(date),"%s%s%s", data->date.year, data->date.mounth, data->date.day);
#else
    char * date = data->date;
#endif
    GEN_and_CHECK(json_gen_write_str(&Gen,"date",date));
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));

  GEN_CLOSE
}

//------------------------------------------------------------------------------

static const char * elpayPath_Err[] = {"error", JSON_PATH_NULL};
static const char * elpayPath_ErrText[] = {"error-text", JSON_PATH_NULL};

#define PARSE_RETURN(res)\
{ \
  free_jsonTree(&Tree); \
  return res; \
}

#define PARSE_ERR_printf(format...)\
do{ \
  if (!strlen(errbuf)) \
    snprintf(errbuf,errbufSize, format); \
}while(0)

#define PARSE_CHECK_AND_RETURN(val)\
if (!(val)) \
{ \
  PARSE_ERR_printf(#val" is false"); \
  PARSE_RETURN(-1); \
}

#define PARSE_ERR_HANDLER(val,errHeader...)\
if (!(val)) \
{ \
  PARSE_ERR_printf(errHeader); \
  PARSE_RETURN(-1); \
}

#define PARSE_BASE \
  jsonTree_t Tree; \
  init_jsonTree(&Tree, errbuf, errbufSize); \
  PARSE_CHECK_AND_RETURN(!json_tree_parse(&Tree, buf))

#define PARSE_CHECK_ERROR \
{ \
  yajl_val errVal = json_tree_get(&Tree, elpayPath_Err, yajl_t_number); \
  if (errVal) \
  { \
    long long _errVal = YAJL_GET_INTEGER(errVal); \
    if (_errVal) \
    { \
      yajl_val errTextVal = json_tree_get(&Tree, elpayPath_ErrText, yajl_t_string); \
      const char * errText = 0; \
      if (errTextVal) \
        errText = YAJL_GET_STRING(errTextVal); \
      if (errText && strlen(errText)) \
        PARSE_ERR_printf("server error %d:\n %s", _errVal, errText); \
      else \
        PARSE_ERR_printf("server error %d", _errVal); \
      PARSE_RETURN(_errVal); \
    } \
  } \
}

static const char * elpayPath_RegMK[] = {"mk", JSON_PATH_NULL};
static const char * elpayPath_RegSK[] = {"sk", JSON_PATH_NULL};
static const char * elpayPath_RegEncCV[] = {"enccv", JSON_PATH_NULL};
static const char * elpayPath_RegTerm[] = {"term", JSON_PATH_NULL};

int elpay_reg_resp_parse(const char * buf, 
    const char * regcode, ELpayTermId_t * id, ELpaySign_t * signKey,
  char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR
  
  yajl_val mkVal = json_tree_get(&Tree, elpayPath_RegMK, yajl_t_string);
  PARSE_CHECK_AND_RETURN(mkVal)
  
  yajl_val skVal = json_tree_get(&Tree, elpayPath_RegSK, yajl_t_string);
  PARSE_CHECK_AND_RETURN(skVal)
  
  yajl_val cvVal = json_tree_get(&Tree, elpayPath_RegEncCV, yajl_t_string);
  PARSE_CHECK_AND_RETURN(cvVal)

  yajl_val termVal = json_tree_get(&Tree, elpayPath_RegTerm, yajl_t_string);
  PARSE_CHECK_AND_RETURN(termVal);
  snprintf(*id,ELPAY_JSON_TERM_ID_BUFSIZE, "%s", YAJL_GET_STRING(termVal));

  int keyEncBufSize = elpay_base64_decsize(ELPAY_JSON_KEYenc_SIZE)+1;
  char mk0[keyEncBufSize], sk0[keyEncBufSize], cv0[keyEncBufSize];
  
  elpay_base64_decode(YAJL_GET_STRING(mkVal), ELPAY_JSON_KEYenc_SIZE, mk0, keyEncBufSize);
  elpay_base64_decode(YAJL_GET_STRING(skVal), ELPAY_JSON_KEYenc_SIZE, sk0, keyEncBufSize);
  elpay_base64_decode(YAJL_GET_STRING(cvVal), ELPAY_JSON_KEYenc_SIZE, cv0, keyEncBufSize);
  
  ELpayKey_t mk, cv;
  
  elpay_des_decrypt((char*)regcode, mk0, ELPAY_JSON_KEYSIZE, mk, ELPAY_JSON_KEY_BUFSIZE);
  elpay_des_decrypt(mk, sk0, ELPAY_JSON_KEYSIZE, *signKey, ELPAY_JSON_KEY_BUFSIZE);
  elpay_des_decrypt(*signKey, cv0, ELPAY_JSON_KEYSIZE, cv, ELPAY_JSON_KEY_BUFSIZE);
  
  int64_t * Check = (int64_t *)cv;
  PARSE_ERR_HANDLER(!(*Check), "check failure");
  
  PARSE_RETURN(0);
}

static const char * elpayPath_AuthSession[] = {"session", JSON_PATH_NULL};
static const char * elpayPath_AuthVersion[] = {"version", JSON_PATH_NULL};

int elpay_auth_resp_parse(const char * buf, 
    ELpaySessionId_t * session,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR
  
  yajl_val sessionVal = json_tree_get(&Tree, elpayPath_AuthSession, yajl_t_string);
  PARSE_CHECK_AND_RETURN(sessionVal);
  snprintf(*session,ELPAY_JSON_SESSION_ID_BUFSIZE, "%s", YAJL_GET_STRING(sessionVal));
  
  yajl_val versionVal = json_tree_get(&Tree, elpayPath_AuthVersion, yajl_t_string);
  PARSE_CHECK_AND_RETURN(sessionVal);
  
  PARSE_RETURN(0);
}

static const char * elpayPath_ProdStId[] = {"st-id", JSON_PATH_NULL};
static const char * elpayPath_ProdStSortOrder[] = {"st-sort-order", JSON_PATH_NULL};
static const char * elpayPath_ProdServName[ELPAY_LANGS_COUNT][2] =
{
  {"service-name-ru", JSON_PATH_NULL},
  {"service-name-ua", JSON_PATH_NULL},
};

static int read_serviceId_from_prodval(int * id, yajl_val val)
{
  *id = -1;
  yajl_val Val = yajl_tree_get(val, elpayPath_ProdStId, yajl_t_number);
  if (!Val) return -1;
  *id = YAJL_GET_INTEGER(Val);
  return 0;
}

static int read_service_from_prodval(int sid, ELpayService_t * This, yajl_val val)
{
  yajl_val Val;

  This->id = sid;

  Val = yajl_tree_get(val, elpayPath_ProdStSortOrder, yajl_t_number);
  if (!Val) return -1;
  This->sort_order = YAJL_GET_INTEGER(Val);

  for (size_t i = 0; i < ELPAY_LANGS_COUNT; ++i)
  {
    Val = yajl_tree_get(val, elpayPath_ProdServName[i], yajl_t_string);
    if (!Val) return -2;
    strncpy(This->name[i], YAJL_GET_STRING(Val), ELPAY_NAME_BUFSIZE);
  }

  return 0;
}

static const char * elpayPath_ProdFieldFid[] = {"field-id", JSON_PATH_NULL};
static const char * elpayPath_ProdFieldName[ELPAY_LANGS_COUNT][2] =
{
  {"name-ru", JSON_PATH_NULL},
  {"name-ua", JSON_PATH_NULL},
};
static const char * elpayPath_ProdFieldMask[] = {"mask", JSON_PATH_NULL};
static const char * elpayPath_ProdFieldCharMin[] = {"char-min", JSON_PATH_NULL};
static const char * elpayPath_ProdFieldCharMax[] = {"char-max", JSON_PATH_NULL};
static int read_product_field(ELpayProductField_t * This, yajl_val val)
{
  yajl_val Val;

  Val = yajl_tree_get(val, elpayPath_ProdFieldFid, yajl_t_number);
  if (!Val) return -1;
  This->id = YAJL_GET_INTEGER(Val);

  for (size_t i = 0; i < ELPAY_LANGS_COUNT; ++i)
  {
    Val = yajl_tree_get(val, elpayPath_ProdFieldName[i], yajl_t_string);
    if (!Val) return -2;
    strncpy(This->name[i], YAJL_GET_STRING(Val), ELPAY_NAME_BUFSIZE);
  }

  Val = yajl_tree_get(val, elpayPath_ProdFieldMask, yajl_t_string);
  if (!Val) return -3;
  strncpy(This->mask, YAJL_GET_STRING(Val), ELPAY_MASK_BUFSIZE);

  Val = yajl_tree_get(val, elpayPath_ProdFieldCharMin, yajl_t_number);
  if (!Val) return -41;
  This->charMin = YAJL_GET_INTEGER(Val);

  Val = yajl_tree_get(val, elpayPath_ProdFieldCharMax, yajl_t_number);
  if (!Val) return -42;
  This->charMax = YAJL_GET_INTEGER(Val);

  return 0;
}

static const char * elpayPath_ProdCommNomMin[] = {"cm-l", JSON_PATH_NULL};
static const char * elpayPath_ProdCommNomMax[] = {"cm-h", JSON_PATH_NULL};
static const char * elpayPath_ProdCommVal[] = {"cm-am", JSON_PATH_NULL};
static const char * elpayPath_ProdCommPer[] = {"cm-p", JSON_PATH_NULL};
static int read_product_comm(ELpayProductCommission_t * This, yajl_val val)
{
  yajl_val Val;

  Val = yajl_tree_get(val, elpayPath_ProdCommNomMin, yajl_t_number);
  if (!Val) return -11;
  This->nominalMin = YAJL_GET_INTEGER(Val);

  Val = yajl_tree_get(val, elpayPath_ProdCommNomMax, yajl_t_number);
  if (!Val) return -12;
  This->nominalMax = YAJL_GET_INTEGER(Val);

  Val = yajl_tree_get(val, elpayPath_ProdCommVal, yajl_t_number);
  if (!Val) return -21;
  This->value = YAJL_GET_INTEGER(Val);

  Val = yajl_tree_get(val, elpayPath_ProdCommPer, yajl_t_number);
  if (!Val) return -22;
  This->percent = YAJL_GET_INTEGER(Val);

  return 0;
}

static const char * elpayPath_ProdPid[] = {"pid", JSON_PATH_NULL};
static const char * elpayPath_ProdSortOrder[] = {"sort-order", JSON_PATH_NULL};
static const char * elpayPath_ProdName[ELPAY_LANGS_COUNT][2] =
{
  {"name-ru", JSON_PATH_NULL},
  {"name-ua", JSON_PATH_NULL},
};
static const char * elpayPath_ProdNameShort[ELPAY_LANGS_COUNT][2] =
{
  {"name-short-ru", JSON_PATH_NULL},
  {"name-short-ua", JSON_PATH_NULL},
};
static const char * elpayPath_ProdFields[] = {"fields", JSON_PATH_NULL};
static const char * elpayPath_ProdComm[] = {"comm", JSON_PATH_NULL};
static const char * elpayPath_ProdTemplName[] = {"template-name", JSON_PATH_NULL};
static int read_product(int sid, ELpayProduct_t * This, yajl_val val)
{
  This->sid = sid;

  yajl_val Val;

  Val = yajl_tree_get(val, elpayPath_ProdPid, yajl_t_number);
  if (!Val) return -10;
  This->id = YAJL_GET_INTEGER(Val);

  Val = yajl_tree_get(val, elpayPath_ProdSortOrder, yajl_t_number);
  if (!Val) return -20;
  This->sort_order = YAJL_GET_INTEGER(Val);

  for (size_t i = 0; i < ELPAY_LANGS_COUNT; ++i)
  {
    Val = yajl_tree_get(val, elpayPath_ProdName[i], yajl_t_string);
    if (!Val) return -31;
    strncpy(This->name[i], YAJL_GET_STRING(Val), ELPAY_NAME_BUFSIZE);

    Val = yajl_tree_get(val, elpayPath_ProdNameShort[i], yajl_t_string);
    if (!Val) return -32;
    strncpy(This->name_short[i], YAJL_GET_STRING(Val), ELPAY_NAMESHORT_BUFSIZE);
  }

  Val = yajl_tree_get(val, elpayPath_ProdFields, yajl_t_array);
  if (!Val) return -40;

  yajl_val * fieldsVal = YAJL_GET_ARRAY(Val)->values;
  size_t fieldsLen = YAJL_GET_ARRAY(Val)->len;

  This->fieldsCount = 0;
  for (size_t i = 0; i < fieldsLen; ++i)
  {
    if (This->fieldsCount >= ELPAY_MAX_FIELDS_COUNT) return -50;
    int err = read_product_field(&This->fields[This->fieldsCount], fieldsVal[i]);
    if (err)
    {
      LOG_FXN_MSG(WRN,FAIL, "read field: %d", err);
      return -51;
    }
    ++This->fieldsCount;
  }

  Val = yajl_tree_get(val, elpayPath_ProdComm, yajl_t_array);
  if (!Val) return -60;

  yajl_val * commVal = YAJL_GET_ARRAY(Val)->values;
  size_t commLen = YAJL_GET_ARRAY(Val)->len;
  assert(commLen == 1);

  int err = read_product_comm(&This->commission, commVal[0]);
  if (err) return -61;

  Val = yajl_tree_get(val, elpayPath_ProdTemplName, yajl_t_string);
  if (!Val) return -70;
  strncpy(This->receipt_templ_name, YAJL_GET_STRING(Val), ELPAY_NAME_BUFSIZE);

  return 0;
}

#define PARSE_PL_CHECK_AND_RETURN(err, header)\
  PARSE_ERR_HANDLER(!(err),"%s failure: %d", header, err)

static const char * elpayPath_Prod[] = {"prod", JSON_PATH_NULL};

static int pl_service_idx_by_id(ELpayProdList_t * list, int id)
{
  for (size_t i = 0; i < list->ServicesCount; ++i)
    if (list->Services[i].id == id)
      return i;
  return -1;
}
int elpay_prodlist_resp_parse(const char * buf,
  ELpayProdList_t * list,
  char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR

  yajl_val prodVal = json_tree_get(&Tree, elpayPath_Prod, yajl_t_array);
  PARSE_CHECK_AND_RETURN(prodVal)

  yajl_val * prodVals = YAJL_GET_ARRAY(prodVal)->values;
  size_t prodLen = YAJL_GET_ARRAY(prodVal)->len;

  list->ServicesCount = 0;
  list->ProductsCount = 0;
  for (size_t i = 0; i < prodLen; ++i)
  {
    assert(list->ProductsCount < ELPAY_MAX_PRODCOUNT);
    if (list->ProductsCount >= ELPAY_MAX_PRODCOUNT) break;

    int err;

    int sid;
    err = read_serviceId_from_prodval(&sid, prodVals[i]);
    PARSE_PL_CHECK_AND_RETURN(err, "read service sid")

    err = read_product(sid, &list->Products[list->ProductsCount], prodVals[i]);
    PARSE_PL_CHECK_AND_RETURN(err, "read product")
    ++list->ProductsCount;

    if (pl_service_idx_by_id(list, sid) >= 0) continue;

    assert(list->ServicesCount < ELPAY_MAX_SERVCOUNT);
    err = read_service_from_prodval(sid, &list->Services[list->ServicesCount], prodVals[i]);
    PARSE_PL_CHECK_AND_RETURN(err, "read service")
    ++list->ServicesCount;
  }

  PARSE_RETURN(0);
}

static const char * elpayPath_TranId[] = {"tran-id", JSON_PATH_NULL};

int elpay_create_operation_resp_parse(const char * buf, 
    ELpayTransactionId_t * tranId,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR
  
  yajl_val tranIdVal = json_tree_get(&Tree, elpayPath_TranId, yajl_t_number);
  PARSE_CHECK_AND_RETURN(tranIdVal);
  *tranId = YAJL_GET_INTEGER(tranIdVal);
  
  PARSE_RETURN(0);
}

static const char * elpayPath_TranStateId[] = {"state-id", JSON_PATH_NULL};
static const char * elpayPath_TranStateErr[] = {"tn-error", JSON_PATH_NULL};
static const char * elpayPath_TranStateInfo[] = {"add-info", JSON_PATH_NULL};

int elpay_operation_status_resp_parse(const char * buf,
    ELpayTransactionState_t * state,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR

  yajl_val Val;

  Val = json_tree_get(&Tree, elpayPath_TranStateId, yajl_t_number);
  PARSE_CHECK_AND_RETURN(Val);
  state->id = YAJL_GET_INTEGER(Val);

  Val = json_tree_get(&Tree, elpayPath_TranStateErr, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(state->errMsg, YAJL_GET_STRING(Val), ELPAY_JSON_TRANSTATE_ERRMSG_BUFSIZE);

//  Val = json_tree_get(&Tree, elpayPath_TranStateInfo, yajl_t_array);
//  PARSE_CHECK_AND_RETURN(Val);

  PARSE_RETURN(0);
}

static const char * elpayPath_BalanceDate[] = {"date", JSON_PATH_NULL};
static const char * elpayPath_BalanceNet[] = {"net-bal", JSON_PATH_NULL};
static const char * elpayPath_BalanceCli[] = {"cli-bal", JSON_PATH_NULL};
static const char * elpayPath_BalanceCredit[] = {"credit", JSON_PATH_NULL};
static const char * elpayPath_BalanceLimitDay24[] = {"limit-24h", JSON_PATH_NULL};
static const char * elpayPath_BalanceLimitDay[] = {"limit-day", JSON_PATH_NULL};
static const char * elpayPath_BalanceLimitOp[] = {"op-limit", JSON_PATH_NULL};

int elpay_balance_resp_parse(const char * buf,
    ELpayBalance_t * balance,
    char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR

  yajl_val Val;

  Val = json_tree_get(&Tree, elpayPath_BalanceDate, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->date, YAJL_GET_STRING(Val), ELPAY_JSON_DATE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceNet, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->net, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceCli, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->client, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceCredit, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->credit, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceLimitDay24, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->limit.day24, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceLimitDay, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->limit.day, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  Val = json_tree_get(&Tree, elpayPath_BalanceLimitOp, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(balance->limit.operation, YAJL_GET_STRING(Val), ELPAY_JSON_BALANCE_BUFSIZE);

  PARSE_RETURN(0);
}

static const char * elpayPath_RecpTemplData[] = {"data", JSON_PATH_NULL};

int elpay_receipt_template_resp_parse(const char * buf,
  char * data,
  char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR

  yajl_val Val;

  Val = json_tree_get(&Tree, elpayPath_RecpTemplData, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(data, YAJL_GET_STRING(Val), ELPAY_JSON_DATE_BUFSIZE);

  PARSE_RETURN(0);
}

static const char * elpayPath_ReportTextData[] = {"text", JSON_PATH_NULL};

int elpay_report_html_resp_parse(const char * buf, char* textbuf, size_t textbufSize,
  char * errbuf, size_t errbufSize)
{
  PARSE_BASE
  PARSE_CHECK_ERROR

  yajl_val Val;

  Val = json_tree_get(&Tree, elpayPath_ReportTextData, yajl_t_string);
  PARSE_CHECK_AND_RETURN(Val);
  strncpy(textbuf, YAJL_GET_STRING(Val), textbufSize);

  PARSE_RETURN(0);
}
