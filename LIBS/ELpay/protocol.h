/* 
 * File:   protocol.h
 */

#ifndef ELPAY_PROTOCOL_H
#define ELPAY_PROTOCOL_H

#include "types.h"

#include <stddef.h>
#include "json.h"

#ifdef __cplusplus
extern "C"
{
#endif

int elpay_sign(const char * key, char * packet, char * sign, char * b64_sign);

size_t elpay_reg_req_gen
(
  ELpayRegReq_t * data,
  char * buf, size_t size
);
int elpay_auth_req_gen
(
  ELpayAuthReq_t * data,
  char * buf, size_t size
);
int elpay_balance_req_gen
(
  ELpayBalanceReq_t * data,
  char * buf, size_t size
);
int elpay_prodlist_req_gen
(
  ELpayProdListReq_t * data,
  char * buf, size_t size
);
int elpay_receipt_template_req_gen
(
  ELpayReceiptTemplReq_t * data,
  char * buf, size_t size
);
int elpay_create_operation_req_gen
(
  ELpayCreateOpReq_t * data,
  char * buf, size_t size);
int elpay_confirm_operation_req_gen
(
  ELpayConfirmOpReq_t * data,
  char * buf, size_t size
);
int elpay_operation_status_req_gen
(
  ELpayOpStatusReq_t * data,
  char * buf, size_t size
);

int elpay_report_sales_req_gen
(
  ELpayReportSalesReq_t* data,
  char * buf, size_t size
);

int elpay_report_financial_req_gen
(
  ELpayReportFinancialReq_t* data,
  char * buf, size_t size
);

int elpay_report_turn_req_gen
(
  ELpayReportTurnReq_t* data,
  char * buf, size_t size
);

int elpay_report_encashment_req_gen
(
  ELpayReportEncashmentReq_t* data,
  char * buf, size_t size
);

//------------------------------------------------------------------

int elpay_reg_resp_parse
(
  const char * buf, 
  const char * regcode,
  ELpayTermId_t * id, ELpaySign_t * signKey,
  char * errbuf, size_t errbufSize
);
int elpay_auth_resp_parse
(
  const char * buf, 
  ELpaySessionId_t * session,
  char * errbuf, size_t errbufSize
);

int elpay_prodlist_resp_parse
(
  const char * buf,
  ELpayProdList_t * list,
  char * errbuf, size_t errbufSize
);

int elpay_create_operation_resp_parse
(
  const char * buf, 
  ELpayTransactionId_t * tranId,
  char * errbuf, size_t errbufSize
);

#define elpay_confirm_operation_resp_parse elpay_operation_status_resp_parse

int elpay_operation_status_resp_parse
(
  const char * buf,
  ELpayTransactionState_t * state,
  char * errbuf,
  size_t errbufSize
);

int elpay_balance_resp_parse
(
  const char * buf,
  ELpayBalance_t * balance,
  char * errbuf,
  size_t errbufSize
);

int elpay_receipt_template_resp_parse
(
  const char * buf,
  char * data,
  char * errbuf,
  size_t errbufSize
);

int elpay_report_html_resp_parse
(
  const char * buf,
  char * textbuf, size_t textbufSize,
  char * errbuf,
  size_t errbufSize
);

#define elpay_report_sales_resp_parse elpay_report_html_resp_parse
#define elpay_report_financial_resp_parse elpay_report_html_resp_parse
#define elpay_report_turn_resp_parse elpay_report_html_resp_parse
#define elpay_report_encashment_resp_parse elpay_report_html_resp_parse

#ifdef __cplusplus
}
#endif

#endif /* ELPAY_PROTOCOL_H */

