/*
 * File:   json.c
 */


#include "json.h"

#define LOG_HEADER "JSON"
#define LOG_LEVEL WRN
#include "SYSTEM/logs.h"

#include <stdio.h>
#include <string.h>

const char * jsonGen_resStr(int res)
{
  switch (res)
  {
  case yajl_gen_status_ok:
    return "success";
  case yajl_gen_keys_must_be_strings:
    return "keys must be string";
  case yajl_max_depth_exceeded:
    return "max depth exeeded";
  case yajl_gen_in_error_state:
    return "gen in error state";
  case yajl_gen_generation_complete:
    return "generation complete";
  case yajl_gen_invalid_number:
    return "invalid number";
  case yajl_gen_no_buf:
    return "no buf";
  case yajl_gen_invalid_string:
    return "invalid string";

  default:
    return "unknown error";
  }
}

/* non-zero when we're reformatting a stream */
static int s_streamReformat = 1;

#define GENnva(__stat,func,gen) \
{ \
  __stat = func(gen); \
  if (__stat == yajl_gen_generation_complete && s_streamReformat) { \
    yajl_gen_reset(gen, "\n"); \
    __stat = func(gen); \
  } \
}

#define GEN(__stat,func,gen,...) \
{ \
  __stat = func(gen,__VA_ARGS__); \
  if (__stat == yajl_gen_generation_complete && s_streamReformat) { \
    yajl_gen_reset(gen, "\n"); \
    __stat = func(gen,__VA_ARGS__); \
  } \
}

#define GEN_CHECK_AND_RETURN(header,stat) \
if (stat) \
{ \
  LOG_FXN_MSG(WRN,FAIL, "%s",jsonGen_resStr(stat)); \
  return stat; \
}

#define GENnva_AND_RETURN(func,gen) \
{ \
  yajl_gen_status __stat; \
  GENnva(__stat,func,gen); \
  GEN_CHECK_AND_RETURN(#func,__stat); \
}

#define GEN_AND_RETURN(func,gen,...) \
{ \
  yajl_gen_status __stat; \
  GEN(__stat,func,gen,__VA_ARGS__); \
  GEN_CHECK_AND_RETURN(#func,__stat); \
}

#define TYPE_GENnva_AND_RETURN(type,gen) GENnva_AND_RETURN(yajl_gen_ ## type,gen)
#define TYPE_GEN_AND_RETURN(type,gen,...) GEN_AND_RETURN(yajl_gen_##type,gen,__VA_ARGS__)

static inline yajl_gen_status yajl_gen_STRING(yajl_gen hand, const JSON_CHAR * str)
{
  return yajl_gen_string(hand, str, strlen(str));
}

//-------------------------------------------------------------

extern yajl_alloc_funcs * jsonAfxPtr;

int init_jsonGen(jsonGen_t* This)
{
  LOG_FXN(DBG,START);

  This->Generator = yajl_gen_alloc(jsonAfxPtr);
  if (This->Generator == NULL) return -1;

//  yajl_gen_config(This->Generator, yajl_gen_beautify, 1);
  yajl_gen_config(This->Generator, yajl_gen_validate_utf8, 1);

  LOG_FXN(DBG,OK);
  return 0;
}

void free_jsonGen(jsonGen_t* This)
{
  LOG_FXN(DBG,START);
  yajl_gen_free(This->Generator);
  LOG_FXN(DBG,OK);
}

void jsonGen_clear(jsonGen_t* This)
{
  LOG_FXN(DBG,START);
  yajl_gen_clear(This->Generator);
  LOG_FXN(DBG,OK);
}

int json_gen_object_open(jsonGen_t* This, const JSON_CHAR * name)
{
  LOG_FXN(DBG,START);
  if (name)
    TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  GENnva_AND_RETURN(yajl_gen_map_open,This->Generator);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_object_close(jsonGen_t* This)
{
  LOG_FXN(DBG,START);
  GENnva_AND_RETURN(yajl_gen_map_close,This->Generator);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_array_open(jsonGen_t* This, const JSON_CHAR * name)
{
  LOG_FXN(DBG,START);
  if (name)
    TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  GENnva_AND_RETURN(yajl_gen_array_open,This->Generator);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_array_close(jsonGen_t* This)
{
  LOG_FXN(DBG,START);
  GENnva_AND_RETURN(yajl_gen_array_close,This->Generator);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_write_null(jsonGen_t* This, const JSON_CHAR * name)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  TYPE_GENnva_AND_RETURN(null, This->Generator);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_write_bool(jsonGen_t* This, const JSON_CHAR * name, int val)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  TYPE_GEN_AND_RETURN(bool, This->Generator, val);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_write_str(jsonGen_t* This, const JSON_CHAR * name, const JSON_CHAR * str)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, str);
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_write_llint(jsonGen_t* This, const JSON_CHAR * name, long long val)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  TYPE_GEN_AND_RETURN(integer, This->Generator, val);
  LOG_FXN(DBG,OK);
  return 0;
}

#ifdef JSON_GEN_DOUBLE_NOT_DIRECTLY
#include <stdint.h>
#include <math.h>
extern char * dtostrf_p (double val, unsigned char prec, char * sout, size_t soutSize);
#endif

int json_gen_write_double(jsonGen_t* This, const JSON_CHAR * name, double val)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
#ifdef JSON_GEN_DOUBLE_NOT_DIRECTLY
  JSON_CHAR buf[255u];
  dtostrf_p(val, 2, buf, sizeof(buf));
  TYPE_GEN_AND_RETURN(number, This->Generator, buf, strlen(buf));
#else
  TYPE_GEN_AND_RETURN(double, This->Generator, val);
#endif
  LOG_FXN(DBG,OK);
  return 0;
}

int json_gen_write_number(jsonGen_t* This, const JSON_CHAR * name, JSON_CHAR * val)
{
  LOG_FXN(DBG,START);
  TYPE_GEN_AND_RETURN(STRING, This->Generator, name);
  TYPE_GEN_AND_RETURN(number, This->Generator, val, strlen(val));
  LOG_FXN(DBG,OK);
  return 0;
}

int jsonGen_internal_buf(jsonGen_t* This, const JSON_CHAR ** buf, size_t * len)
{
  LOG_FXN(DBG,START);
  GEN_AND_RETURN(yajl_gen_get_buf, This->Generator, buf, len);
  LOG_FXN(DBG,OK);
  return 0;
}

size_t jsonGen_copy_to_buf(jsonGen_t* This, char * buf, size_t size)
{
  LOG_FXN(DBG,START);
  const JSON_CHAR * int_buf; size_t len;
  GEN_AND_RETURN(yajl_gen_get_buf, This->Generator, &int_buf, &len);
  if (len > size)
  {
    LOG_FXN(DBG,FAIL);
    return 0;
  }

  strncpy(buf, int_buf, len);
  buf[len] = '\0';

  LOG_FXN(DBG,OK);
  return len;
}

//-------------------------------------------------------------

void init_jsonTree(jsonTree_t* This, char * errBuf, size_t errBufSize)
{
  LOG_FXN(DBG,START);

  This->node = 0;

  This->errBuf = errBuf;
  This->errBufSize = errBufSize;
  This->errBuf[0] = '\0';

  LOG_FXN(DBG,OK);
}

void free_jsonTree(jsonTree_t* This)
{
  LOG_FXN(DBG,START);

  if (This->node)
    yajl_tree_free(This->node);

  LOG_FXN(DBG,OK);
}

int json_tree_parse(jsonTree_t* This, const char * buf)
{
  LOG_FXN(DBG,START);

  if (!buf)
  {
    LOG_FXN(DBG,ABORT);
    return -1;
  }

  This->errBuf[0] = 0;

  This->node = yajl_tree_parse(buf, This->errBuf, This->errBufSize);
  if (This->node == NULL)
  {
    if (!strlen(This->errBuf)) snprintf(This->errBuf,This->errBufSize, "unknown error");
    LOG_FXN(DBG,FAIL);
    return -1;
  }

  LOG_FXN(DBG,OK);
  return 0;
}

yajl_val json_tree_get(jsonTree_t* This, const char ** path, yajl_type type)
{
  LOG_FXN(DBG,START);

  char path_buf[255u];
  {
    int i = 0, len = 0;
    size_t n = sizeof(path_buf);
    while(path[i] && (n > 0))
    {
      snprintf(&path_buf[len],n-len, path[i]);
      len = strlen(path_buf);
      ++i;
    }
    path_buf[len] = '\0';
  }
  LOG_FXN_MSG(DBG,NONE, "for \"%s\"", path_buf);

  yajl_val val = yajl_tree_get(This->node, path, type);
  if (!val)
  {
    snprintf(This->errBuf,This->errBufSize, "\"%s\" read failure", path_buf);
    LOG_FXN(DBG,FAIL);
  }
  else
    LOG_FXN(DBG,OK);
  return val;
}
