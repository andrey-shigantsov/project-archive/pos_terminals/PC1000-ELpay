#define LOG_HEADER "ELpayOpList"
#define LOG_LEVEL INF
#include "SYSTEM/logs.h"

#include "OperationList.h"

#include "SYSTEM/base.h"
#include "SYSTEM/file.h"

#include "ELpay/json.h"

#include <string.h>

#define GEN_BASE \
  jsonGen_t Gen; \
  if (init_jsonGen(&Gen)) return 0; \
  jsonGen_clear(&Gen);

#define GEN_CLOSE(retVal) \
  free_jsonGen(&Gen); \
  return retVal;

#define GEN_and_CHECK(func) \
{ \
  if (func) \
  { \
    LOG_FXN_MSG(WRN,FAIL,#func); \
    GEN_CLOSE(0); \
  } \
}

#define GEN_and_CKECK_item(item) \
{ \
  GEN_and_CHECK(json_gen_object_open(&Gen, 0)); \
  { \
    GEN_and_CHECK(json_gen_write_str(&Gen, "account", (item)->account)); \
    GEN_and_CHECK(json_gen_write_double(&Gen, "nominal", (item)->nominal)); \
    GEN_and_CHECK(json_gen_write_llint(&Gen, "status", (item)->status)); \
  } \
  GEN_and_CHECK(json_gen_object_close(&Gen)); \
}

static bool gen_json_list_and_write(ELpay_OperationList_t * This)
{
  GEN_BASE

  GEN_and_CHECK(json_gen_object_open(&Gen, 0));
  {
    GEN_and_CHECK(json_gen_array_open(&Gen, "OperationList"));
    for(size_t i = 0; i < ELpay_OperationList_ItemsCount(This); ++i)
    {
      ELpay_OperationListItem_t * item = ELpay_OperationList_Item(This,i);
      GEN_and_CKECK_item(item);
    }
    GEN_and_CHECK(json_gen_array_close(&Gen))
  }
  GEN_and_CHECK(json_gen_object_close(&Gen));

  JSON_CHAR * buf; size_t len;
  GEN_and_CHECK(jsonGen_internal_buf(&Gen, (const JSON_CHAR **)&buf, &len));

  int wn = Lib_FileWrite(This->fid, buf, len);
  if (wn <= 0)
    LOG_FXN_MSG(WRN,FAIL,"write error %d", wn);
  else
    LOG_FXN_MSG(INF,NONE,"write %d/%d bytes", wn,len);
  GEN_CLOSE(wn > 0)
}

#define PARSE_RETURN(res)\
{ \
  free(buf); \
  free_jsonTree(&Tree); \
  return res; \
}

#define PARSE_CHECK_AND_RETURN(val)\
if (!(val)) \
{ \
  LOG_FXN_MSG(WRN,FAIL,#val" is false"); \
  PARSE_RETURN(0); \
}

static const char * jsonPath_OpLIst[] = {"OperationList", JSON_PATH_NULL};
static const char * jsonPath_ItemAccount[] = {"account", JSON_PATH_NULL};
static const char * jsonPath_ItemNominal[] = {"nominal", JSON_PATH_NULL};
static const char * jsonPath_ItemStatus[] = {"status", JSON_PATH_NULL};

static bool read_and_parse_json_list(ELpay_OperationList_t * This)
{
#ifdef READ_REAL_SIZE
  long size = file_size(This->FileName);
  if (size < 0)
    return 0;
  if (!size)
  {
    LOG_FXN_MSG(WRN,FAIL,"file size is null");
    return 0;
  }
#else
  long size = ELPAY_OPLIST_MAX_ITEMS_COUNT*255;
#endif
  BYTE * buf = malloc(size+1);
  assert(buf);
  int rn = Lib_FileRead(This->fid, buf, size);
  if (rn < 0)
  {
    LOG_FXN_MSG(WRN,FAIL,"read failure: code = %d", rn);
    free(buf);
    return 0;
  }
  if (!rn)
  {
    LOG_FXN_MSG(INF,FAIL,"read size is null");
    free(buf);
    return 0;
  }
  buf[rn] = 0;
  LOG_FXN_MSG(DBG,NONE,"read buf (size = %d):\n%s", rn, buf);

  jsonTree_t Tree;
  init_jsonTree(&Tree, This->errBuf, sizeof(This->errBuf));
  PARSE_CHECK_AND_RETURN(!json_tree_parse(&Tree, buf));

  yajl_val opListVal = json_tree_get(&Tree, jsonPath_OpLIst, yajl_t_array);
  PARSE_CHECK_AND_RETURN(opListVal)

  yajl_val * items = YAJL_GET_ARRAY(opListVal)->values;
  size_t count = YAJL_GET_ARRAY(opListVal)->len;
  for (size_t i = 0; i < count; ++i)
  {
    yajl_val ItemAccountVal = yajl_tree_get(items[i], jsonPath_ItemAccount, yajl_t_string);
    PARSE_CHECK_AND_RETURN(ItemAccountVal)
    yajl_val ItemNominalVal = yajl_tree_get(items[i], jsonPath_ItemNominal, yajl_t_number);
    PARSE_CHECK_AND_RETURN(ItemNominalVal)
    yajl_val ItemStatusVal = yajl_tree_get(items[i], jsonPath_ItemStatus, yajl_t_number);
    PARSE_CHECK_AND_RETURN(ItemStatusVal)
    ELpay_OperationList_append(This,YAJL_GET_STRING(ItemAccountVal), YAJL_GET_DOUBLE(ItemNominalVal), YAJL_GET_INTEGER(ItemStatusVal));
  }
  PARSE_RETURN(1)
}

static void write_list(ELpay_OperationList_t * This)
{
  LOG_FXN(INF,START);

  if (file_exist(This->FileName))
    if (!file_remove(This->FileName)) return;

  This->fid = file_open(This->FileName, O_CREATE);
  if (This->fid < 0)
  {
    LOG_FXN(WRN,FAIL);
    return;
  }

  bool res = gen_json_list_and_write(This);

  file_close(This->fid);

  if (!res)
    LOG_FXN(INF,FAIL);
  else
    LOG_FXN(INF,OK);
}

static void read_list(ELpay_OperationList_t * This)
{
  LOG_FXN(INF,START);

  This->fid = file_open(This->FileName, O_RDWR);
  if (This->fid < 0)
  {
    LOG_FXN(WRN,FAIL);
    return;
  }

  bool res = read_and_parse_json_list(This);

  file_close(This->fid);

  if (!res)
    LOG_FXN(WRN,FAIL);
  else
    LOG_FXN(INF,OK);
}

INLINE void reset_Items(ELpay_OperationList_t* This)
{
  This->ItemsCount = 0;

  This->ItemsWriteIdx = 0;
}

INLINE void Items_IdxInc(ELpay_OperationList_t* This)
{
  if (This->ItemsCount < ELPAY_OPLIST_MAX_ITEMS_COUNT)
    ++This->ItemsCount;
  if (This->ItemsCount > ELPAY_OPLIST_MAX_ITEMS_COUNT)
    This->ItemsCount = ELPAY_OPLIST_MAX_ITEMS_COUNT;

  ++This->ItemsWriteIdx;
  if (This->ItemsWriteIdx >= ELPAY_OPLIST_MAX_ITEMS_COUNT)
    This->ItemsWriteIdx = 0;
}

INLINE int Items_IdxConversion(ELpay_OperationList_t* This, int i)
{
  if (i >= This->ItemsCount)
    return -1;

  int idx = This->ItemsWriteIdx - This->ItemsCount + i;
  if (idx < 0)
    idx += ELPAY_OPLIST_MAX_ITEMS_COUNT;
  return idx;
}

INLINE int Items_IdxRevertion(ELpay_OperationList_t* This, int idx)
{
  if (idx >= This->ItemsCount)
    return -1;

  int i = idx - This->ItemsWriteIdx + This->ItemsCount;
  if (i >= This->ItemsCount)
    i -= ELPAY_OPLIST_MAX_ITEMS_COUNT;
  return i;
}

void init_ELpay_OperationList(ELpay_OperationList_t* This, const char* listFileName)
{
  strncpy(This->FileName, listFileName, sizeof(This->FileName));

  reset_Items(This);

  read_list(This);
}

int ELpay_OperationList_append(ELpay_OperationList_t* This, const char* account, double nominal, bool status)
{
  int idx = This->ItemsWriteIdx;
  ELpay_OperationListItem_t * item = &This->Items[idx];

  strncpy(item->account, account, sizeof(item->account));
  item->nominal = nominal;
  item->status = status;

  Items_IdxInc(This);

  return Items_IdxRevertion(This,idx);
}

void ELpay_OperationList_clear(ELpay_OperationList_t* This)
{
  reset_Items(This);
}

ELpay_OperationListItem_t * ELpay_OperationList_Item(ELpay_OperationList_t* This, int i)
{
  int idx = Items_IdxConversion(This,i);
  if (idx < 0) return 0;
  ELpay_OperationListItem_t * item = &This->Items[idx];
  return item;
}

void ELpay_OperationList_update_file(ELpay_OperationList_t* This)
{
  write_list(This);
}
