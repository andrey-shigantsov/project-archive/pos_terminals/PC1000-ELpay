/* 
 * File:   transport.h
 */

#include "../Terminal.h"

#include "SYSTEM/NETWORK/TcpSocket.h"

#ifndef ELPAY_TRANSPORT_H
#define ELPAY_TRANSPORT_H

int elpay_reg_req_send(ELpayTerminal_t * This, ELpayRegReq_t * data);
int elpay_reg_resp_get(ELpayTerminal_t * This, const char * regcode);

int elpay_auth_req_send(ELpayTerminal_t * This, ELpayAuthReq_t* data);
int elpay_auth_resp_get(ELpayTerminal_t * This);

int elpay_balance_req_send(ELpayTerminal_t * This, ELpayBalanceReq_t* data);
int elpay_balance_resp_get(ELpayTerminal_t * This, ELpayBalance_t * balance);

int elpay_prodlist_req_send(ELpayTerminal_t * This, ELpayProdListReq_t* data);
int elpay_prodlist_resp_get(ELpayTerminal_t * This, ELpayProdList_t * list);

int elpay_receipt_template_req_send(ELpayTerminal_t * This, ELpayReceiptTemplReq_t* data);
int elpay_receipt_template_resp_get(ELpayTerminal_t * This);

int elpay_create_operation_req_send(ELpayTerminal_t * This, ELpayCreateOpReq_t* data);
int elpay_create_operation_resp_get(ELpayTerminal_t * This);

int elpay_confirm_operation_req_send(ELpayTerminal_t * This, ELpayConfirmOpReq_t* data);
int elpay_confirm_operation_resp_get(ELpayTerminal_t * This);

int elpay_operation_status_req_send(ELpayTerminal_t * This, ELpayOpStatusReq_t* data);
int elpay_operation_status_resp_get(ELpayTerminal_t * This);

int elpay_report_sales_req_send(ELpayTerminal_t * This, ELpayReportSalesReq_t* data);
int elpay_report_sales_resp_get(ELpayTerminal_t * This, char* text, size_t size);

int elpay_report_financial_req_send(ELpayTerminal_t * This, ELpayReportFinancialReq_t* data);
int elpay_report_financial_resp_get(ELpayTerminal_t * This, char* text, size_t size);

int elpay_report_turn_req_send(ELpayTerminal_t * This, ELpayReportTurnReq_t* data);
int elpay_report_turn_resp_get(ELpayTerminal_t * This, char* text, size_t size);

int elpay_report_encashment_req_send(ELpayTerminal_t * This, ELpayReportEncashmentReq_t* data);
int elpay_report_encashment_resp_get(ELpayTerminal_t * This, char* text, size_t size);

#endif /* ELPAY_TRANSPORT_H */
