/* 
 * File:   operationStatus.c
 */

#include "SERVICES/ELpay/internal/operationStatus.h"

static const char * statusText[][2] =
{
  {"successfull", "готово"},
  {"unsupported operation", "неподдерживаемя операция"},
  {"general failure", "ошибка"},
  {"network failure", "ошибка сети"},
  
  {"reqest failure", "ошибка запроса"},
  {"response failure", "ошибка ответа"},

  {"server error", "ошибка сервера"}
};

const char * elpay_status_str(ELpayOperationStatus_t status)
{
  return statusText[status][0];
}

const char * elpay_status_str_rus(ELpayOperationStatus_t status)
{
  return statusText[status][1];
}
