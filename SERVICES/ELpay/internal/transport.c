
#define LOG_HEADER "ELpay"
#define LOG_LEVEL INF
#include "ELpay_port.h"
#include "ELpay/protocol.h"
#include "SERVICES/ELpay/internal/transport.h"

#include <stdlib.h>
#include <string.h>

int elpay_http_signature(const char * signKey, char * jsonBuf, char * signBuf, int signBufSize)
{
  ELpaySign_t sign;
  ELpaySignEnc_t b64_sign;
  if (elpay_sign(signKey, jsonBuf, sign, b64_sign))
    return -1;
  snprintf(signBuf, signBufSize, "X-Spek-Signature: %s\r\n", b64_sign);
  return 0;
}

//------------------------------------------------------------------------------

static int elpay_send(TcpSocket_t * Socket, char * data, size_t len,
                    const char *  userHeader)
{
  LOG_FXN(INF, START);
  char postBuf[65536];
  size_t postLen = elpay_http_post(data, len, userHeader, postBuf, sizeof(postBuf));

  LOG(DBG, "send data:\n%s", postBuf);

  int res = TcpSocket_send(Socket, postBuf, postLen);
  if (res)
    LOG_FXN(INF, FAIL);
  else
    LOG_FXN(INF, OK);
  return res;
}

static int elpay_send_with_sign(TcpSocket_t * Socket, char * data, size_t len, const char * sign_key)
{
  LOG_FXN(INF, START);
  int res;
  char httpHeaderBuf[1024];
  res = elpay_http_signature(sign_key, data, httpHeaderBuf, sizeof(httpHeaderBuf));
  if (res)
  {
    LOG_FXN_MSG(WRN, FAIL, "http signature gen failure: %d", res);
    return res;
  }

  res = elpay_send(Socket, data, len, httpHeaderBuf);
  if (res)
    LOG_FXN(INF, FAIL);
  else
    LOG_FXN(INF, OK);
  return res;
}

#define RCV_REPEAT_INIT \
  int repeatSource = 0, repeatCounter = 0;
#define RCV_REPEAT_REINIT \
  repeatSource = 0; repeatCounter = 0;
#define RCV_REPEAT_SOURCE_INIT \
  int curRepeatSource = 0; const char * curRepeatSourceName = 0;
#define RCV_REPEAT_SET_SOURCE(name) \
  ++curRepeatSource; curRepeatSourceName = name;

#define RCV_REPEAT_HANDLER \
{ \
  if (curRepeatSource != repeatSource) \
  { \
    LOG_FXN_MSG(WRN, NONE, "start repeating for %s", curRepeatSourceName); \
    repeatCounter = 0; \
    repeatSource = curRepeatSource; \
  } \
  if (repeatCounter > ELPAY_TERMINAL_RECEIVE_REPEATCOUNT) \
  { \
    LOG_FXN_MSG(WRN, NONE, "stop repeating for %s", curRepeatSourceName); \
    break; \
  } \
  ++repeatCounter; \
  continue; \
}

static int elpay_socket_read(TcpSocket_t * Socket, char * buf, size_t bufSize)
{
  buf[0] = 0;
  if (bufSize <= 1)
  {
    LOG_FXN_MSG(WRN, FAIL, "buffer size=%d invalid", bufSize);
    return -1;
  }

  bufSize -= 1; // для символа нуля в конце

  uint16_t buflen = bufSize;

  int res = TcpSocket_receive(Socket, buf, &buflen, ELPAY_TERMINAL_RCV_TIMEOUT);
  if (res)
  {
    LOG_FXN_MSG(WRN, FAIL, "%s", network_resStr(res));
    return -2;
  }

  if (buflen)
  {
    buf[buflen] = 0;
    LOG_FXN_MSG(DBG,NONE, "size = %d, data:\n%s", buflen, buf);
  }
  return buflen;
}

#define RCV_RETURN_ERR(str...) \
{ \
  snprintf(This->ErrBuf,This->ErrBufSize, str); \
  return -1; \
}

#include "http_parser.h"

typedef struct
{
  bool isMessageProcessing, isChunk;

  char ** content;
  long contentLen;
} parser_data_t;

#define HTTP_CB(dst) http_##dst##_handler
#define HTTP_EVENT_CB_ARGS (http_parser * parser)
#define HTTP_DATA_CB_ARGS (http_parser * parser, const char * at, size_t length)

#define HTTP_EVENT_CB_HEADER(dst) static int HTTP_CB(dst) HTTP_EVENT_CB_ARGS
#define HTTP_DATA_CB_HEADER(dst) static int HTTP_CB(dst) HTTP_DATA_CB_ARGS

#define HTTP_NO_LOG
#ifndef HTTP_NO_LOG
#define HTTP_EVENT_FXN_DUMMY \
{ \
  (void)parser; \
  LOG_FXN(DBG,START); \
  return 0; \
}
#define HTTP_DATA_FXN_DUMMY \
{ \
  (void)parser; \
  char __buf[length+1]; \
  memcpy(__buf, at, length); __buf[length] = 0; \
  LOG_FXN_MSG(DBG,NONE, "%s", __buf); \
  return 0; \
}
#else
#define HTTP_EVENT_FXN_DUMMY \
{ \
  (void)parser; \
  return 0; \
}
#define HTTP_DATA_FXN_DUMMY \
{ \
  (void)parser; \
  (void)length; \
  return 0; \
}
#endif

HTTP_EVENT_CB_HEADER(message_begin)
{
#ifndef HTTP_NO_LOG
  LOG_FXN(DBG,START);
#endif
  parser_data_t * data = ((parser_data_t*)parser->data);
  data->isMessageProcessing = true;
  data->contentLen = 0;
  return 0;
}

HTTP_DATA_CB_HEADER(url)
HTTP_DATA_FXN_DUMMY

HTTP_DATA_CB_HEADER(status)
HTTP_DATA_FXN_DUMMY

HTTP_DATA_CB_HEADER(header_field)
HTTP_DATA_FXN_DUMMY

HTTP_DATA_CB_HEADER(header_value)
HTTP_DATA_FXN_DUMMY

HTTP_EVENT_CB_HEADER(headers_complete)
HTTP_EVENT_FXN_DUMMY

HTTP_DATA_CB_HEADER(body)
{
#ifndef HTTP_NO_LOG
  LOG_FXN_PREMSG(DBG,START, "length = %d", length);
#endif
  parser_data_t * data = ((parser_data_t*)parser->data);

  char ** content = (data->content);
  int idx = data->contentLen;

  data->contentLen += length;
  *content = realloc(*content, data->contentLen+1);

  memcpy(*content+idx, at, length);
  (*content)[data->contentLen] = 0;

  LOG_FXN_MSG(INF,OK, "content length = %d", data->contentLen);
  return 0;
}

HTTP_EVENT_CB_HEADER(message_complete)
{
#ifndef HTTP_NO_LOG
  LOG_FXN(DBG,START);
#endif
  ((parser_data_t*)parser->data)->isMessageProcessing = false;
  return 0;
}

HTTP_EVENT_CB_HEADER(chunk_header)
{
#ifndef HTTP_NO_LOG
  LOG_FXN(DBG,START);
#endif
  parser_data_t * data = ((parser_data_t*)parser->data);
  data->isChunk = true;
  return 0;
}

HTTP_EVENT_CB_HEADER(chunk_complete)
HTTP_EVENT_FXN_DUMMY

long elpay_receive(ELpayTerminal_t * This, char ** content)
{
  LOG_FXN(INF, START);

  This->ErrBuf[0] = '\0';
  *content = 0;
//  *content = malloc(200000);
  const size_t bufSize = ELPAY_TERMINAL_SOCKET_READ_BUFLEN;
  char buf[bufSize];

  http_parser_settings settings;
  memset(&settings, 0, sizeof(&settings));
  settings.on_message_begin = HTTP_CB(message_begin);
  settings.on_url = HTTP_CB(url);
  settings.on_status = HTTP_CB(status);
  settings.on_header_field = HTTP_CB(header_field);
  settings.on_header_value = HTTP_CB(header_value);
  settings.on_headers_complete = HTTP_CB(headers_complete);
  settings.on_body = HTTP_CB(body);
  settings.on_message_complete = HTTP_CB(message_complete);
  settings.on_chunk_header = HTTP_CB(chunk_header);
  settings.on_chunk_complete = HTTP_CB(chunk_complete);

  http_parser parser;
  http_parser_init(&parser, HTTP_RESPONSE);

  parser_data_t data;
  parser.data = &data;

  data.content = content;

  RCV_REPEAT_INIT
  do
  {
    RCV_REPEAT_SOURCE_INIT

    RCV_REPEAT_SET_SOURCE("read")
    int len = elpay_socket_read(&This->Socket, buf, bufSize);
    if (!len)
    {
      if (data.isMessageProcessing)
      {
        LOG_FXN_MSG(WRN,NONE,"HTTP message is incomplete");
        break;
      }
      RCV_REPEAT_HANDLER
    }
    else if (len < 0)
      RCV_RETURN_ERR("socket read failure")

    size_t nparsed = http_parser_execute(&parser, &settings, buf, len);
    if (nparsed != len)
      RCV_RETURN_ERR("http parse failure: %s",
                     http_errno_description(HTTP_PARSER_ERRNO(&parser)))
  } while(data.isMessageProcessing);

  if (!(*content))
  {
    RCV_RETURN_ERR("content buf ptr is null");
  }
  else
    LOG_FXN_MSG(INF,NONE, "content lenght = %d", data.contentLen);
  if (data.contentLen > 0)
  {
    LOG_FXN_MSG(INF,NONE, "received data (strlen = %d): ", strlen(*content));
    log_msg_raw(*content);
  }
  return data.contentLen;
}

//------------------------------------------------------------------------------

#define REQ_SEND_CHECK_AND_RETURN(x, res, formatMsg...) \
  if (x) \
  { \
    snprintf(This->ErrBuf,This->ErrBufSize, formatMsg); \
    LOG_FXN_MSG(WRN, FAIL, formatMsg); \
    return res; \
  }

#define REQ_SEND_BASE \
  LOG_FXN(INF, START); \
  This->ErrBuf[0] = '\0'; \
  const size_t jsonBufSize = 65536; \
  char jsonBuf[jsonBufSize]; \
  size_t jsonBufLen;

#define REQ_SEND_CHECK_GEN_AND_RETURN \
  REQ_SEND_CHECK_AND_RETURN(jsonBufLen <= 0,-1, "req gen failure")

#define REQ_SEND_FXN_AND_RETURN(fxn,...) \
  int res = elpay_##fxn(&This->Socket, jsonBuf, jsonBufLen, __VA_ARGS__); \
  REQ_SEND_CHECK_AND_RETURN(res,res, "send failure")\
  LOG_FXN(INF, OK); \
  return res;

int elpay_reg_req_send(ELpayTerminal_t * This, ELpayRegReq_t * data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_reg_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send, "")
}

int elpay_auth_req_send(ELpayTerminal_t * This, ELpayAuthReq_t * data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_auth_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_prodlist_req_send(ELpayTerminal_t * This, ELpayProdListReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_prodlist_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey);
}

int elpay_receipt_template_req_send(ELpayTerminal_t * This, ELpayReceiptTemplReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_receipt_template_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_create_operation_req_send(ELpayTerminal_t * This, ELpayCreateOpReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_create_operation_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_confirm_operation_req_send(ELpayTerminal_t * This, ELpayConfirmOpReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_confirm_operation_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_operation_status_req_send(ELpayTerminal_t * This, ELpayOpStatusReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_operation_status_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_balance_req_send(ELpayTerminal_t * This, ELpayBalanceReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_balance_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_report_sales_req_send(ELpayTerminal_t* This, ELpayReportSalesReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_report_sales_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_report_financial_req_send(ELpayTerminal_t* This, ELpayReportFinancialReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_report_financial_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_report_turn_req_send(ELpayTerminal_t* This, ELpayReportTurnReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_report_turn_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}

int elpay_report_encashment_req_send(ELpayTerminal_t* This, ELpayReportEncashmentReq_t* data)
{
  REQ_SEND_BASE

  jsonBufLen = elpay_report_encashment_req_gen(data, jsonBuf, jsonBufSize);
  REQ_SEND_CHECK_GEN_AND_RETURN

  REQ_SEND_FXN_AND_RETURN(send_with_sign, This->SignKey)
}


//------------------------------------------------------------------------------

#define RESP_GET_RETURN(res) \
do { \
  if (buf) free(buf); \
  if (res) \
    LOG_FXN_MSG(INF,FAIL,"retval = %d",res); \
  else \
    LOG_FXN(INF,OK); \
  return res; \
} while(0)

#define RESP_GET_BASE \
  LOG_FXN_MSG(INF, NONE, "response waiting..."); \
  This->ErrBuf[0] = '\0'; \
  char * buf; \
  int bufLen = elpay_receive(This, &buf); \
  if (!buf || (bufLen < 0)) \
  { \
    RESP_GET_RETURN(-1); \
  } \
  LOG_FXN_MSG(INF, NONE, "response parsing..."); \
  int res;

#define RESP_GET_CHECK_PARSE_RES_AND_RETURN \
  if (res) \
  { \
    RESP_GET_RETURN(res); \
  }

int elpay_reg_resp_get(ELpayTerminal_t * This, const char * regcode)
{
  RESP_GET_BASE

  res = elpay_reg_resp_parse(buf, regcode, &This->id, &This->SignKey,
                                 This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(res);
}

int elpay_auth_resp_get(ELpayTerminal_t * This)
{
  RESP_GET_BASE

  res = elpay_auth_resp_parse(buf, &This->session,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(res);
}

extern void ELpayTerminal_saveProdList(ELpayTerminal_t * This, BYTE * buf, int len);

int elpay_prodlist_resp_get(ELpayTerminal_t * This, ELpayProdList_t * list)
{
  RESP_GET_BASE

  res = elpay_prodlist_resp_parse(buf, list,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  ELpayTerminal_saveProdList(This,buf,bufLen);

  RESP_GET_RETURN(0);
}

int elpay_receipt_template_resp_get(ELpayTerminal_t * This)
{
  RESP_GET_BASE

//  res = elpay_auth_resp_parse(buf, ThisData,
//                                  This->ErrBuf,This->ErrBufSize);
//  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_balance_resp_get(ELpayTerminal_t * This, ELpayBalance_t * balance)
{
  RESP_GET_BASE

  res = elpay_balance_resp_parse(buf, balance,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_create_operation_resp_get(ELpayTerminal_t * This)
{
  RESP_GET_BASE

  res = elpay_create_operation_resp_parse(buf, &This->tran.id,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_confirm_operation_resp_get(ELpayTerminal_t * This)
{
  RESP_GET_BASE

  res = elpay_confirm_operation_resp_parse(buf, &This->tran.state,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_operation_status_resp_get(ELpayTerminal_t * This)
{
  RESP_GET_BASE

  res = elpay_operation_status_resp_parse(buf, &This->tran.state,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_report_sales_resp_get(ELpayTerminal_t* This, char * text, size_t size)
{
  RESP_GET_BASE

  res = elpay_report_sales_resp_parse(buf, text, size,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_report_financial_resp_get(ELpayTerminal_t* This, char * text, size_t size)
{
  RESP_GET_BASE

  res = elpay_report_financial_resp_parse(buf, text, size,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_report_turn_resp_get(ELpayTerminal_t* This, char * text, size_t size)
{
  RESP_GET_BASE

  res = elpay_report_turn_resp_parse(buf, text, size,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}

int elpay_report_encashment_resp_get(ELpayTerminal_t* This, char * text, size_t size)
{
  RESP_GET_BASE

  res = elpay_report_encashment_resp_parse(buf, text, size,
                                  This->ErrBuf,This->ErrBufSize);
  RESP_GET_CHECK_PARSE_RES_AND_RETURN

  RESP_GET_RETURN(0);
}
