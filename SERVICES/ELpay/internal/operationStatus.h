/* 
 * File:   operationStatus.h
 */

#ifndef ELPAY_OPERATIONSTATUS_H
#define ELPAY_OPERATIONSTATUS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
  elpay_status_OK = 0,
  elpay_status_UnsupportedOperation,
  elpay_status_GeneralFailure,
  elpay_status_NetworkFailure,

  elpay_status_RequestFailure,
  elpay_status_ResponseFailure,

  elpay_status_ServerResult
} ELpayOperationStatus_t;

const char * elpay_status_str(ELpayOperationStatus_t status);
const char * elpay_status_str_rus(ELpayOperationStatus_t status);

#ifdef __cplusplus
}
#endif

#endif /* ELPAY_OPERATIONSTATUS_H */

