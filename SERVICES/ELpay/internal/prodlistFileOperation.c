#define LOG_HEADER "ELpayTerm"
#define LOG_LEVEL DBG
#include "SYSTEM/logs.h"

#include "../Terminal.h"
#include "ELpay/protocol.h"
#include "ELpay_port.h"

#include "SYSTEM/file.h"

void ELpayTerminal_saveProdList(ELpayTerminal_t * This, BYTE * buf, int len)
{
  LOG_FXN(INF, START);

  This->ProdListFID = 0;

  if (file_exist(This->ProdListFileName))
    if (!file_remove(This->ProdListFileName))
    {
      LOG_FXN(WRN,FAIL);
      return;
    }

  BYTE omode = O_CREATE;
  This->ProdListFID = file_open(This->ProdListFileName, omode);
  if (This->ProdListFID < 0)
  {
    LOG_FXN(WRN,FAIL);
    return;
  }

  if (file_write(This->ProdListFID, buf, len) != len)
    LOG_FXN(WRN,FAIL);

  if (!file_close(This->ProdListFID))
    LOG_FXN(WRN, FAIL);
  else
    LOG_FXN(INF, OK);
}

bool ELpayTerminal_loadProdList(ELpayTerminal_t * This, ELpayProdList_t * list)
{
  LOG_FXN(INF, START);
  if (!file_exist(This->ProdListFileName))
  {
    LOG_FXN(INF, ABORT);
    return 0;
  }

  This->ProdListFID = file_open(This->ProdListFileName, O_RDWR);
  if (This->ProdListFID < 0)
  {
    LOG_FXN(WRN, FAIL);
    return 0;
  }

  long size = file_size(This->ProdListFileName);
  BYTE * buf = malloc(size+1);
  assert(buf);

#define FREE_AND_CLOSE \
do{ \
  free(buf); \
  file_close(This->ProdListFID); \
} while(0)

  if (file_read(This->ProdListFID,buf,size) != size)
  {
    FREE_AND_CLOSE;
    LOG_FXN(WRN, FAIL);
    return 0;
  }
  buf[size] = 0;

  int res = elpay_prodlist_resp_parse(buf,list,This->ErrBuf,This->ErrBufSize);
  if (res)
    LOG_FXN_MSG(WRN,FAIL,"parse error: %s", This->ErrBuf);
  LOG_FXN_MSG(INF,OK,"loaded (size = %d)", size);
  FREE_AND_CLOSE;
  return res == 0;

#undef FREE_AND_CLOSE
}
