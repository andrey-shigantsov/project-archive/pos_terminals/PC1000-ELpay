#ifndef ELPAY_OPERATIONLIST_H
#define ELPAY_OPERATIONLIST_H

#include "config.h"
#include "SYSTEM/base.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ELPAY_OPLIST_ACCOUNT_BUFSIZE 32

#ifndef ELPAY_OPLIST_MAX_ITEMS_COUNT
#define ELPAY_OPLIST_MAX_ITEMS_COUNT 255
#endif

typedef struct
{
  char account[ELPAY_OPLIST_ACCOUNT_BUFSIZE+1];
  double nominal;
  bool status;
} ELpay_OperationListItem_t;

typedef struct
{
  char FileName[256];
  int fid;

  char errBuf[256];

  ELpay_OperationListItem_t Items[ELPAY_OPLIST_MAX_ITEMS_COUNT];
  int ItemsCount, ItemsWriteIdx;
} ELpay_OperationList_t;

void init_ELpay_OperationList(ELpay_OperationList_t * This, const char* listFileName);

/* возвращает индекс елемента */
int ELpay_OperationList_append(ELpay_OperationList_t * This, const char * account, double nominal, bool status);

INLINE int ELpay_OperationList_ItemsCount(ELpay_OperationList_t * This){return This->ItemsCount;}

ELpay_OperationListItem_t * ELpay_OperationList_Item(ELpay_OperationList_t * This, int i);

void ELpay_OperationList_update_file(ELpay_OperationList_t * This);

void ELpay_OperationList_clear(ELpay_OperationList_t * This);

#ifdef __cplusplus
}
#endif

#endif // ELPAY_OPERATIONLIST_H
