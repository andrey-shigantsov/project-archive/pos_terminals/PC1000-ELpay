/*
 * Terminal.h
 *
 *  Created on: 20 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SERVICES_ELPAY_TERMINAL_H_
#define SERVICES_ELPAY_TERMINAL_H_

#include "config.h"

#include "ELpay/types.h"
#include "internal/operationStatus.h"

#include "OperationList.h"

#include "SYSTEM/NETWORK/TcpSocket.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  ELpayTermId_t id;
  ELpaySign_t SignKey;
  ELpaySessionId_t session;

  struct
  {
    ELpayTransactionId_t id;
    ELpayTransactionState_t state;
    int opListIdx;
  } tran;

  ELpay_OperationList_t opList;

  char ProdListFileName[256];
  int ProdListFID;

  char HostAddr[256];
  uint16_t ipPort;
  TcpSocket_t Socket;

  int last_res;
  char ErrBuf[256];
  size_t ErrBufSize;
} ELpayTerminal_t;

void init_ELpayTerminal(ELpayTerminal_t * This, int SocketNum, const char * addr, const char* OperationListFile, const char * ProdListFile);
void close_ELpayTerminal(ELpayTerminal_t * This);

void ELpayTerminal_setId(ELpayTerminal_t * This, const char * id);
int ELpayTerminal_setSignKey_b64(ELpayTerminal_t * This, const char * sign_key);

bool ELpayTerminal_loadProdList(ELpayTerminal_t * This, ELpayProdList_t * list);

ELpayOperationStatus_t ELpayTerminal_registration(ELpayTerminal_t* This, const char * code);
ELpayOperationStatus_t ELpayTerminal_authorization(ELpayTerminal_t* This, const char * login, const char * password);
ELpayOperationStatus_t ELpayTerminal_get_balance(ELpayTerminal_t* This, ELpayBalance_t * balance);
ELpayOperationStatus_t ELpayTerminal_get_products_list(ELpayTerminal_t* This, ELpayProdList_t * list);
ELpayOperationStatus_t ELpayTerminal_get_receipt_template(ELpayTerminal_t* This, const char * name);
ELpayOperationStatus_t ELpayTerminal_create_operation(ELpayTerminal_t* This, ELpayProductData_t * ProdData);
ELpayOperationStatus_t ELpayTerminal_confirm_operation(ELpayTerminal_t* This);
ELpayOperationStatus_t ELpayTerminal_get_operation_status(ELpayTerminal_t* This);
ELpayOperationStatus_t ELpayTerminal_get_report_sales(ELpayTerminal_t* This, char* date1, char* date2, bool isDetailed, char * text, size_t size);
ELpayOperationStatus_t ELpayTerminal_get_report_financial(ELpayTerminal_t* This, char* date, char * text, size_t size);
ELpayOperationStatus_t ELpayTerminal_get_report_turn(ELpayTerminal_t* This, char * text, size_t size);
ELpayOperationStatus_t ELpayTerminal_get_report_encashment(ELpayTerminal_t* This, char* date, char * text, size_t size);

void ELpayTerminal_cancel_operation(ELpayTerminal_t * This);

#ifdef __cplusplus
}
#endif

#endif /* SERVICES_ELPAY_TERMINAL_H_ */
