/*
 * Terminal.c
 *
 *  Created on: 20 февр. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "ELpayTerm"
#define LOG_LEVEL DBG
#include "SYSTEM/logs.h"

#include "Terminal.h"
#include "internal/transport.h"
#include "ELpay/protocol.h"
#include "ELpay_port.h"

#include "SYSTEM/NETWORK/internal.h"

static void set_elpay_mvi(ELpayMVI_t * mvi)
{
  mvi->model = "PC1000";
  mvi->os_ver = "JOS";
  mvi->imei = "";
  mvi->phone_num = "";
  mvi->lac = "65535";
  mvi->cid = "65535";
}

static const char elpay_POS_OS_ver[] = "1.0.0";
static const char elpay_App_ver[] = "1.0.0";

static int socket_open(ELpayTerminal_t * This)
{
  int res;
#ifdef ELPAY_TERMINAL_OPN_CYCLE
  int res2;
  TIMER_DO(USER_TIMER, ELPAY_TERMINAL_OPN_TIMEOUT)
  {
    LOG_FXN_MSG(DBG, NONE, "try...");
#endif
    res = TcpSocket_open(&This->Socket, This->HostAddr, 80);
#ifdef ELPAY_TERMINAL_OPN_CYCLE
    res2 = TcpSocket_checkLink(&This->Socket);
  } TIMER_WHILE(res2);
  if (res2)
  {
    res = WLS_TIMEOUT;
    LOG_FXN_MSG(WRN, FAIL, "timeout: res2 = %s", network_resStr(res2));
  }
  else
#endif
  if (res)
    LOG_FXN(DBG, FAIL);
  else
    LOG_FXN(DBG, OK);
  return res;
}

void init_ELpayTerminal(ELpayTerminal_t * This, int SocketNum, const char * addr, const char * OperationListFile, const char * ProdListFile)
{
  This->ErrBufSize = sizeof(This->ErrBuf);
  This->ErrBuf[0] = '\0';

  init_TcpSocket(&This->Socket, SocketNum, 1);
  strncpy(This->HostAddr, addr, sizeof(This->HostAddr));

  This->ProdListFID = 0;
  strncpy(This->ProdListFileName, ProdListFile, sizeof(This->ProdListFileName));

  init_ELpay_OperationList(&This->opList, OperationListFile);

//  socket_open(This);
}

void close_ELpayTerminal(ELpayTerminal_t * This)
{
  TcpSocket_close(&This->Socket);
}

void ELpayTerminal_setId(ELpayTerminal_t * This, const char * id)
{
  strncpy(This->id, id, ELPAY_JSON_TERM_ID_BUFSIZE);
  This->id[ELPAY_JSON_TERM_ID_BUFSIZE-1] = '\0';
}

int ELpayTerminal_setSignKey_b64(ELpayTerminal_t * This, const char * sign_key)
{
  int len = strlen(sign_key);
  int encSize = elpay_base64_encsize(ELPAY_JSON_SIGNSIZE);
  if (len != encSize)
  {
    LOG(WRN, "encSK size failure: %d != %d", len, encSize);
    return -1;
  }

  int res = elpay_base64_decode((char*)sign_key, len, This->SignKey, ELPAY_JSON_SIGN_BUFSIZE);
  if (res)
    return -2;
  This->SignKey[ELPAY_JSON_SIGN_BUFSIZE-1] = '\0';
  return 0;
}

#define FXN_ARGS(...) __VA_ARGS__

#define NET_FXN_AND_RETURN(fxn, args) \
{ \
  int socketRes = fxn(args); \
  if (socketRes) \
  { \
    const char * __resStr = network_resStr(socketRes); \
    snprintf(This->ErrBuf, This->ErrBufSize, "%s:\n%s", #fxn, __resStr); \
    return elpay_status_NetworkFailure;\
  } \
}

#define OP_INIT \
LOG(INF, OP_NAME "..."); \
ELpayOperationStatus_t res = elpay_status_OK; \
This->ErrBuf[0] = '\0'; \
if (!TcpSocket_isOpenned(&This->Socket)) \
{ \
  NET_FXN_AND_RETURN(socket_open, FXN_ARGS(This)) \
}

#define OP_RES res

#define OP_FREE_AND_RETURN \
if (res) \
  LOG(INF, OP_NAME " result: %s:\n%s", elpay_status_str(res), This->ErrBuf); \
else \
  LOG(INF, OP_NAME " result: %s", elpay_status_str(res)); \
return res;

#define OP_EXEC(name, reqParams, respParams) \
if (This->last_res = elpay_##name##_req_send(reqParams)) \
  res = elpay_status_RequestFailure; \
else if (This->last_res = elpay_##name##_resp_get(respParams)) \
{ \
  if (This->last_res < 0) \
    res = elpay_status_ResponseFailure; \
  else \
    res = elpay_status_ServerResult; \
}

ELpayOperationStatus_t ELpayTerminal_registration(ELpayTerminal_t * This, const char * code)
{
#define OP_NAME "registration"
  OP_INIT

  ELpayRegReq_t data;
  data.code = code;
  set_elpay_mvi(&data.mvi);
  data.pos_os_ver = elpay_POS_OS_ver;
  data.app_ver = elpay_App_ver;

  OP_EXEC(reg,FXN_ARGS(This,&data),FXN_ARGS(This,data.code))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_authorization(ELpayTerminal_t* This, const char * login, const char * password)
{
#define OP_NAME "authorization"
  OP_INIT

  ELpayAuthReq_t data;
  data.termId = This->id;
  data.login = login;
  data.password = password;
  set_elpay_mvi(&data.mvi);
  data.pos_os_ver = elpay_POS_OS_ver;
  data.app_ver = elpay_App_ver;

  OP_EXEC(auth,FXN_ARGS(This,&data),FXN_ARGS(This))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_balance(ELpayTerminal_t* This, ELpayBalance_t * balance)
{
#define OP_NAME "get balance"
  OP_INIT

  ELpayBalanceReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;

  OP_EXEC(balance,FXN_ARGS(This,&data),FXN_ARGS(This,balance))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_products_list(ELpayTerminal_t* This, ELpayProdList_t * list)
{
#define OP_NAME "get products list"
  OP_INIT

  ELpayProdListReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;

  OP_EXEC(prodlist,FXN_ARGS(This,&data),FXN_ARGS(This,list))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_receipt_template(ELpayTerminal_t* This, const char * name)
{
#define OP_NAME "get receipt template"
  OP_INIT

  ELpayReceiptTemplReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.name = name;

  OP_EXEC(receipt_template,FXN_ARGS(This,&data),FXN_ARGS(This))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_create_operation(ELpayTerminal_t * This, ELpayProductData_t * ProdData)
{
#define OP_NAME "create operation"
  OP_INIT

  ELpayCreateOpReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.pid = ProdData->pid;
  data.act = ProdData->fields[0].id;
  data.msisdn = ProdData->fields[0].value;
  data.nominal = ProdData->nominal;
  data.commission = ProdData->commission;

  OP_EXEC(create_operation,FXN_ARGS(This,&data),FXN_ARGS(This))

  if (OP_RES == elpay_status_OK)
  {
    This->tran.opListIdx = ELpay_OperationList_append(&This->opList,ProdData->fields[0].value,ProdData->nominal,false);
    ELpay_OperationList_update_file(&This->opList);
  }

  OP_FREE_AND_RETURN
#undef OP_NAME
}

static void update_operation_status(ELpayTerminal_t* This)
{
  if(This->tran.state.id == elpay_tnst_Successfull)
  {
    ELpay_OperationListItem_t * item = ELpay_OperationList_Item(&This->opList, This->tran.opListIdx);
    assert(item);
    item->status = true;
    ELpay_OperationList_update_file(&This->opList);
  }
}

ELpayOperationStatus_t ELpayTerminal_confirm_operation(ELpayTerminal_t* This)
{
#define OP_NAME "confirm operation"
  OP_INIT

  ELpayConfirmOpReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.tranId = This->tran.id;

  OP_EXEC(confirm_operation,FXN_ARGS(This,&data),FXN_ARGS(This))

  if (OP_RES == elpay_status_OK)
    update_operation_status(This);

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_operation_status(ELpayTerminal_t * This)
{
#define OP_NAME "operation status"
  OP_INIT

  ELpayOpStatusReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.tranId = This->tran.id;

  OP_EXEC(operation_status,FXN_ARGS(This,&data),FXN_ARGS(This))

  if (OP_RES == elpay_status_OK)
    update_operation_status(This);

  OP_FREE_AND_RETURN
#undef OP_NAME
}

void ELpayTerminal_cancel_operation(ELpayTerminal_t * This)
{

}

ELpayOperationStatus_t ELpayTerminal_get_report_sales(ELpayTerminal_t* This,
                                                      char * date1, char * date2, bool isDetailed,
                                                      char* text, size_t size)
{
#define OP_NAME "report sales"
  OP_INIT

  ELpayReportSalesReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.date1 = date1;
  data.date2 = date2;
  data.isDetailed = isDetailed;

  OP_EXEC(report_sales,FXN_ARGS(This,&data),FXN_ARGS(This,text,size))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_report_financial(ELpayTerminal_t* This,
                                                          char* date,
                                                          char * text, size_t size)
{
#define OP_NAME "report financial"
  OP_INIT

  ELpayReportFinancialReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.date = date;

  OP_EXEC(report_financial,FXN_ARGS(This,&data),FXN_ARGS(This,text,size))

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_report_turn(ELpayTerminal_t* This, char * text, size_t size)
{
#define OP_NAME "report turn"
  OP_INIT

  ELpayReportTurnReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;

  OP_EXEC(report_turn,FXN_ARGS(This,&data),FXN_ARGS(This,text,size))

  if (OP_RES == elpay_status_OK)
    update_operation_status(This);

  OP_FREE_AND_RETURN
#undef OP_NAME
}

ELpayOperationStatus_t ELpayTerminal_get_report_encashment(ELpayTerminal_t* This,
                                                           char* date,
                                                           char * text, size_t size)
{
#define OP_NAME "report sales"
  OP_INIT

  ELpayReportEncashmentReq_t data;
  data.base.termId = This->id;
  data.base.session = This->session;
  data.base.app_ver = elpay_App_ver;
  data.date = date;

  OP_EXEC(report_encashment,FXN_ARGS(This,&data),FXN_ARGS(This,text,size))

  OP_FREE_AND_RETURN
#undef OP_NAME
}
