#define LOG_HEADER "SYSTEM"
#define LOG_LEVEL INF
#include "control.h"

#include "NETWORK/System.h"

#include "DRIVERS/keyboard.h"
#include "DRIVERS/printer.h"
#include "GUI/System.h"

BYTE system_isLogo = 0;
static BYTE flag_isRunning = 0, flag_needStop = 0;
static BYTE tickTimer = SYSTICK_TIMER, tick100msCount = 1;

int system_drawLogo(BYTE * logo)
{
  LOG_FXN(INF,START);

  system_isLogo = logo != 0;
  if (system_isLogo)
    Lib_LcdDrawLogo(logo);

  LOG_FXN(INF,OK);
  return 0;
}

int init_system()
{
  LOG_FXN(INF,START);

  init_driver_printer();

  init_driver_keyboard(gui_key_handler, gui_key_edit_finished);
  init_gui();

  flag_needStop = 0;

  LOG_FXN(INF,OK);
  return 0;
}

int system_exec()
{
  Lib_SetTimer(tickTimer, tick100msCount);
  gui_tick_handler();

  flag_isRunning = 1;
  while(flag_isRunning)
  {
    if (Lib_CheckTimer(tickTimer) == 0)
    {
      Lib_SetTimer(tickTimer, tick100msCount);
      keyboard_tick_handler();
      gui_tick_handler();

      keyboard_handler();
    }
    if (flag_needStop)
      flag_isRunning = 0;
    else
      Lib_DelayMs(100);
  }
  return 0;
}

void system_exit()
{
  flag_needStop = 1;
}
