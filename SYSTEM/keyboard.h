/* 
 * File:   keyboard.h
 */

#ifndef SYSTEM_KEYBOARD_H
#define SYSTEM_KEYBOARD_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum {keysMode_Int, keysMode_Float, keysMode_All} KeysMode_t;
void setKeysMode(KeysMode_t mode);
  
#define KEY_IS_NUM(key) ((key >= '0')&&(key <= '9'))
#define KEY_IS_LETTER(key) ((key >= 'a')&&(key <= 'z')||(key >= 'A')&&(key <= 'Z'))
#define KEY_IS_SPEC(key) ((key == '.')||(key == '*')||(key == '#')||(key == ','))

#define KEY_IS_TEXT(key) (KEY_IS_NUM(key) || KEY_IS_LETTER(key) || KEY_IS_SPEC(key))

void keyboard_flush();

BYTE wait_keypress();
void wait_keypress_and_check(BYTE key);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_KEYBOARD_H */

