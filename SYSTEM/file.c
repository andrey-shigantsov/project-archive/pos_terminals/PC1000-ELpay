#define LOG_HEADER "FILE"
#define LOG_LEVEL WRN
#include "SYSTEM/logs.h"

#include "file.h"

#define RES_HANDLER(res) \
do{ \
  if(res < 0) \
    LOG_FXN_MSG(WRN, FAIL, "code = %d", res); \
  else \
    LOG_FXN(INF, OK); \
} while(0)

#define RES_HANDLER_AND_RETURN(res) \
do{ \
  RES_HANDLER(res); \
  return res; \
} while(0)

int file_open(char * filename, BYTE mode)
{
  LOG_FXN(INF, START);

  int res = Lib_FileOpen(filename, mode);
  RES_HANDLER_AND_RETURN(res);
}

int file_write(int fid, BYTE * dat, int len)
{
  LOG_FXN(INF, START);

  int res = Lib_FileWrite(fid, dat, len);

  RES_HANDLER_AND_RETURN(res);
}

int file_read(int fid, BYTE * dat, int len)
{
  LOG_FXN(INF, START);

  int res = Lib_FileRead(fid, dat, len);

  RES_HANDLER_AND_RETURN(res);
}

int file_close(int fid)
{
  LOG_FXN(INF, START);

  int res = Lib_FileClose(fid);

  RES_HANDLER(res);
  return res == 0;
}

bool file_exist(char * filename)
{
  LOG_FXN(INF, START);

  int res = Lib_FileExist(filename);

  RES_HANDLER(res);
  return res >= 0;
}

long file_size(char * filename)
{
  LOG_FXN(INF, START);

  long res = Lib_FileSize(filename);

  RES_HANDLER_AND_RETURN(res);
}

bool file_remove(const char * filename)
{
  LOG_FXN(INF, START);

  int res = Lib_FileRemove(filename);

  RES_HANDLER(res);
  return res == 0;
}
