/* 
 * File:   keyboard.c
 */

#include "keyboard.h"
#include "SYSTEM/output.h"
#include "SYSTEM/keyboard.h"

static keyboard_key_handler_t key_handler = 0;
static key_edit_finished_t key_edit_finished = 0;

static bool isTickCounter, isNew;
static BYTE keymapIdx;
static BYTE prevKey;
static int tickCounter, tickCount = 7;

KeysMode_t keysMode;

static void reset()
{
  isTickCounter = 0;
  tickCounter = 0;

  isNew = 1;
  keymapIdx = 0;
  prevKey = 0;
}

void init_driver_keyboard(keyboard_key_handler_t key_handler0, key_edit_finished_t state_changed0)
{
  reset();
  
  key_handler = key_handler0;
  key_edit_finished = state_changed0;
  
  Lib_KbFlush();
}

static BYTE keymap_all[10][5] =
{
  {'*', '#',',', '0', '\0'},
  {'.', 'q','z', '1', '\0'},
  {'a', 'b','c', '2', '\0'},
  {'d', 'e','f', '3', '\0'},
  {'g', 'h','i', '4', '\0'},
  {'j', 'k','l', '5', '\0'},
  {'m', 'n','o', '6', '\0'},
  {'p', 'r','s', '7', '\0'},
  {'t', 'u','v', '8', '\0'},
  {'w', 'x','y', '9', '\0'}
};

static BYTE keymap_float[10][3] =
{
  {'0', '.', '\0'},
  {'1', '.', '\0'},
  {'2', '\0', '\0'},
  {'3', '\0', '\0'},
  {'4', '\0', '\0'},
  {'5', '\0', '\0'},
  {'6', '\0', '\0'},
  {'7', '\0', '\0'},
  {'8', '\0', '\0'},
  {'9', '\0', '\0'}
};

void setKeysMode(KeysMode_t mode)
{
  keysMode = mode;
  reset();
}

void keyboard_tick_handler()
{
  if (!isTickCounter) return;
  
  ++tickCounter;
  if (tickCounter >= tickCount)
  {
    reset();
    
    if (key_edit_finished)
      key_edit_finished();
  }
}

void keyboard_handler()
{
  if (Lib_KbCheck()) return;

  BYTE key = Lib_KbGetCh();
  tickCounter = 0;
  isTickCounter = 1;
  
  if ((keysMode != keysMode_Int) && KEY_IS_NUM(key))
  {
    BYTE idx = key - '0';

    if (key == prevKey)
    {
      if (isNew)
        isNew = 0;

      ++keymapIdx;
      bool isEnd = false;
      switch (keysMode)
      {
      case keysMode_All:
        isEnd = keymap_all[idx][keymapIdx] == '\0';
        break;
      case keysMode_Float:
        isEnd = keymap_float[idx][keymapIdx] == '\0';
        break;
      }
      if (isEnd)
        keymapIdx = 0;
    }
    else
    {
      prevKey = key;
      isNew = 1;
      keymapIdx = 0;
    }

    switch (keysMode)
    {
    case keysMode_All:
      key = keymap_all[idx][keymapIdx];
      break;
    case keysMode_Float:
      key = keymap_float[idx][keymapIdx];
      break;
    }
  }
  
  if (key_handler)
    key_handler(key, isNew);
}

void keyboard_flush()
{
  Lib_KbFlush();
}

BYTE wait_keypress()
{
  BYTE Key = 0;
  while(1)
  {
    if (Lib_KbCheck()) continue;

    Key = Lib_KbGetCh();
    break;
  }
  return Key;
}

void wait_keypress_and_check(BYTE key)
{
  BYTE Key = 0;
  while(1)
  {
    if (Lib_KbCheck()) continue;

    Key = Lib_KbGetCh();
    if ((Key == key)||(!key))
      return;
  }
}
