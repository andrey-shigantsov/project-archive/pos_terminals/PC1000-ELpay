/*
 * printer.h
 *
 *  Created on: 5 апр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_DRIVERS_PRINTER_H_
#define SYSTEM_DRIVERS_PRINTER_H_

#include "SYSTEM/base.h"

#ifdef __cplusplus
extern "C"{
#endif

int init_driver_printer();

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_DRIVERS_PRINTER_H_ */
