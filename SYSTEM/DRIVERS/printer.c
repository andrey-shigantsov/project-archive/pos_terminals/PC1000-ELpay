/*
 * printer.c
 *
 *  Created on: 5 апр. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "PRINTER"
#define LOG_LEVEL WRN
#include "printer.h"

#include "SYSTEM/logs.h"

#include <stdarg.h>

int init_driver_printer()
{
  int res;

  res = Lib_PrnInit();
  if (res)
  {
    LOG_FXN_MSG(WRN,FAIL, "init error code %d", res);
    return res;
  }

  Lib_PrnSetFont(16,16,0x33);
  Lib_PrnSetLeftIndent(0);
  Lib_PrnSetSpeed(23);
}

const char * printer_resStr(int res)
{
  switch (res)
  {
  default:
    return "invalid state";

  case PRN_BUSY:
    return "busy";
  case PRN_NOPAPER:
    return "no paper";
  case PRN_DATAERR:
    return "data error";
  case PRN_FAULT:
    return "fault";
  case PRN_TOOHEAT:
    return "too heat";
  case PRN_UNFINISHED:
    return "unfinished";
  case PRN_NOFONTLIB:
    return "no FontLib";
  case PRN_BUFFOVERFLOW:
    return "buffer overflow";
  case PRN_SETFONTERR:
    return "set font failure";
  case PRN_GETFONTERR:
    return "get font failure";
  case PRN_BATFORBID:
    return "low battery";
  case PRN_OK:
    return "print OK";
  }
}
