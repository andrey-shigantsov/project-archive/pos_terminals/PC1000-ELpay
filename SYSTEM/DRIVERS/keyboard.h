/* 
 * File:   keyboard.h
 */

#include "SYSTEM/base.h"

#ifndef SYSTEM_DRIVER_KEYBOARD_H
#define SYSTEM_DRIVER_KEYBOARD_H

#ifdef __cplusplus
extern "C" {
#endif
  
typedef void (*keyboard_key_handler_t)(BYTE key, BYTE isNew);
typedef void (*key_edit_finished_t)();

void init_driver_keyboard(keyboard_key_handler_t key_handler, key_edit_finished_t key_edit_finished);

void keyboard_tick_handler();
void keyboard_handler();

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_DRIVER_KEYBOARD_H */
