/*
 * logs.h
 *
 *  Created on: 25 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_LOGS_H_
#define SYSTEM_LOGS_H_

#include "config.h"
#include <stdio.h>

#ifdef __cplusplus
extern "C"{
#endif

void log_text_raw(const char * text);
void log_msg_raw(const char * msg);

#ifndef LOG_DBG_TEXT
#define LOG_DBG_TEXT "DEBUG: "
#endif
#ifndef LOG_INF_TEXT
#define LOG_INF_TEXT ""
#endif
#ifndef LOG_WRN_TEXT
#define LOG_WRN_TEXT "WARNING: "
#endif
#ifndef LOG_ERR_TEXT
#define LOG_ERR_TEXT "ERROR: "
#endif
#ifndef LOG_EXC_TEXT
#define LOG_EXC_TEXT "EXCEPTION: "
#endif

typedef enum {log_lvl_EXC, log_lvl_ERR, log_lvl_WRN, log_lvl_INF, log_lvl_DBG} LogLevel_t;
#define LOG_LVL(lvl) log_lvl_##lvl

#define LOG_PREHEADER_TEXT(lvl) LOG_##lvl##_TEXT

#ifndef LOG_PRINTF_BUFSIZE
#define LOG_PRINTF_BUFSIZE 65535
#endif

#ifdef LOG_PRINTF_USING_MALLOC
#include <malloc.h>
#define log_printf_raw(mode,...) \
do{ \
  int __logbufSize = snprintf(0, 0, ##__VA_ARGS__); \
  char * __logbuf = (char *) malloc(__logbufSize+1); \
  if (__logbuf) \
  { \
    snprintf(__logbuf, LOG_PRINTF_BUFSIZE, ##__VA_ARGS__); \
    if (__logbuf[0] == '\0') \
      log_##mode##_raw("!!! LOGBUF WRITE FAILURE !!!"); \
    else \
      log_##mode##_raw(__logbuf); \
      free(__logbuf); \
  } \
  else \
    log_##mode##_raw("!!! LOGBUF MALLOC FAILURE !!!"); \
}while(0)
#else
#define log_printf_raw(mode,...) \
do{ \
  char __logbuf[LOG_PRINTF_BUFSIZE+1]; \
  __logbuf[LOG_PRINTF_BUFSIZE] = '\0'; \
  snprintf(__logbuf, LOG_PRINTF_BUFSIZE, ##__VA_ARGS__); \
  if (__logbuf[0] == '\0') \
    log_##mode##_raw("!!! LOGBUF WRITE FAILURE !!!"); \
  else \
    log_##mode##_raw(__logbuf); \
}while(0)
#endif

#define log_printf(lvl,...) \
  log_printf_raw(msg,LOG_PREHEADER_TEXT(lvl) __VA_ARGS__)

//-------------------------------------------------------------

#ifndef LOG_LEVEL
#define LOG_LEVEL DBG
#endif

#ifdef LOG_HEADER
#define LOG_HEADER_TEXT(lvl) LOG_PREHEADER_TEXT(lvl) LOG_HEADER ": "
#define LOG_X(lvl,max,...) \
do{ \
  if (LOG_LVL(lvl) <= LOG_LVL(max)) \
    log_printf(lvl, LOG_HEADER ": " __VA_ARGS__); \
}while(0)
#else
#define LOG_HEADER_TEXT(lvl) LOG_PREHEADER_TEXT(lvl)
#define LOG_X(lvl,max,...) \
do{ \
  if (LOG_LVL(lvl) <= LOG_LVL(max)) \
    log_printf(lvl, ##__VA_ARGS__); \
}while(0)
#endif

#define LOG(lvl, ...) LOG_X(lvl,LOG_LEVEL, ##__VA_ARGS__)

//-------------------------------------------------------------

#ifndef LOG_OP_NONE_TEXT
#define LOG_OP_NONE_TEXT ""
#endif
#ifndef LOG_OP_START_TEXT
#define LOG_OP_START_TEXT "..."
#endif
#ifndef LOG_OP_ABORT_TEXT
#define LOG_OP_ABORT_TEXT " abort"
#endif
#ifndef LOG_OP_FAIL_TEXT
#define LOG_OP_FAIL_TEXT " failure"
#endif
#ifndef LOG_OP_OK_TEXT
#define LOG_OP_OK_TEXT " successfull"
#endif
#ifndef LOG_OP_FINISH_TEXT
#define LOG_OP_FINISH_TEXT " finished"
#endif

#define LOG_OP_STATE(state) LOG_OP_##state##_TEXT

#define LOG_OP(name,lvl,state) LOG(lvl, "%s" LOG_OP_STATE(state), name)
#define LOG_OP_PREMSG(name,lvl,state,format,...) LOG(lvl, "%s" format LOG_OP_STATE(state), name, ##__VA_ARGS__)
#define LOG_OP_MSG(name,lvl,state,format,...) LOG(lvl, "%s" LOG_OP_STATE(state) ": " format, name, ##__VA_ARGS__)

//-------------------------------------------------------------

#define LOG_FXN(lvl,state) LOG_OP(__func__,lvl,state)
#define LOG_FXN_PREMSG(lvl,state,format...) LOG_OP_PREMSG(__func__,lvl,state,format)
#define LOG_FXN_MSG(lvl,state,format...) LOG_OP_MSG(__func__,lvl,state,format)
#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_LOGS_H_ */
