/*
 * base.h
 *
 *  Created on: 1 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_BASE_H_
#define SYSTEM_BASE_H_

#include "config.h"

#include <sys/types.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include <assert.h>

#include <josapi.h>

#ifndef INLINE
#define INLINE static inline
#endif

#define NETWORK_SYS_TIMER 1
#define NETWORK_TIMER 2
#define SYSTICK_TIMER 3
#define USER_TIMER 4

#define TIMER_DO(timer, timeout_ms) \
  int __timer = timer; \
  uint16_t __100msCount = timeout_ms/100 + (timeout_ms%100?1:0); \
  Lib_SetTimer(__timer, __100msCount); \
  do {

#define TIMER_FROM_DO_RESET() \
  Lib_SetTimer(__timer, __100msCount)

#define TIMER_WHILE(v) \
    if (Lib_CheckTimer(__timer) == 0) \
      break; \
  } while(v)

#define MIN(x,y) (x <= y ? x : y)

#ifdef __cplusplus
extern "C"{
#endif


#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_BASE_H_ */
