#ifndef SYSTEM_FILE_H
#define SYSTEM_FILE_H

#include "base.h"

#ifdef __cplusplus
extern "C" {
#endif

int file_open(char * filename, BYTE mode);
int file_write(int fid, BYTE * dat, int len);
int file_read(int fid, BYTE * dat, int len);
int file_close(int fid);

bool file_exist(char * filename);
long file_size(char * filename);

bool file_remove(const char * filename);

#ifdef __cplusplus
}
#endif

#endif // SYSTEM_FILE_H
