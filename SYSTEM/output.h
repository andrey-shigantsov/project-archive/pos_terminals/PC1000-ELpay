/*
 * output.h
 *
 *  Created on: 25 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_OUTPUT_H_
#define SYSTEM_OUTPUT_H_

#include "base.h"
#include "logs.h"

#ifdef __cplusplus
extern "C"{
#endif

#define system_info(...) Lib_Lcdprintf(__VA_ARGS__)
#define system_info_f(fs,...) \
do{ \
  Lib_LcdSetFont(fs,16,0); \
  system_info(__VA_ARGS__); \
}while(0)

#define system_info_xyf(x,y,fs,...) \
do{ \
  Lib_LcdGotoxy(x,y); \
  system_info_f(fs, __VA_ARGS__); \
}while(0)
#define system_new_info(x,y,fs,...) \
do{ \
  Lib_LcdCls(); \
  system_info_xyf(x,y,fs,__VA_ARGS__); \
}while(0)

#define system_dbg(x,y,...) Lib_LcdPrintxy(x,y,0x80, __VA_ARGS__)

//-------------------------------------------------------------

extern BYTE system_isLogo;

#define SYS_INFO(header, operation) \
do { \
  if (system_isLogo) break; \
  system_new_info(8,4,16,"%s\n", header); \
  system_info_f(8,"\n   %s...", operation); \
} while (0)
#define SYS_INFO_RES(res) \
do { \
  if (system_isLogo) break; \
  if(res) \
    system_info("[ ERR ]\n"); \
  else \
    system_info("[ OK ]\n"); \
} while (0)

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_OUTPUT_H_ */
