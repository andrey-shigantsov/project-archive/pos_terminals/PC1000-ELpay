/*
 * File:   control.h
 */

#ifndef SYSTEM_CONTROL_H
#define SYSTEM_CONTROL_H

#include "base.h"
#include "output.h"

#ifdef __cplusplus
extern "C" {
#endif

#define APP_VER_STR(mj,mi) mj "." mi
#define DECLARE_APP_MSG(h,v) const APP_MSG App_Msg={h,v,0,0,0,"", "", "", 0, ""};

int init_system();
void init_logs(char * LogFileName);

int system_drawLogo(BYTE * logo);

void system_wait_keypress(BYTE key);

int system_exec();
void system_exit();

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_CONTROL_H */
