#define LOG_HEADER "GUI-SYS/tr"
#if 1
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#else
#define LOG_LEVEL DBG
#endif
#include "SYSTEM/logs.h"

#include "tr.h"
#include <stdint.h>

#define UNKNOWN_SYM 0x7fu

static inline bool utf8_is_ascii(const unsigned char * sym)
{
  return sym[0] < 0x80u;
}

static inline bool utf8_is_cyr(const unsigned char * sym)
{
  return (sym[0] >= 0xd0u)||(sym[0] <= 0xd3u);
}

static inline unsigned char tr_cyrsym(uint16_t sym)
{
  // LOG_FXN_MSG(DBG,START,"sym=0x%x", sym);

  if (sym == 0xd081u)
    sym = 0xd095u;
  else if (sym == 0xd191u)
    sym = 0xd0b5u;
  else if ( (sym < 0xd090u)||(sym > 0xd18fu) )
  {
    // LOG_FXN_MSG(DBG,FAIL,"unknown");
    return UNKNOWN_SYM;
  }

  char offset = 0;
  if (sym >= 0xd180u)
    offset = (sym - 0xd180u) + (0xd0bfu - 0xd090u + 1);
  else
    offset = sym - 0xd090u;
  unsigned char ch = 0xc0u + offset;
  // LOG_FXN_MSG(DBG,OK,"char=0x%x", ch);
  return ch;
}

int tr_cyr(const char * str_utf8, unsigned char * buf, size_t bufsize)
{
  size_t i = 0, j = 0;
  bool isEnd = false, isUnk = false;
  while (!isEnd)
  {
    if (j >= bufsize)
    {
      LOG_FXN_MSG(WRN,FAIL,"buffer overflow");
      return -1;
    }

    size_t offset = 1;
    const unsigned char * ptr = (const unsigned char *)&str_utf8[i];
    if (utf8_is_ascii(ptr))
    {
      isUnk = false;

      buf[j++] = ptr[0];

      if (ptr[0] == '\0') isEnd = true;
    }
    else if (utf8_is_cyr(ptr))
    {
      isUnk = false;

      uint16_t sym16 = ((ptr[0] << 8) | (ptr[1])) & 0xffff;
      offset = 2;
      switch (sym16)
      {
#if 1
      case 0xd18f:
        buf[j++] = '9';
        buf[j++] = 'I';
        LOG(WRN, "replace 'я' to '9I'");
        break;

      case 0xd097:
        buf[j++] = 'I';
        LOG(WRN, "replace 'Ї' to 'I'");
        break;
      case 0xd197:
        buf[j++] = 'i';
        LOG(WRN, "replace 'ї' to 'i'");
        break;

      case 0xe284:
        offset = 3;
        switch(ptr[2])
        {
        case 0x96:
          buf[j++] = 'N';
          buf[j++] = 'o';
          buf[j++] = '.';
          LOG(WRN, "replace '№' to 'No.'");
          break;
        default:
          isUnk = true;
          break;
        }
        break;
#endif
      default:
        buf[j++] = tr_cyrsym(sym16);
        break;
      }
    }
    else
    {
      if (!isUnk)
        isUnk = true;
    }
    if (isUnk)
    {
      buf[j++] = UNKNOWN_SYM;
      LOG(WRN, "Unknown sym: code 0x%02d%02d%02d%02d", ptr[0], ptr[1], ptr[2], ptr[3]);
    }
    i += offset;
  }
  return 0;
}
