/*
 * printer.h
 *
 *  Created on: 5 апр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_PRINTER_H_
#define SYSTEM_PRINTER_H_

#include <josapi.h>

#ifdef __cplusplus
extern "C"{
#endif

const char * printer_resStr();

#define printer_setFont(h) Lib_PrnSetFont(h,h,0x33)

#define PRINTER_RAW_ADDSTR(fmt...) \
do { \
  int __printer_res = Lib_PrnStr(fmt); \
  if (__printer_res) \
  { \
    LOG_OP_MSG("PRINTER: add str", WRN,FAIL, "%s", printer_resStr(__printer_res)); \
    break; \
  } \
}while(0)

#define PRINTER_RAW_PRINT \
do { \
  int __printer_res = Lib_PrnStart(); \
  if (__printer_res) \
  { \
    LOG_OP_MSG("PRINTER: print", WRN,FAIL, "start: %s", printer_resStr(__printer_res)); \
    break; \
  } \
  do \
  { \
    __printer_res = Lib_PrnCheck(); \
    Lib_DelayMs(500); \
  } while (__printer_res == PRN_BUSY); \
  if (__printer_res) \
  { \
    LOG_OP_MSG("PRINTER: print", WRN,FAIL, "printer check: %s", printer_resStr(__printer_res)); \
    break; \
  } \
} while(0)

#define printer_print(fmt...) \
do { \
  LOG_OP("PRINTER: print", INF,START); \
  PRINTER_RAW_ADDSTR(fmt); \
  PRINTER_RAW_PRINT; \
  LOG_OP("PRINTER: print",INF,OK); \
} while(0)

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_PRINTER_H_ */
