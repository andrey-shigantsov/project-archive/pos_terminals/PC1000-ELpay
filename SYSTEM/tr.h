#ifndef SYSTEM_GUI_TR
#define SYSTEM_GUI_TR

#ifdef __cplusplus
extern "C"{
#endif

#include <stddef.h>
#include <stdbool.h>

int tr_cyr(const char * str_utf8, unsigned char* buf, size_t bufsize);

#ifdef __cplusplus
}
#endif

#endif // SYSTEM_GUI_TR
