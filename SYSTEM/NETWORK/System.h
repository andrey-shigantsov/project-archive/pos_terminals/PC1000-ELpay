/* 
 * File:   System.h
 */

#ifndef NETWORK_SYSTEM_H
#define NETWORK_SYSTEM_H

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

int init_network_sys();
bool network_sys_isInit();

int network_sys_dial(uint16_t timeout_ms);
int network_sys_close();

int network_sys_resIsFatal(int res);

// возвращает true, если была реинициализация модуля (wasReinit)
bool network_sys_res_handler(int res);

#ifdef __cplusplus
}
#endif

#endif /* NETWORK_SYSTEM_H */
