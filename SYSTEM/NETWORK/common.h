/*
 * common.h
 *
 *  Created on: 20 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_NETWORK_COMMON_H_
#define SYSTEM_NETWORK_COMMON_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "../base.h"

typedef enum
{
  netSignal_No = 0x5,
  netSignal_VeryWeak = 0x4,
  netSignal_Weak = 0x3,
  netSignal_Normal = 0x2,
  netSignal_Strong = 0x1,
  netSignal_VeryStrong = 0x0
} NetworkSignalLevel_t;

const char * network_resStr(int res);
const char * network_SignalLevelStr(int lvl);

int network_setDialData(const char * num, const char * uid, const char * pwd);

int network_SignalLevel(int * lvl);
int network_CheckSim();

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_NETWORK_COMMON_H_ */
