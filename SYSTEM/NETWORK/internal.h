/*
 * internal.h
 *
 *  Created on: 11 мар. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_NETWORK_INTERNAL_H_
#define SYSTEM_NETWORK_INTERNAL_H_

#include "System.h"
#include "../logs.h"

#include <wlsapi.h>
#include <string.h>

#ifdef __cplusplus
extern "C"{
#endif

#define NETWORK_FAIL_MSG_FORMAT(res, header) header": %s", network_resStr(res)

#define NETWORK_CHECK_AND_RETURN(res, header) \
  if (res) \
  { \
    LOG_FXN_MSG(WRN, FAIL, NETWORK_FAIL_MSG_FORMAT(res, header)); \
    return res; \
  }

#define NETWORK_FXN(res, wasReinit, fxn, ...) \
Lib_SetTimer(NETWORK_SYS_TIMER, NETWORK_SYS_RESHANDLER_TIMEOUT/100); \
wasReinit = 0; \
do{ \
  res = fxn(__VA_ARGS__); \
  if (network_sys_resIsFatal(res)) \
  { \
    if (wasReinit) \
    { \
      LOG_FXN_MSG(WRN, NONE, "module fatal failure again, exit"); \
      break; \
    } \
    else \
      LOG_FXN_MSG(WRN, NONE, "module fatal failure"); \
  } \
  else \
  { \
    if (wasReinit) LOG_FXN(WRN, OK); \
    break; \
  } \
  if (Lib_CheckTimer(NETWORK_SYS_TIMER) == 0) \
  { \
    LOG_FXN_MSG(WRN, FAIL, "sys-timer timeout"); \
    res = WLS_TIMEOUT; \
    break; \
  } \
  wasReinit = network_sys_res_handler(res); \
  if (!wasReinit) \
  { \
    LOG_FXN(WRN, OK); \
    break; \
  } \
  LOG_FXN_MSG(WRN, NONE, "try again..."); \
} while(1);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_NETWORK_INTERNAL_H_ */
