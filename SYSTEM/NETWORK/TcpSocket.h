/* 
 * File:   TcpSocket.h
 */

#ifndef NETWORK_TCPSOCKET_H
#define NETWORK_TCPSOCKET_H

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  bool isSync, wasReinit;
  int num;

#define NETWORK_TCP_ADDR_BUFSIZE 256
  char addr[NETWORK_TCP_ADDR_BUFSIZE];
#define NETWORK_TCP_PORT_BUFSIZE (5+1)
  char port[NETWORK_TCP_PORT_BUFSIZE];
} TcpSocketData_t;

typedef int (*tcpsocket_connect_fxn)(TcpSocketData_t * This);
typedef int (*tcpsocket_close_fxn)(TcpSocketData_t * This);
typedef int (*tcpsocket_checkLink_fxn)(TcpSocketData_t * This);
typedef int (*tcpsocket_send_fxn)(TcpSocketData_t * This, char * data, ushort len);
typedef int (*tcpsocket_recv_fxn)(TcpSocketData_t * This, char * data, ushort * len, ushort timeout_ms);

typedef struct
{
  tcpsocket_connect_fxn connect;
  tcpsocket_close_fxn close;
  tcpsocket_checkLink_fxn checkLink;
  tcpsocket_send_fxn send;
  tcpsocket_recv_fxn recv;
} TcpSocketFxns_t;

typedef struct
{
  TcpSocketData_t data;
  const TcpSocketFxns_t * fxns;
} TcpSocket_t;

bool TcpSocket_isOpenned(TcpSocket_t * This);

int init_TcpSocket(TcpSocket_t * This, int num, char isSync);

int TcpSocket_open(TcpSocket_t * This, const char * addr, uint16_t port);
int TcpSocket_checkLink(TcpSocket_t * This);

int TcpSocket_send(TcpSocket_t * This, char * data, uint16_t len);
int TcpSocket_receive(TcpSocket_t * This, char * data, uint16_t * len, uint16_t timeout_ms);

int TcpSocket_close(TcpSocket_t * This);

#ifdef __cplusplus
}
#endif

#endif /* NETWORK_TCPSOCKET_H */
