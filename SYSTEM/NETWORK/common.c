/*
 * common.c
 *
 *  Created on: 11 мар. 2017 г.
 *      Author: Svetozar
 */

#define LOG_HEADER "NETWORK"
#include "internal.h"

const char * network_resStr(int res)
{
  switch(res)
  {
  case WLS_OK:
    return "success";
  case WLS_ERR:
    return "operate err";
  case WLS_NORSP:
    return "no resp";
  case WLS_RSPERR:
    return "module rsp err";
  case WLS_NOSIM:
    return "no SIM card";
  case WLS_NEEDPIN:
    return "need SIM PIN";
  case WLS_NEEDPUK:
    return "need SIM PUK";
  case WLS_SIMBLOCKED:
    return "SIM Card locked";
  case WLS_SIMERR:
    return "no SIM or PIN";
  case WLS_PINERR:
    return "PIN err";
  case WLS_PARAMERR:
    return "input para err";
  case WLS_USERORPWDERR:
    return "UserName&Pwd err";
  case WLS_SNLTOOWEAK:
    return "signal too weak";
  case WLS_LINKCLOSED:
    return "module offline";
  case WLS_LINKOPENED:
    return "module  online";
  case WLS_LINKOPENING:
    return "logoning net";
  case WLS_TCPCLOSED:
    return "TCP closed";
  case WLS_TCPOPENED:
    return "TCP opened";
  case WLS_TCPOPENING:
    return "TCP openning";
  case WLS_ATTACHED:
    return "module attached";
  case WLS_DETTACHED:
    return "module dettached";
  case WLS_UNKNOWNTYPE:
    return "type unknown";
  case WLS_DIALERR:
    return "attached refused";
  case WLS_TCPSENDERR:
    return "TCP send fail ";
  case WLS_TCPCONNECTOVERTIME:
    return "TCP con timeout";
  case WLS_PORTERR:
    return "COM port err";
  case WLS_PORTINUSE:
    return "COM port inuse";
  case WLS_DETECTERR:
    return "detect module err";
  case WLS_SOCKETOVERRUN:
    return "TCP socKetNo over";
  case WLS_TIMEOUT:
    return "timeout";
  case WLS_OTHERR:
    return "other err";
  case WLS_NOTSUPPORT:
    return "dont support";
  case WLS_NoUNReadMessage:
    return "no MSG inside";

  default:
    return "unkown err";
  }
}

const char * network_SignalLevelStr(int lvl)
{
  switch(lvl)
  {
  default:
    return "invalid";
  case netSignal_No:
    return "no";
  case netSignal_VeryWeak:
    return "very weak";
  case netSignal_Weak:
    return "weak";
  case netSignal_Normal:
    return "normal";
  case netSignal_Strong:
    return "strong";
  case netSignal_VeryStrong:
    return "very strong";
  }
}

int network_SignalLevel(int * lvl)
{
  int res;
  bool wasReinit;
  NETWORK_FXN(res, wasReinit, Wls_CheckSignal, lvl)
  NETWORK_CHECK_AND_RETURN(res, "check signal")
  return 0;
}

int network_CheckSim()
{
  int res;
  bool wasReinit;
  NETWORK_FXN(res, wasReinit, Wls_CheckSim)
  NETWORK_CHECK_AND_RETURN(res, "check sim")
  return 0;
}
