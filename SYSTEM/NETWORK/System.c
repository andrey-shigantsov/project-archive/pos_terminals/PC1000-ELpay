/* 
 * File:   System.c
 */

#define LOG_HEADER "NETWORK"
#include "internal.h"

static uchar dialNum[32], UId[32], Pwd[32];

int network_sys_resIsFatal(int res)
{
  switch (res)
  {
  case WLS_RSPERR:
  case WLS_NORSP:
    return 1;

  default:
    return 0;
  }
}

bool network_sys_res_handler(int res)
{
  int res2;
  if (network_sys_resIsFatal(res))
  {
    LOG_FXN_MSG(WRN, NONE, "try reinit...");

//    res2 = network_sys_close();
//    if (res2)
      res2 = init_network_sys();
    if (!res2)
      res2 = network_sys_dial(NETWORK_DIAL_TIMEOUT);
    if (res2)
      LOG_FXN_MSG(WRN, NONE, "reinit failure");
    else
      LOG_FXN_MSG(WRN, NONE, "reinit successfull");
    return 1;
  }
  return 0;
}

static bool isInit = false;
int init_network_sys()
{
  LOG_FXN(INF,START);

  int res;

  res = Wls_Reset();
  NETWORK_CHECK_AND_RETURN(res, "reset");
  Lib_DelayMs(500);

  res = Wls_Init();
  NETWORK_CHECK_AND_RETURN(res, "init");
  Lib_DelayMs(500);

  isInit = true;
  LOG_FXN(INF,OK);
  return 0;
}

bool network_sys_isInit()
{
  return isInit;
}

int network_setDialData(const char * num, const char * uid, const char * pwd)
{
  LOG_FXN(INF,START);

  strncpy(dialNum, num, sizeof(dialNum));
  strncpy(UId, uid, sizeof(UId));
  strncpy(Pwd, pwd, sizeof(Pwd));

  LOG_FXN(INF,OK);
  return 0;
}

int network_sys_dial(uint16_t timeout_ms)
{
  LOG_FXN(INF,START);

  int res;

  res = Wls_CheckDial();
  if (res)
  {
    res = Wls_InputUidPwd(UId, Pwd);
    NETWORK_CHECK_AND_RETURN(res, "input uid&pwd");

    TIMER_DO(NETWORK_TIMER, timeout_ms)
    {
      LOG_FXN_MSG(INF, NONE, "try...");
      res = Wls_Dial(dialNum);
      if (res)
      {
        LOG_FXN_MSG(INF, NONE, "%s", network_resStr(res));
        network_sys_close();
      }
    } TIMER_WHILE(res);
  }
  else
    LOG_FXN_MSG(INF, NONE, "dial already established");
  if (res)
    LOG_FXN(INF,FAIL);
  else
    LOG_FXN(INF,OK);
  return res;
}

int network_sys_close()
{
  LOG_FXN(INF, START);
  int res = Wls_NetClose();
  NETWORK_CHECK_AND_RETURN(res, "close");
  LOG_FXN(INF, OK);
  return 0;
}
