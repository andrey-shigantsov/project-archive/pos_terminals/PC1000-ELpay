/*
 * File:   TcpSocket.c
 */

#define LOG_HEADER "NETWORK: TCP"
#define LOG_LEVEL WRN
#include "TcpSocket.h"
#include "internal.h"

#include <string.h>

#define TCP_FXN(fxn...) \
  LOG_FXN(DBG,NONE); \
  int res; \
  NETWORK_FXN(res, This->wasReinit, fxn) \
  return res;

static int connect_SS(TcpSocketData_t * This)
{
  (void)This;
  TCP_FXN(Wls_TcpConnect, This->addr, This->port)
}

static int connect_SA(TcpSocketData_t * This)
{
  (void)This;
  TCP_FXN(Wls_TcpPreConnect, This->addr, This->port)
}

static int connect_US(TcpSocketData_t * This)
{
  TCP_FXN(Wls_MTcpConnect, This->num, This->addr, This->port)
}

static int connect_UA(TcpSocketData_t * This)
{
  TCP_FXN(Wls_MTcpPreConnect, This->num, This->addr, This->port)
}

static int close_SX(TcpSocketData_t * This)
{
  (void)This;
  TCP_FXN(Wls_TcpClose)
}

static int close_UX(TcpSocketData_t * This)
{
  TCP_FXN(Wls_MTcpClose, This->num)
}

static int checkLink_SX(TcpSocketData_t * This)
{
  (void)This;
  TCP_FXN(Wls_CheckTcpLink)
}

static int checkLink_UX(TcpSocketData_t * This)
{
  TCP_FXN(Wls_CheckMTcpLink, This->num)
}

static int send_SX(TcpSocketData_t * This, char * data, ushort len)
{
  (void)This;
  TCP_FXN(Wls_TcpSend, data, len)
}

static int send_UX(TcpSocketData_t * This, char * data, ushort len)
{
  TCP_FXN(Wls_MTcpSend, This->num, data, len)
}

static int recv_SX(TcpSocketData_t * This, char * data, ushort * len, ushort timeout_ms)
{
  (void)This;
  TCP_FXN(Wls_TcpRecv, data, len, timeout_ms)
}

static int recv_UX(TcpSocketData_t * This, char * data, ushort * len, ushort timeout_ms)
{
  (void)This;
  TCP_FXN(Wls_MTcpRecv, This->num, data, len, timeout_ms)
}

//-------------------------------------------------------------

const TcpSocketFxns_t
fxns_SS = {connect_SS, close_SX, checkLink_SX, send_SX, recv_SX},
fxns_SA = {connect_SA, close_SX, checkLink_SX, send_SX, recv_SX},
fxns_US = {connect_US, close_UX, checkLink_UX, send_UX, recv_UX},
fxns_UA = {connect_UA, close_UX, checkLink_UX, send_UX, recv_UX};

int init_TcpSocket(TcpSocket_t * This, int num, char isSync)
{
  This->data.num = num;
  This->data.isSync = isSync;

  if (isSync)
  {
    if (num == 1)
      This->fxns = &fxns_SS;
    else
      This->fxns = &fxns_US;
  }
  else
  {
    if (num == 1)
      This->fxns = &fxns_SA;
    else
      This->fxns = &fxns_UA;
  }
  return 0;
}

int TcpSocket_open(TcpSocket_t * This, const char * addr, uint16_t port)
{
  LOG_FXN(INF,START);

  int state = TcpSocket_checkLink(This);

  switch (state)
  {
  case WLS_OK:
  case WLS_TCPOPENED:
  case WLS_TCPOPENING:
    LOG_FXN_MSG(WRN, NONE, "%s", "state = %s", network_resStr(state));
    TcpSocket_close(This);
    break;
  }

  strncpy(This->data.addr, addr, NETWORK_TCP_ADDR_BUFSIZE);
  snprintf(This->data.port, NETWORK_TCP_PORT_BUFSIZE, "%d", port);

  LOG_FXN_MSG(INF, NONE, "%s:%s", This->data.addr, This->data.port);

  int res;

  res = This->fxns->connect(&This->data);

  if (res)
    LOG_FXN_MSG(WRN, FAIL, "%s", network_resStr(res));
  else
    LOG_FXN(INF,OK);
  return res;
}

bool TcpSocket_isOpenned(TcpSocket_t * This)
{
  int state = TcpSocket_checkLink(This);
  LOG_FXN_MSG(DBG,NONE, "state == %s", network_resStr(state));
  return state == WLS_OK;
}

int TcpSocket_checkLink(TcpSocket_t * This)
{
  return This->fxns->checkLink(&This->data);
}

int TcpSocket_send(TcpSocket_t * This, char * data, uint16_t len)
{
  LOG_FXN(INF,START);

  int res = This->fxns->send(&This->data, data, len);

  if (res)
    LOG_FXN_MSG(WRN, FAIL, "%s", network_resStr(res));
  else
    LOG_FXN(INF,OK);
  return res;
}

int TcpSocket_receive(TcpSocket_t * This, char * data, uint16_t * len, uint16_t timeout_ms)
{
  LOG_FXN(INF,START);

  int res = This->fxns->recv(&This->data, data, len, timeout_ms);

  if (res)
    LOG_FXN_MSG(WRN, FAIL, "%s", network_resStr(res));
  else if (*len == 0)
    LOG_FXN_MSG(WRN, FAIL, "no data");
  else
    LOG_FXN(INF,OK);
  return res;
}

int TcpSocket_close(TcpSocket_t * This)
{
  LOG_FXN(INF,START);

  if (!TcpSocket_isOpenned(This))
  {
    LOG_FXN_MSG(WRN, OK, "%s", "already closed");
    return 0;
  }

  int res = This->fxns->close(&This->data);

  if (res)
    LOG_FXN_MSG(WRN, FAIL, "%s", network_resStr(res));
  else
    LOG_FXN(INF,OK);
  return res;
}
