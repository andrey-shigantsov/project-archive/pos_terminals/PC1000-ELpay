/*
 * File:   Label.h
 */

#ifndef GUI_LABEL_H
#define GUI_LABEL_H

#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GUI_LABEL_BUFSIZE 255u

typedef struct
{
  Object_t obj;
  Alignment_t alignText;
  char text[GUI_LABEL_BUFSIZE];
  char curTextPart[GUI_LABEL_BUFSIZE];
  int curTextPartPos;
} Label_t;

void init_Label(Label_t * This);

INLINE void Label_setTextAlignment(Label_t * This, Alignment_t align)
{
  This->alignText = align;
}

INLINE char * Label_Text(Label_t * This)
{
  return This->text;
}
void Label_setText(Label_t * This, const char * text);
void Label_setTextCyr(Label_t * This, const char * text);

#ifdef __cplusplus
}
#endif

#endif /* GUI_LABEL_H */
