/*
 * File:   Menu.c
 */

#include "Menu.h"
#define LOG_HEADER "GUI-SYS/Menu"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

#include "SYSTEM/base.h"

#include <string.h>
#include <assert.h>

static void draw(Menu_t * This)
{
  Object_clearDrawBox(&This->obj);
  if (This->isHeader)
    object_draw(&This->Header.obj);
  if (!This->Items) return;
  if (!This->ItemsCount) return;
  object_draw(&This->layout.obj);
}

static inline int max_item_idx(Menu_t * This)
{
  if (This->ItemsCount >= This->pageItemsCount)
    return This->ItemsCount - This->pageItemsCount;
  else
    return 0;
}

static void refresh_currentItemIdx(Menu_t * This)
{
  if (This->currentItemIdx < 0)
    This->currentItemIdx = 0;
  else
  {
    int maxIdx = max_item_idx(This);
    if (This->currentItemIdx > maxIdx)
      This->currentItemIdx = maxIdx;
    assert(This->currentItemIdx >= 0);
  }

  assert(This->rowsCount <= GUI_MENU_OBJBUF_SIZE);
  assert(This->colsCount <= GUI_MENU_OBJS_COLCOUNT);

  int i,j;
  for (i = 0; i < This->rowsCount; ++i)
  {
    This->ObjBuf[i] = This->Objs[i];
    for (j = 0; j < This->colsCount; ++j)
    {
      int I = This->rowsCount*j+i;
      I += This->currentItemIdx;
      if (I < This->ItemsCount)
        This->Objs[i][j] = &This->Items[I].obj;
      else
        This->Objs[i][j] = 0;
    }
  }
}

static void refresh_items_keys(Menu_t * This)
{
  if (!This->ItemsCount) return;
  for (int i = 0; i < This->pageItemsCount; ++i)
  {
    int j = This->currentItemIdx+i;
    if (j >= This->ItemsCount) break;
    snprintf(Label_Text(&This->Items[j].Key), 2, "%d", i+1);
  }
}

static void refresh_items_font(Menu_t * This)
{
  for (int i = 0; i < This->ItemsCount; ++i)
  {
    This->Items[i].obj.font.size = This->obj.font.size;
    This->Items[i].Key.obj.font.size = This->obj.font.size;
    This->Items[i].Label.obj.font.size = This->obj.font.size;
  }
}

static void refresh_layout(Menu_t * This)
{
  switch (This->layoutSize)
  {
  default:
  case menuSmall:
    This->obj.font.size = 8;
    This->rowsCount = GUI_MENU_SMALL_ROWCOUNT + (This->isHeader ? 0 : 1);
    This->colsCount = GUI_MENU_SMALL_COLCOUNT;
    break;

  case menuNormal:
    This->obj.font.size = 8;
    This->rowsCount = GUI_MENU_NORM_ROWCOUNT + (This->isHeader ? 0 : 1);
    This->colsCount = GUI_MENU_NORM_COLCOUNT;
    break;
  }
  This->pageItemsCount = This->rowsCount * This->colsCount;

  refresh_currentItemIdx(This);
  refresh_items_font(This);
  refresh_items_keys(This);

  TableLayout_setCounts(&This->layout, This->rowsCount, This->colsCount);

  This->obj.isNeedResize = true;
}

static void key_handler(Menu_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {
  case KEYOK:
    ++This->layoutSize;
    This->layoutSize %= GUI_MENU_LAYOUT_SIZES_COUNT;
    refresh_layout(This);
    object_draw(&This->obj);
    break;

  case KEYUP:
    This->currentItemIdx -= This->pageItemsCount;
    refresh_currentItemIdx(This);
    refresh_items_keys(This);
    tablelayout_exec(&This->layout);
    object_draw(&This->obj);
    break;

  case KEYDOWN:
    This->currentItemIdx += This->pageItemsCount;
    refresh_currentItemIdx(This);
    refresh_items_keys(This);
    tablelayout_exec(&This->layout);
    object_draw(&This->obj);
    break;

  default:
    if (KEY_IS_NUM(key) && key != '0')
    {
      int offset = (key & 0x0f) - 1;
      assert((offset >= 0)&&(offset <= 8));
      if (offset >= MIN(This->pageItemsCount,This->ItemsCount))
      {
        LOG_FXN_MSG(DBG,NONE, "call item action dropped");
        break;
      }

      if (This->item_action)
      {
        LOG_FXN_MSG(DBG,NONE, "call item action: current item idx = %d, offset = %d", This->currentItemIdx, offset);
        This->item_action(This->obj.Parent, This->currentItemIdx + offset);
      }
    }
    break;
  }
}

void Menu_enableHeader(Menu_t * This, bool flag)
{
  This->isHeader = flag;
  refresh_layout(This);
}

void Menu_setItems(Menu_t * This, MenuItem_t * Items, int Count)
{
  This->Items = Items;
  This->ItemsCount = Count;
  This->currentItemIdx = 0;

  refresh_currentItemIdx(This);
  refresh_items_keys(This);
  refresh_items_font(This);

  This->obj.isNeedResize = true;
}

void Menu_setLayoutSize(Menu_t * This, MenuLayoutSize_t size)
{
  This->layoutSize = size;
  refresh_layout(This);
}

static void resize(Menu_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  if (This->isHeader)
  {
    Object_setMargins(&This->layout.obj, 1,0,0,0);

    Object_setMargins(&This->Header.obj, 1,0,0,1);
    BYTE wH = This->Header.obj.font.size+This->Header.obj.margins.top+This->Header.obj.margins.bottom;
    Object_setGeometry(&This->Header.obj, g.x,g.y,g.w, wH);
    g.y += wH;
    g.h -= wH;
  }
  else
  {
    TableLayout_setRowsSpacing(&This->layout, 0);
    Object_setMargins(&This->layout.obj, 1,0,0,1);

    BYTE wH = This->layout.obj.margins.top+This->layout.obj.margins.bottom;
    g.y += wH;
    g.h -= wH;
  }
  Object_setGeometry(&This->layout.obj, g.x,g.y,g.w,g.h);
}

static void current_handler(Menu_t * This)
{
  setKeysMode(keysMode_Int);

  This->currentItemIdx = 0;
  refresh_currentItemIdx(This);
  refresh_items_keys(This);
  tablelayout_exec(&This->layout);
}

void init_Menu(Menu_t * This)
{
  init_Object(&This->obj, This, "Menu");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.current_handler = (GUI_fxn_t)current_handler;

  This->isHeader = false;
  init_Label(&This->Header);
  This->Header.obj.font.isInverseColor = 1;
  This->Header.obj.font.size = 8;
  Object_setMargins(&This->Header.obj, 1,0,0,1);

  This->Items = 0;
  This->ItemsCount = 0;
  This->currentItemIdx = 0;

  init_TableLayout(&This->layout, 0, 0,
    This->ObjBuf, 0, 0);

  Menu_setLayoutSize(This, menuNormal);

  for (int i = 0; i < This->rowsCount; ++i)
    This->ObjBuf[i] = This->Objs[i];
}

static void mi_draw(MenuItem_t * This)
{
  object_draw(&This->layout.obj);
}

static void mi_resize(MenuItem_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  Object_setGeometry(&This->layout.obj, g.x,g.y,g.w,g.h);
}

void init_MenuItem(MenuItem_t * This, Menu_t * Parent)
{
  init_Object(&This->obj, This, "MenuItem");
  This->obj.draw = (GUI_fxn_t)mi_draw;
  This->obj.resize = (GUI_fxn_t)mi_resize;

  if (Parent)
  {
    Object_setParent(&This->obj, &Parent->obj);
    This->obj.font.size =
    This->Key.obj.font.size =
    This->Label.obj.font.size = Parent->obj.font.size;
  }

  init_Label(&This->Key);
  This->Key.obj.font.isInverseColor = 1;
  Object_setMargins(&This->Key.obj, 1,1,1,2);

  init_Label(&This->Label);

  This->Objs[0] = &This->Key.obj;
  This->Objs[1] = &This->Label.obj;

  This->ObjsAlign[0] = alHLeft | alVCenter;
  This->ObjsAlign[1] = alHLeft | alVCenter;

  This->ObjsStretch[0] = 1;
  This->ObjsStretch[1] = 16;

  Object_setSizePolicy(This->Objs[0], szMin, szMin);
  Object_setSizePolicy(This->Objs[1], szExpand, szMin);

  init_ListLayout(&This->layout, orHorizontal, 1, This->Objs, GUI_MENU_ITEM_OBJSCOUNT);
  ListLayout_setAlignment(&This->layout, This->ObjsAlign);
  ListLayout_setStretch(&This->layout, This->ObjsStretch);
}

void MenuItems_FromActions(MenuItem_t * Items, Menu_t * Parent, Action_t * Actions, int Count)
{
  for (int i = 0; i < Count; ++i)
  {
    MenuItem_t * item = &Items[i];
    init_MenuItem(item, Parent);
    Label_setTextCyr(&item->Label, Actions[i].name);
  }
}
