/* 
 * File:   LineEdit.c
 */

#include "LineEdit.h"
#define LOG_HEADER "GUI-SYS/LineEdit"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/DRIVERS/keyboard.h"

#include <string.h>

static void draw(LineEdit_t * This)
{
  Object_clearDrawBox(&This->obj);

  Box_t b = Object_Box(&This->obj);
  
  if (This->isSelected)
    Lib_LcdDrawBox(b.x0,b.y0,b.x,b.y);
  Lib_LcdDrawBox(b.x0+1,b.y0+1,b.x-1,b.y-1);
  
  b = Object_contentBoxAuto(&This->obj, alVCenter);

    BYTE len = (This->textLen <= This->maxDispLen)?
      This->textLen : This->maxDispLen;  
  if (This->textLen)
  {
    Lib_LcdGotoxy(b.x0,b.y0);
    Lib_LcdSetFont(This->obj.font.size,16,0);
    if (This->isPassword)
    {
      BYTE i;
      for (i = 0; i < len-1; ++i)
        Lib_Lcdprintf("*");
      if (!This->isLastCharChanging)
        Lib_Lcdprintf("%c", (This->pwdLastIsHide)?
          '*' : This->text[This->textLen-1]);
    }
    else
    {
      if (This->isLastCharChanging)
      {
        BYTE i;
        for (i = This->textLen-len; i < This->textLen-1; ++i)
          Lib_Lcdprintf("%c", This->text[i]);
      }
      else
        Lib_LcdPrintxy(b.x0,b.y0, font_attr(&This->obj.font),
          &This->text[This->textLen - len]);
    }
    if (This->isLastCharChanging)
    {
      BYTE older = Lib_LcdSetAttr(1);
      Lib_Lcdprintf("%c", This->text[This->textLen-1]);
      Lib_LcdSetAttr(older);
    }
  }
  
  Box_t b1 = b;
  BYTE w = font_width(&This->obj.font);
  if (!This->isLastCharChanging)
  {
    b1.x0 += len*w;
    b1.y0 -= 1;
    b1.y = b1.y0 + This->obj.font.size + 1;
    Lib_LcdDrawLine(b1.x0, b1.y0, b1.x0,b1.y, This->cursorIsVisible);
  }
  else
  {
    b1.x0 += (len-1)*w;
    b1.y = b1.y0 + This->obj.font.size - 1;
    Lib_LcdDrawLine(b1.x0, b1.y0, b1.x0,b1.y, 1);
    Lib_LcdDrawLine(b1.x0, b1.y, b1.x0+font_width(&This->obj.font)-1,b1.y, 1);
  }
}

static void tick_handler(LineEdit_t * This)
{
  if (This->isSelected)
  {
    ++This->curTickCounter;

    int count;

    if (This->cursorIsVisible)
      count = This->curVisTickCount;
    else
      count = This->curUnvisTickCount;
    if (This->curTickCounter >= count)
    {
      This->cursorIsVisible = !This->cursorIsVisible;
      This->curTickCounter = 0;
      object_draw(&This->obj);
    }
  }
  
  if (This->isPassword && (!This->pwdLastIsHide))
  {
    ++This->pwdTickCounter;
    if (This->pwdTickCounter >= This->pwdHideLastTickCount)
    {
      This->pwdLastIsHide = 1;
      This->pwdTickCounter = 0;
      object_draw(&This->obj);
    }
  }
}

static void text_append(LineEdit_t * This, const char c)
{
  This->isLastCharAppend = 0;
  if (!This->text) return;
  if (!This->textBufSize) return;
  if (This->textLen + 1 >= This->textBufSize) return;
  
  This->text[This->textLen++] = c;
  This->text[This->textLen] = '\0';

  if (This->isPassword)
    This->pwdLastIsHide = 0;
  This->isLastCharChanging = 0;
  This->isLastCharAppend = 1;
}

static void text_set_last(LineEdit_t * This, const char c)
{
  if (!This->text) return;
  if (!This->textBufSize) return;
  if (!This->textLen) return;
  if (!This->isLastCharAppend) return;
  
  This->text[This->textLen-1] = c;

  if (This->isPassword)
    This->pwdLastIsHide = 0;
  
  This->isLastCharChanging = 1;
}

static void text_append_str(LineEdit_t * This, const char * s)
{
  if (!This->text) return;
  if (!This->textBufSize) return;
  
  int slen = strlen(s);
  if (This->textLen + slen >= This->textBufSize) return;
  
  strncpy(&This->text[This->textLen], s, slen);
  This->textLen += slen;
  This->text[This->textLen] = '\0';
  
  if (This->isPassword)
    This->pwdLastIsHide = 0;
  This->isLastCharChanging = 0;
}

static void text_remove_last(LineEdit_t * This)
{
  if (!This->text) return;
  if (!This->textBufSize) return;
  if (This->textLen <= 0) return;
  
  --This->textLen;
  This->text[This->textLen] = '\0';
  
  if (This->isPassword)
    This->pwdLastIsHide = 1;
  This->isLastCharChanging = 0;
}

static void key_handler(LineEdit_t * This, BYTE key, BYTE isNew)
{
  switch (key)
  {    
  case KEYCLEAR:
    text_remove_last(This);
    object_draw(&This->obj);
    break;
    
  case KEY00:
    text_append_str(This, "00");
    object_draw(&This->obj);
    break;
    
  default:
    if (!KEY_IS_TEXT(key)) break;
    if (isNew)
      text_append(This, key);
    else
      text_set_last(This, key);
    object_draw(&This->obj);
    break;
  }
}

static void key_edit_finished(LineEdit_t * This)
{
  This->isLastCharChanging = 0;
  object_draw(&This->obj);
}

static void resize(LineEdit_t * This)
{
  Geometry_t g = Object_contentGeometry(&This->obj);
  This->maxDispLen = g.w / font_width(&This->obj.font);
}

static BYTE content_minH(LineEdit_t * This)
{
  return This->obj.font.size;
}

static BYTE content_minW(LineEdit_t * This)
{
  return font_width(&This->obj.font)*MIN(This->maxDispLen,This->textBufSize);
}

void init_LineEdit(LineEdit_t * This)
{
  init_Object(&This->obj, This, "LineEdit");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;
  This->obj.key_handler = (GUI_key_handler_fxn_t)key_handler;
  This->obj.key_edit_finished = (GUI_fxn_t)key_edit_finished;
  This->obj.contentMinFxn.h = (GUI_minmax_fxn_t)content_minH;
  This->obj.contentMinFxn.w = (GUI_minmax_fxn_t)content_minW;
  
  Object_setInternalMargins(&This->obj, 1,1,1,1);
  Object_setMargins(&This->obj, 3,3,3,3);
  LineEdit_setTextBuf(This, 0,0);
  
  This->maxDispLen = 8;
  
  This->isSelected = 0;
  This->cursorIsVisible = 0;
  This->isPassword = 0;
  This->pwdLastIsHide = 0;
  This->isLastCharChanging = 0;
  
  This->curTickCounter = 0;
  This->curVisTickCount = 5;
  This->curUnvisTickCount = 2;
  This->pwdHideLastTickCount = 5;
}

void LineEdit_setTextBuf(LineEdit_t * This, char * buf, BYTE size)
{
  LineEdit_setTextBufWithLen(This, buf, size, 0);
}

void LineEdit_setTextBufWithLen(LineEdit_t * This, char * buf, BYTE size, BYTE len)
{
  This->text = buf;
  This->textBufSize = size;
  This->textLen = len;
  if (This->text)
    This->text[This->textLen] = '\0';
}

void LineEdit_setText(LineEdit_t * This, const char * text)
{
  strncpy(This->text, text, This->textBufSize);
  This->text[This->textBufSize-1] = '\0';
  This->textLen = strlen(This->text);
}

const char * LineEdit_Text(LineEdit_t * This)
{
  return This->text;
}

void LineEdit_setSelection(LineEdit_t * This, BYTE flag)
{
  if (This->isPassword)
    This->pwdLastIsHide = 1;
  This->cursorIsVisible = flag;
  This->isSelected = flag;
}

void LineEdit_enablePasswordMode(LineEdit_t * This)
{
  This->isPassword = 1;
}

void LineEdit_passwordHideLast(LineEdit_t * This)
{
  This->pwdLastIsHide = 1;
}
