/*
 * File:   Label.c
 */

#include "Label.h"
#define LOG_HEADER "GUI-SYS/Label"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

#include <string.h>

static void refresh_curtext(Label_t * This)
{
  int maxsize = Object_maxContentStringLen(&This->obj);
  assert(This->curTextPartPos >= 0);
  assert(This->curTextPartPos < GUI_LABEL_BUFSIZE-1);
  char * ptr = This->text + This->curTextPartPos;
  int size = strlen(ptr);
  if (size > maxsize)
    size = maxsize;
  assert(size < GUI_LABEL_BUFSIZE);
  strncpy(This->curTextPart, ptr, size);
  This->curTextPart[size] = '\0';
}

static void draw(Label_t * This)
{
  Font_t * f = &This->obj.font;
  Object_fillDrawBox(&This->obj, f->isInverseColor);

  Box_t b = Object_contentBoxAuto(&This->obj, This->alignText);
  Lib_LcdPrintxy(b.x0,b.y0, font_attr(&This->obj.font), This->curTextPart);
}

static BYTE content_minH(Label_t * This)
{
  return This->obj.font.size;
}

static BYTE content_minW(Label_t * This)
{
  size_t len = strlen(This->text);
  return len*font_width(&This->obj.font);
}

static void tick_handler(Label_t * This)
{
}

static void resize(Label_t * This)
{
  refresh_curtext(This);
}

void init_Label(Label_t * This)
{
  init_Object(&This->obj, This, "Label");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.contentMinFxn.h = (GUI_minmax_fxn_t)content_minH;
  This->obj.contentMinFxn.w = (GUI_minmax_fxn_t)content_minW;
  This->obj.tick_handler = (GUI_fxn_t)tick_handler;

  This->alignText = alVCenter;
  This->text[0] = '\0';
  This->curTextPartPos = 0;
}

void Label_setText(Label_t * This, const char * text)
{
  strncpy(This->text, text, GUI_LABEL_BUFSIZE);
  This->curTextPartPos = 0;
  refresh_curtext(This);
}

void Label_setTextCyr(Label_t* This, const char* text)
{
  tr_cyr(text,This->text,GUI_LABEL_BUFSIZE);
  This->curTextPartPos = 0;
  refresh_curtext(This);
}
