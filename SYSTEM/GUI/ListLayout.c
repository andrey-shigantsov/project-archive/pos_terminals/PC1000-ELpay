/* 
 * File:   ListLayout.c
 */

#include "ListLayout.h"
#define LOG_HEADER "GUI-SYS/ListLayout"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

static void draw(ListLayout_t * This)
{
	BYTE i;
  for (i = 0; i < This->ChildrensCount; ++i)
    object_draw(This->Childrens[i]);
}

static void resize(ListLayout_t * This)
{
  AbstractLayout_setSegmentFromObj(&This->base, This->base.orientation, &This->obj);
  listlayout_exec(This);

  BYTE i;
  for (i = 0; i < This->ChildrensCount; ++i)
    object_resize(This->Childrens[i]);
}

void init_ListLayout(ListLayout_t * This,
  Orientation_t orientation, BYTE spacing,
  Object_t ** Childrens, BYTE ChildrensCount)
{
  init_Object(&This->obj, This, "ListLayout");
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.resize = (GUI_fxn_t)resize;
  
  init_AbstractLayout(&This->base, Childrens, ChildrensCount, orientation, 0, 0, spacing);
  
  This->Childrens = Childrens;
  This->ChildrensAlign = 0;
  This->ChildrensCount = ChildrensCount;
}

void listlayout_exec(ListLayout_t * This)
{
  float W = AbstractLayout_EqualSegmentLen(&This->base);
  Geometry_t g0 = Object_contentGeometry(&This->obj);
  BYTE Pos0, Pos, W0;
  switch (This->base.orientation)
  {
  case orHorizontal:
    W0 = g0.h;
    Pos0 = g0.y;
    Pos = g0.x;
    break;
    
  case orVertical:
    W0 = g0.w;
    Pos0 = g0.x;
    Pos = g0.y;
    break;
  }
  
  BYTE i;
  for (i = 0; i < This->ChildrensCount; ++i)
  {
    if (!This->Childrens[i]) continue;
    if (!Object_isVisiable(This->Childrens[i])) continue;

    Object_t * obj = This->Childrens[i];
    Geometry_t * g = &(obj->geometry);
    Alignment_t al = (This->ChildrensAlign ? This->ChildrensAlign[i] : alHCenter|alVCenter);
    SizePolicy_t sp;
    GUI_minmax_fxn_t min_fxn;
    BYTE * pos0, * pos, * w0, * w;
    switch (This->base.orientation)
    {
    case orHorizontal:
      pos0 = &g->y;
      w0 = &g->h;
      pos = &g->x;
      w = &g->w;
      sp = obj->SizePolicyH;
      break;

    case orVertical:
      pos0 = &g->x;
      w0 = &g->w;
      pos = &g->y;
      w = &g->h;
      sp = obj->SizePolicyV;
      break;
    }

    float Ws;
    switch (sp)
    {
    case szMin:
      Ws = This->base.min_fxn(obj);
      break;
    default:
      Ws = W;
      if (This->base.Stretchs)
        Ws *= This->base.Stretchs[i];
      break;
    }

    *pos0 = Pos0;
    *w0 = W0;
    *pos = Pos;
    *w = Ws;
    Object_align(obj, al);
    object_resize(obj);

    Pos += Ws + This->base.spacing;
  }
}
