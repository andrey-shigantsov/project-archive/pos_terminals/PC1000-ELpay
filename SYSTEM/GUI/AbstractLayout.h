/* 
 * File:   AbstractLayout.h
 */

#ifndef GUI_ABSTRACTLAYOUT_H
#define GUI_ABSTRACTLAYOUT_H

#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct
{
  Object_t ** Childrens;

  BYTE pos0, pos;
  BYTE spacing0;
  float spacing;
  BYTE count;

  BYTE * Stretchs;
  Orientation_t orientation;
  GUI_minmax_fxn_t min_fxn;
} AbstractLayout_t;

void init_AbstractLayout(AbstractLayout_t * This, Object_t ** Childrens, BYTE count,
                         Orientation_t orientation, BYTE pos0, BYTE pos, BYTE spacing);

void AbstractLayout_setSegment(AbstractLayout_t * This, Orientation_t orientation, BYTE pos0, BYTE pos);
void AbstractLayout_setSegmentFromObj(AbstractLayout_t * This,
                                      Orientation_t orientation, Object_t * obj);

INLINE void AbstractLayout_setCount(AbstractLayout_t * This, BYTE count)
{
  This->count = count;
}

INLINE void AbstractLayout_setStretch(AbstractLayout_t * This, BYTE * stretch)
{
  This->Stretchs = stretch;
}

INLINE void AbstractLayout_setSpacing(AbstractLayout_t * This, BYTE spacing)
{
  This->spacing = This->spacing0 = spacing;
}

float AbstractLayout_EqualSegmentLen(AbstractLayout_t * This);
  
#ifdef __cplusplus
}
#endif

#endif /* GUI_ABSTRACTLAYOUT_H */
