/* 
 * File:   System.h
 */

#ifndef GUI_SYSTEM_H
#define GUI_SYSTEM_H

#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif
  
void init_gui();
  
void gui_setCurrentObject(Object_t * This);
void gui_refresh();

void gui_tick_handler();
void gui_key_handler(BYTE key, BYTE isNew);
void gui_key_edit_finished();
  
#ifdef __cplusplus
}
#endif
  
#endif /* GUI_SYSTEM_H */
