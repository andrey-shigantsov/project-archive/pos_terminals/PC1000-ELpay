/* 
 * File:   ListLayout.h
 */

#ifndef GUI_LISTLAYOUT_H
#define GUI_LISTLAYOUT_H

#include "AbstractLayout.h"
#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct
{
  Object_t obj;
  
  AbstractLayout_t base;

  Object_t ** Childrens;
  Alignment_t * ChildrensAlign;
  BYTE ChildrensCount;
} ListLayout_t;

void init_ListLayout(ListLayout_t * This,
  Orientation_t orientation, BYTE spacing,
  Object_t ** Childrens, BYTE ChildrensCount);

INLINE void ListLayout_setAlignment(ListLayout_t * This, Alignment_t * ChildrensAlign)
{
  This->ChildrensAlign = ChildrensAlign;
}

INLINE void ListLayout_setStretch(ListLayout_t * This, BYTE * stretch)
{
  AbstractLayout_setStretch(&This->base, stretch);
}

INLINE void ListLayout_setSpacing(ListLayout_t * This, BYTE spacing)
{
  AbstractLayout_setSpacing(&This->base, spacing);
}

void listlayout_exec(ListLayout_t * This);
  
#ifdef __cplusplus
}
#endif

#endif /* GUI_LISTLAYOUT_H */
