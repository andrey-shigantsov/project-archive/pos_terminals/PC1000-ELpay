/* 
 * File:   AbstractLayout.c
 */

#include "AbstractLayout.h"
#define LOG_HEADER "GUI-SYS/AbstractLayout"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

void init_AbstractLayout(AbstractLayout_t * This, Object_t** Childrens, BYTE count, Orientation_t orientation,
  BYTE pos0, BYTE pos, BYTE spacing)
{
  This->Childrens = Childrens;

  This->count = count;
  This->spacing = This->spacing0 = spacing;

  This->Stretchs = 0;
  This->orientation = -1;
  AbstractLayout_setSegment(This, orientation, pos0,pos);
}

void AbstractLayout_setSegment(AbstractLayout_t * This, Orientation_t orientation, BYTE pos0, BYTE pos)
{
  This->pos0 = pos0;
  This->pos = pos;  
  if (This->orientation != orientation)
  {
    This->orientation = orientation;
    switch (This->orientation)
    {
    case orVertical:
      This->min_fxn = (GUI_minmax_fxn_t)object_minH;
      break;

    case orHorizontal:
      This->min_fxn = (GUI_minmax_fxn_t)object_minW;
      break;
    }
  }
}

void AbstractLayout_setSegmentFromObj(AbstractLayout_t * This,
                                      Orientation_t orientation, Object_t * obj)
{
  if (!obj) return;
  if (!Object_isVisiable(obj)) return;
  
  Box_t b = Object_contentBox(obj);
  switch (orientation)
  {
  case orHorizontal:
    AbstractLayout_setSegment(This, orientation, b.x0, b.x);
    break;
    
  case orVertical:
    AbstractLayout_setSegment(This, orientation, b.y0, b.y);
    break;
  }
}

float AbstractLayout_EqualSegmentLen(AbstractLayout_t * This)
{
  if (!This->count) return 0;
  BYTE visibleCount = This->count, subsegmentsCount = 0, posdif = 0;
  for(int i = 0; i < This->count; ++i)
  {
    if (This->Childrens && This->Childrens[i])
    {
      bool isMin = (Object_SizePolicy(This->Childrens[i],This->orientation) == szMin);
      if (isMin)
        posdif += This->min_fxn(This->Childrens[i]);
      if (isMin)
      {
        continue;
      }
      if (!Object_isVisiable(This->Childrens[i]))
      {
        --visibleCount;
        continue;
      }
    }

    if (This->Stretchs)
      subsegmentsCount += This->Stretchs[i];
    else
      ++subsegmentsCount;
  }
  float dpos = This->pos - This->pos0 - posdif;
  This->spacing = This->spacing0;
  float len;
  if (subsegmentsCount)
  {
    float xpos = (float)(dpos - (visibleCount-1)*This->spacing0);
    len = xpos/subsegmentsCount;
  }
  else
    len = 0;
  This->spacing += (dpos - len*subsegmentsCount)/(visibleCount-1);
  return len;
}
