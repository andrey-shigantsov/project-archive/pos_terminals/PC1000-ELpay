#ifndef SYS_GUI_ACTION_H_
#define SYS_GUI_ACTION_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  const char * name;
  GUI_fxn_t fxn;
} Action_t;

#ifdef __cplusplus
}
#endif

#endif // SYS_GUI_ACTION_H_
