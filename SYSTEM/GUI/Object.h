/* 
 * File:   Object.h
 */

#ifndef GUI_OBJECT_H
#define GUI_OBJECT_H

#include "SYSTEM/base.h"
#include "SYSTEM/tr.h"
#include "SYSTEM/keyboard.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  BYTE x, y;
  BYTE w, h;
} Geometry_t;

typedef struct
{
  BYTE left, right;
  BYTE top, bottom;
} Margins_t;

INLINE void Geometry_set(Geometry_t * This, int x, int y, int w, int h)
{
  This->x = x;
  This->y = y;
  This->w = w;
  This->h = h;
}

INLINE void Margins_set(Margins_t * This, int t, int l, int r, int b)
{
  This->left = l;
  This->right = r;
  This->top = t;
  This->bottom = b;
}

typedef struct
{
  BYTE x0, y0;
  BYTE x, y;
} Box_t;

typedef struct
{
  bool isInverseColor;
  BYTE size;
} Font_t;

typedef enum
{
  alHLeft = 0x00,
  alHRight = 0x01,
  alHCenter = 0x03,

  alVTop = 0x00,
  alVBottom = 0x10,
  alVCenter = 0x30,
} Alignment_t;

typedef enum
{
  orHorizontal, orVertical
} Orientation_t;

void alignH(Alignment_t align, BYTE * y, BYTE * h, BYTE min);
void alignW(Alignment_t align, BYTE * x, BYTE * w, BYTE min);
typedef void (*GUI_align_fxn_t)(Alignment_t align, BYTE * pos, BYTE * max, BYTE min);

typedef enum
{
  szExpand = 0x00,
  szMin = 0x01,
  szMax = 0x02
} SizePolicy_t;

#define ALIGN_H(x) (Alignment_t)(x & 0x0f)
#define ALIGN_V(x) (Alignment_t)(x & 0xf0)

BYTE font_attr(Font_t * This);
BYTE font_width(Font_t * This);
BYTE string_width(Font_t * This, char * str);

typedef void (*GUI_static_fxn_t)();
typedef void (*GUI_fxn_t)(void * Master);
typedef void (*GUI_key_handler_fxn_t)(void * Master, BYTE key, BYTE isNew);
typedef BYTE (*GUI_minmax_fxn_t)(void * Master);

typedef struct
{
  GUI_minmax_fxn_t w, h;
} MinMaxFxns_t;

typedef struct
{
  void * Master, /* Основной GUI класс */
    * Parent; /* Передаётся в callback-функции */

  const char * name;
  
  Geometry_t geometry;
  Margins_t margins, marginsInternal;
  
  SizePolicy_t SizePolicyH, SizePolicyV;

  Font_t font;
  
  bool isVisiable, isNeedResize;

  MinMaxFxns_t minFxn, maxFxn, contentMinFxn, contentMaxFxn;

  GUI_fxn_t resize;
  GUI_fxn_t current_handler;

  GUI_fxn_t draw;

  GUI_fxn_t tick_handler;

  GUI_key_handler_fxn_t key_handler;
  GUI_fxn_t key_edit_finished;

} Object_t;

void init_Object(Object_t * This, void * Master, const char * name);
void Object_setParent(Object_t * This, void * Parent);

void Object_copyAllParams(Object_t * This, Object_t * src);
void Object_copySizeParams(Object_t * This, Object_t * src);

INLINE SizePolicy_t Object_SizePolicy(Object_t * This, Orientation_t orientatin)
{
  switch(orientatin)
  {
  case orVertical:
    return This->SizePolicyV;
  case orHorizontal:
    return This->SizePolicyH;
  }
  assert(0);
}

void Object_setSizePolicy(Object_t * This, SizePolicy_t h, SizePolicy_t v);

void Object_setGeometry(Object_t * This, int x, int y, int w, int h);

void Object_setInternalMargins(Object_t * This, int t, int l, int r, int b);
void Object_setMargins(Object_t * This, int t, int l, int r, int b);

Box_t Object_Box(Object_t * This);

void Object_align(Object_t * This, Alignment_t align);

Geometry_t Object_contentGeometry(Object_t * This);
Geometry_t Object_contentGeometryAuto(Object_t * This, Alignment_t align);

int Object_maxContentStringLen(Object_t * This);

Box_t Object_contentBox(Object_t * This);
Box_t Object_contentBoxAuto(Object_t * This, Alignment_t align);

void Object_clearDrawBox(Object_t * This);
void Object_fillDrawBox(Object_t * This, uchar color);

INLINE bool Object_isVisiable(Object_t * This)
{
  return This->isVisiable;
}
INLINE void Object_setVisiable(Object_t * This, bool flag)
{
  This->isVisiable = flag;
}

BYTE object_minH(Object_t * This);
BYTE object_minW(Object_t * This);
BYTE object_maxH(Object_t * This);
BYTE object_maxW(Object_t * This);

BYTE object_content_minH(Object_t * This);
BYTE object_content_minW(Object_t * This);
BYTE object_content_maxH(Object_t * This);
BYTE object_content_maxW(Object_t * This);

void object_resize(Object_t * This);
void object_current_handler(Object_t * This);

void object_draw(Object_t * This);

void object_tick_handler(Object_t * This);

void object_key_handler(Object_t * This, BYTE key, BYTE isNew);
void object_key_edit_finished(Object_t * This);

#ifdef __cplusplus
}
#endif


#endif /* GUI_OBJECT_H */
