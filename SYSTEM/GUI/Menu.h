/*
 * File:   Menu.h
 */

#ifndef MENU_H
#define MENU_H

#include "Object.h"
#include "Action.h"
#include "TableLayout.h"
#include "ListLayout.h"
#include "Label.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GUI_MENU_SMALL_ROWCOUNT 4
#define GUI_MENU_SMALL_COLCOUNT 2
#define GUI_MENU_SMALL_LABELLEN 9

#define GUI_MENU_NORM_ROWCOUNT 4
#define GUI_MENU_NORM_COLCOUNT 1
#define GUI_MENU_NORM_LABELLEN 24

// + 1 для случая без заголовка
#if GUI_MENU_SMALL_ROWCOUNT >= GUI_MENU_NORM_ROWCOUNT
#define GUI_MENU_OBJBUF_SIZE GUI_MENU_SMALL_ROWCOUNT + 1
#else
#define GUI_MENU_OBJBUF_SIZE GUI_MENU_NORM_ROWCOUNT + 1
#endif

#if GUI_MENU_SMALL_COLCOUNT >= GUI_MENU_NORM_COLCOUNT
#define GUI_MENU_OBJS_COLCOUNT GUI_MENU_SMALL_COLCOUNT
#else
#define GUI_MENU_OBJS_COLCOUNT GUI_MENU_NORM_COLCOUNT
#endif

#if GUI_MENU_SMALL_LABELLEN > GUI_MENU_NORM_LABELLEN
#define GUI_MENU_LABEL_BUFLEN GUI_MENU_SMALL_LABELLEN
#else
#define GUI_MENU_LABEL_BUFLEN GUI_MENU_NORM_LABELLEN
#endif

typedef enum {menuSmall, menuNormal} MenuLayoutSize_t;
#define GUI_MENU_LAYOUT_SIZES_COUNT (menuNormal + 1)

typedef void (* menu_item_action_fxn_t)(void * Parent, int idx);

#define GUI_MENU_ITEM_OBJSCOUNT 2
typedef struct
{
  Object_t obj;
  Label_t Key, Label;

  ListLayout_t layout;
  Object_t * Objs[GUI_MENU_ITEM_OBJSCOUNT];
  Alignment_t ObjsAlign[GUI_MENU_ITEM_OBJSCOUNT];
  BYTE ObjsStretch[GUI_MENU_ITEM_OBJSCOUNT];
} MenuItem_t;

typedef struct
{
  Object_t obj;
  TableLayout_t layout;
  MenuLayoutSize_t layoutSize;
  BYTE rowsCount, colsCount, pageItemsCount;

  MenuItem_t * Items;
  int ItemsCount;
  int currentItemIdx;
  menu_item_action_fxn_t item_action;

  Object_t ** ObjBuf[GUI_MENU_OBJBUF_SIZE];
  Object_t * Objs[GUI_MENU_OBJBUF_SIZE][GUI_MENU_OBJS_COLCOUNT];

  bool isHeader;
  Label_t Header;
} Menu_t;

void init_MenuItem(MenuItem_t * This, Menu_t * Parent);
void MenuItems_FromActions(MenuItem_t * Items, Menu_t * Parent, Action_t * Actions, int Count);
INLINE char * MenuItem_Text(MenuItem_t * This)
{
  return Label_Text(&This->Label);
}

void init_Menu(Menu_t * This);

INLINE char * Menu_Header(Menu_t * This)
{
  return Label_Text(&This->Header);
}

void Menu_enableHeader(Menu_t * This, bool flag);

INLINE void Menu_setHeader(Menu_t * This, const char * text)
{
  Menu_enableHeader(This,(text != 0)&&(text[0] != '\0'));
  Label_setText(&This->Header, text);
}

void Menu_setItems(Menu_t * This, MenuItem_t * Items, int Count);
void Menu_setLayoutSize(Menu_t * This, MenuLayoutSize_t size);

#ifdef __cplusplus
}
#endif

#endif /* MENU_H */
