/* 
 * File:   LineEdit.h
 */

#ifndef GUI_LINEEDIT_H
#define GUI_LINEEDIT_H

#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct
{
  Object_t obj;

  BYTE isSelected, isPassword, pwdLastIsHide, cursorIsVisible, 
    isLastCharChanging, isLastCharAppend;

  Alignment_t alignText;
  char * text;
  BYTE textLen, textBufSize;

  BYTE maxDispLen;
  
  int curVisTickCount, curUnvisTickCount, curTickCounter;
  int pwdHideLastTickCount, pwdTickCounter;
} LineEdit_t;

void init_LineEdit(LineEdit_t * This);
void LineEdit_setTextBuf(LineEdit_t * This, char * buf, BYTE size);
void LineEdit_setTextBufWithLen(LineEdit_t * This, char * buf, BYTE size, BYTE len);

INLINE void LineEdit_setTextAlignment(LineEdit_t * This, Alignment_t align)
{
  This->alignText = align;
}

void LineEdit_setText(LineEdit_t * This, const char * text);
const char * LineEdit_Text(LineEdit_t * This);

void LineEdit_setSelection(LineEdit_t * This, BYTE flag);
void LineEdit_enablePasswordMode(LineEdit_t * This);
void LineEdit_passwordHideLast(LineEdit_t * This);

#ifdef __cplusplus
}
#endif

#endif /* GUI_LINEEDIT_H */
