/*
 * WlsIndicator.c
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */


#include "NetworkIndicator.h"
#define LOG_HEADER "GUI-SYS/NetworkIndicator"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"
#include "SYSTEM/output.h"
#include "SYSTEM/NETWORK/common.h"
#include "SYSTEM/NETWORK/System.h"

static void draw(NetworkIndicator_t * This)
{
  Object_clearDrawBox(&This->obj);

  if (This->SignalLevelResult)
    system_dbg(0,0, "sl:%s", network_resStr(This->SignalLevelResult));
  else if (This->StatusSimResult)
    system_dbg(0,0, "ss:%s", network_resStr(This->StatusSimResult));

  Box_t b = Object_contentBox(&This->obj);

  if (!This->SignalLevelResult)
  {
    BYTE x = b.x;
    BYTE y = b.y;
    BYTE dx = 1, dy;

    dy = (This->SignalLevel == netSignal_VeryStrong)? 7 : 0;
    Lib_LcdDrawBox(x-dx,y-dy,x,y);

    x -= dx + 2;

    dy = (This->SignalLevel <= netSignal_Strong)? 5 : 0;
    Lib_LcdDrawBox(x-dx,y-dy,x,y);

    x -= dx + 2;

    dy = (This->SignalLevel <= netSignal_Normal)? 3 : 0;
    Lib_LcdDrawBox(x-dx,y-dy,x,y);

    x -= dx + 2;

    dy = (This->SignalLevel <= netSignal_Weak)? 2 : 0;
    Lib_LcdDrawBox(x-dx,y-dy,x,y);

    x -= dx + 2;

    dy = (This->SignalLevel <= netSignal_VeryWeak)? 1 : 0;
    Lib_LcdDrawBox(x-dx,y-dy,x,y);

    if (This->SignalLevel == netSignal_No)
    {
      Box_t b2;
      BYTE s = 4;
      b2.x0 = b.x - 14 + 2;
      b2.y0 = b.y - s - 3;
      b2.x = b2.x0 + s;
      b2.y = b2.y0 + s;

      Lib_LcdDrawLine(b2.x0, b2.y0, b2.x, b2.y, 1);
      Lib_LcdDrawLine(b2.x, b2.y0, b2.x0, b2.y, 1);
    }
  }

}

static void update(NetworkIndicator_t * This)
{
  LOG_FXN(INF, START);

  if (network_sys_isInit())
  {
    This->isUpdating = true;
    object_draw(&This->obj);

    This->SignalLevelResult = network_SignalLevel(&This->SignalLevel);
    This->StatusSimResult = network_CheckSim();
    This->isUpdating = false;
  }
  else
  {
    This->SignalLevelResult = 0;
    This->StatusSimResult = 0;
    This->SignalLevel = netSignal_No;
  }

  LOG_FXN(INF, OK);
}

static void resize(NetworkIndicator_t * This)
{

}

void NetworkIndicator_update(NetworkIndicator_t * This)
{
  update(This);
  object_draw(&This->obj);
}

void init_NetworkIndicator(NetworkIndicator_t * This)
{
  init_Object(&This->obj, This, "NetIndicator");
  This->obj.draw = (GUI_fxn_t)draw;
  This->obj.resize = (GUI_fxn_t)resize;

  This->isUpdating = false;

  This->SignalLevel = netSignal_No;
  This->SignalLevelResult = 0;

  This->StatusSimResult = 0;
}
