/* 
 * File:   System.c
 */

#include "System.h"
#define LOG_HEADER "GUI-SYS/System"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

Object_t * CurrentObj;

void init_gui()
{
  CurrentObj = 0;
}

void gui_setCurrentObject(Object_t * object)
{
  CurrentObj = object;
  object_current_handler(CurrentObj);
  object_draw(CurrentObj);
}

void gui_refresh()
{
  object_draw(CurrentObj);
}

void gui_tick_handler()
{
  object_tick_handler(CurrentObj);
}

void gui_key_handler(BYTE key, BYTE isNew)
{
  object_key_handler(CurrentObj, key, isNew);
}

void gui_key_edit_finished()
{
  object_key_edit_finished(CurrentObj);
}
