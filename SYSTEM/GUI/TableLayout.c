/* 
 * File:   TableLayout.c
 */

#include "TableLayout.h"
#define LOG_HEADER "GUI-SYS/TableLayout"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

static void draw(TableLayout_t * This)
{
	BYTE i,j;
  for (i = 0; i < This->RowsCount; ++i)
    for (j = 0; j < This->ColumnsCount; ++j)
      object_draw(This->Childrens[i][j]);
}

static void resize(TableLayout_t * This)
{
  AbstractLayout_setSegmentFromObj(&This->baseH, orHorizontal, &This->obj);
  AbstractLayout_setSegmentFromObj(&This->baseV, orVertical, &This->obj);
  tablelayout_exec(This);

  BYTE i,j;
  for (i = 0; i < This->RowsCount; ++i)
    for (j = 0; j < This->ColumnsCount; ++j)
      object_resize(This->Childrens[i][j]);
}

void init_TableLayout(TableLayout_t * This,
  BYTE spacingH, BYTE spacingV,
  Object_t *** Childrens, BYTE RowsCount, BYTE ColumnsCount)
{
  init_Object(&This->obj, This, "TableLayout");
  This->obj.resize = (GUI_fxn_t)resize;
  This->obj.draw = (GUI_fxn_t)draw;

  This->Childrens = Childrens;
  This->ChildrensAlign = 0;
  This->RowsCount = RowsCount;
  This->ColumnsCount = ColumnsCount;

  init_AbstractLayout(&This->baseH, 0, ColumnsCount, orHorizontal, 0, 0, spacingH);
  init_AbstractLayout(&This->baseV, 0, RowsCount, orVertical, 0, 0, spacingV);
}

void TableLayout_setCounts(TableLayout_t * This, BYTE RowsCount, BYTE ColumnsCount)
{
  This->RowsCount = RowsCount;
  This->ColumnsCount = ColumnsCount;
  
  AbstractLayout_setCount(&This->baseH, ColumnsCount);
  AbstractLayout_setCount(&This->baseV, RowsCount);

  This->obj.isNeedResize = true;
}

void tablelayout_exec(TableLayout_t * This)
{
  Geometry_t g0 = Object_contentGeometry(&This->obj);
  BYTE
    x = g0.x,
    y = g0.y;
  float
    w = AbstractLayout_EqualSegmentLen(&This->baseH),
    h = AbstractLayout_EqualSegmentLen(&This->baseV);

  BYTE i,j;
  for (i = 0; i < This->RowsCount; ++i)
  {
    float H = h;
    if (This->baseV.Stretchs)
      H *= This->baseV.Stretchs[i];
    x = g0.x;
    for (j = 0; j < This->ColumnsCount; ++j)
    {
      float W = w;
      if (This->baseH.Stretchs)
        W *= This->baseH.Stretchs[j];
      if (This->Childrens[i][j])
      {
        Object_t * obj = This->Childrens[i][j];
        Geometry_t * g = &(obj->geometry);
        Alignment_t al = (This->ChildrensAlign ? This->ChildrensAlign[i][j] : alHCenter|alVCenter);
        g->x = x;
        g->y = y;
        g->w = W;
        g->h = H;
        Object_align(obj, al);
        object_resize(obj);
      }
      x += W + This->baseH.spacing;
    }
    y += H + This->baseV.spacing;
  }
}
