/*
 * WlsIndicator.h
 *
 *  Created on: 24 февр. 2017 г.
 *      Author: Svetozar
 */

#ifndef SYSTEM_GUI_NETWORKINDICATOR_H_
#define SYSTEM_GUI_NETWORKINDICATOR_H_

#include "Object.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  Object_t obj;
  int SignalLevel;

  bool isUpdating;
  int SignalLevelResult, StatusSimResult;
} NetworkIndicator_t;

void init_NetworkIndicator(NetworkIndicator_t * This);

void NetworkIndicator_update(NetworkIndicator_t * This);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_GUI_NETWORKINDICATOR_H_ */
