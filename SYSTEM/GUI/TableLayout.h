/* 
 * File:   TableLayout.h
 */

#ifndef GUI_TABLELAYOUT_H
#define GUI_TABLELAYOUT_H

#include "AbstractLayout.h"
#include "Object.h"

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct
{
  Object_t obj;
  
  AbstractLayout_t baseH, baseV;

  Object_t *** Childrens;
  Alignment_t ** ChildrensAlign;
  BYTE RowsCount, ColumnsCount;
} TableLayout_t;

void init_TableLayout(TableLayout_t * This,
  BYTE spacingH, BYTE spacingV,
  Object_t *** Childrens, BYTE RowsCount, BYTE ColumnsCount);

INLINE void TableLayout_setChildrensAlignment(TableLayout_t * This, Alignment_t ** ChildrensAlign)
{
  This->ChildrensAlign = ChildrensAlign;
}

INLINE void TableLayout_setRowsStretch(TableLayout_t * This, BYTE * stretch)
{
  AbstractLayout_setStretch(&This->baseV, stretch);
}

INLINE void TableLayout_setColumnsStretch(TableLayout_t * This, BYTE * stretch)
{
  AbstractLayout_setStretch(&This->baseH, stretch);
}

void TableLayout_setCounts(TableLayout_t * This, BYTE RowsCount, BYTE ColumnsCount);

INLINE void TableLayout_setRowsSpacing(TableLayout_t * This, BYTE spacing)
{
  AbstractLayout_setSpacing(&This->baseV, spacing);
}

INLINE void TableLayout_setColumnsSpacing(TableLayout_t * This, BYTE spacing)
{
  AbstractLayout_setSpacing(&This->baseH, spacing);
}

void tablelayout_exec(TableLayout_t * This);
  
#ifdef __cplusplus
}
#endif

#endif /* GUI_TABLELAYOUT_H */
