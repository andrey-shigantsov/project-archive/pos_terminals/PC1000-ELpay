/* 
 * File:   Object.c
 */

#include "Object.h"
#define LOG_HEADER "GUI-SYS/Object"
#define LOG_LEVEL SYSGUI_LOG_LEVEL
#include "SYSTEM/logs.h"

#include <string.h>

#define GUI_LOG_FXN(lvl,state) LOG_FXN_PREMSG(lvl,state, " for \"%s\"", This->name)
#define GUI_LOG_FXN_MSG(lvl,state,format,...) LOG_FXN_PREMSG(lvl,state, " for \"%s\"" format ": ", This->name, ##__VA_ARGS__)

static Box_t GeometryToBox(Geometry_t g)
{
  Box_t b;
  b.x0 = g.x;
  b.y0 = g.y;
  b.x = g.x + g.w - 1;
  b.y = g.y + g.h - 1;
  return b;
}

BYTE font_attr(Font_t * This)
{
  BYTE size;
  switch (This->size)
  {
  default:
  case 8:
    size = 0x00;
    break;
    
  case 16:
    size = 0x01;
    break;
  }
  return (This->isInverseColor << 7) | size;
}

BYTE font_width(Font_t * This)
{
  BYTE w;
  switch (This->size)
  {
  default:
  case 8:
    w = 6;
    break;
    
  case 16:
    w = 8;
    break;
  }
  return w;  
}

BYTE string_width(Font_t * This, char * str)
{
  return strlen(str)*font_width(This);
}

void init_Object(Object_t * This, void * Master, const char * name)
{
  assert(This != 0);

  This->name = name;
  GUI_LOG_FXN(DBG,START);

  This->Master = Master;
  This->Parent = 0;
  
  memset(&This->minFxn, 0, sizeof(This->minFxn));
  memset(&This->maxFxn, 0, sizeof(This->maxFxn));
  memset(&This->contentMinFxn, 0, sizeof(This->contentMinFxn));
  memset(&This->contentMaxFxn, 0, sizeof(This->contentMaxFxn));

  This->resize = 0;
  This->current_handler = 0;

  This->draw = 0;

  This->tick_handler = 0;

  This->key_handler = 0;
  This->key_edit_finished = 0;
  

  This->SizePolicyH = szExpand;
  This->SizePolicyV = szExpand;

  Geometry_set(&This->geometry, 0,0,0,0);

  Margins_set(&This->marginsInternal, 0,0,0,0);
  Margins_set(&This->margins, 0,0,0,0);
  
  This->font.size = 8;
  This->font.isInverseColor = 0;

  This->isVisiable = true;
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_setParent(Object_t * This, void * Parent)
{
  if (!This) return;

  This->Parent = Parent;
}

void Object_copySizeParams(Object_t * This, Object_t * src)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  This->geometry = src->geometry;
  This->margins = src->margins;
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_copyAllParams(Object_t * This, Object_t * src)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  This->font = src->font;
  This->geometry = src->geometry;
  This->marginsInternal = src->marginsInternal;
  This->margins = src->margins;
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_setSizePolicy(Object_t * This, SizePolicy_t h, SizePolicy_t v)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  This->SizePolicyH = h;
  This->SizePolicyV = v;
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_setGeometry(Object_t * This, int x, int y, int w, int h)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  Geometry_set(&This->geometry, x,y,w,h);
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_setInternalMargins(Object_t * This, int t, int l, int r, int b)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  Margins_set(&This->marginsInternal, t,l,r,b);
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

void Object_setMargins(Object_t * This, int t, int l, int r, int b)
{
  if (!This) return;

  GUI_LOG_FXN(DBG,START);

  Margins_set(&This->margins, t,l,r,b);
  This->isNeedResize = true;

  GUI_LOG_FXN(DBG,OK);
}

Box_t Object_Box(Object_t * This)
{
  return GeometryToBox(This->geometry);
}

#define OBJECT_ALL_MARGINS(This,x) (This->marginsInternal.x + This->margins.x)
#define OBJECT_CONTENT_GEOMETRY_X(This) (This->geometry.x + OBJECT_ALL_MARGINS(This,left))
#define OBJECT_CONTENT_GEOMETRY_Y(This) (This->geometry.y + OBJECT_ALL_MARGINS(This,top))
#define OBJECT_CONTENT_GEOMETRY_W(This) (This->geometry.w - (OBJECT_ALL_MARGINS(This,left) + OBJECT_ALL_MARGINS(This,right)))
#define OBJECT_CONTENT_GEOMETRY_H(This) (This->geometry.h - (OBJECT_ALL_MARGINS(This,top) + OBJECT_ALL_MARGINS(This,bottom)))

Margins_t Object_AllMargins(Object_t * This)
{
  Margins_t res;
  res.bottom =  OBJECT_ALL_MARGINS(This,bottom);
  res.left = OBJECT_ALL_MARGINS(This,left);
  res.right = OBJECT_ALL_MARGINS(This,right);
  res.top = OBJECT_ALL_MARGINS(This,top);
  return res;
}

Geometry_t Object_contentGeometry(Object_t * This)
{
  Geometry_t g;
  Margins_t m = Object_AllMargins(This);
  g.x = OBJECT_CONTENT_GEOMETRY_X(This);
  g.y = OBJECT_CONTENT_GEOMETRY_Y(This);
  g.w = OBJECT_CONTENT_GEOMETRY_W(This);
  g.h = OBJECT_CONTENT_GEOMETRY_H(This);
  return g;
}

INLINE BYTE Object_MinHAuto(Object_t * This)
{
  return OBJECT_ALL_MARGINS(This,top) + OBJECT_ALL_MARGINS(This,bottom) + object_content_minH(This);
}

INLINE BYTE Object_MinWAuto(Object_t * This)
{
  return OBJECT_ALL_MARGINS(This,left) + OBJECT_ALL_MARGINS(This,right) + object_content_minW(This);
}

void alignH(Alignment_t align, BYTE * y, BYTE * h, BYTE min)
{
  if (min >= *h) return;
  switch(ALIGN_V(align))
  {
  default:
  case alVTop:
    break;

  case alVBottom:
    *y += *h - min;
    break;

  case alVCenter:
    *y += (*h - min)/2;
    break;
  }
  *h = min;
}

void alignW(Alignment_t align, BYTE * x, BYTE * w, BYTE min)
{
  if (min >= *w) return;
  switch(ALIGN_H(align))
  {
  default:
  case alHLeft:
    break;

  case alHRight:
    *x += *w - min;
    break;

  case alHCenter:
    *x += (*w - min)/2;
    break;
  }
  *w = min;
}

void Object_align(Object_t * This, Alignment_t align)
{
  switch(This->SizePolicyH)
  {
  case szMax:
  case szExpand:
    break;

  case szMin:
    alignW(align,&This->geometry.x,&This->geometry.w,object_minW(This));
    break;
  }
  switch(This->SizePolicyV)
  {
  case szMax:
  case szExpand:
    break;

  case szMin:
    alignH(align,&This->geometry.y,&This->geometry.h,object_minH(This));
    break;
  }
}

Geometry_t Object_contentGeometryAuto(Object_t * This, Alignment_t align)
{
  Geometry_t g = Object_contentGeometry(This);

  BYTE
    minH = object_content_minH(This),
    minW = object_content_minW(This);

  alignH(align,&g.y,&g.h,minH);
  alignW(align,&g.x,&g.w,minW);

  return g;
}

Box_t Object_contentBox(Object_t * This)
{
  return GeometryToBox(Object_contentGeometry(This));
}

Box_t Object_contentBoxAuto(Object_t * This, Alignment_t align)
{
  return GeometryToBox(Object_contentGeometryAuto(This, align));
}

void Object_fillDrawBox(Object_t * This, uchar color)
{
  if (!This) return;

  Box_t b = Object_Box(This);

  int y;
  for (y = b.y0; y <= b.y; ++y)
    Lib_LcdDrawLine(b.x0,y,b.x,y,color);
}

void Object_clearDrawBox(Object_t * This)
{
  Object_fillDrawBox(This, 0);
}

int Object_maxContentStringLen(Object_t * This)
{
  return Object_contentGeometry(This).w / font_width(&This->font);
}

//-------------------------------------------------------------

#define GUI_CALLBACK(This, fxn, ...) \
  if (!This->fxn) return; \
  This->fxn(__VA_ARGS__);

#define GUI_CALLBACK_WITH_RET(default_val, This, fxn, ...) \
  if (!This->fxn) return default_val; \
  return This->fxn(__VA_ARGS__);

#define GUI_CALLBACK_WITH_LOG(lvl, This, fxn, ...) \
  if (!This->fxn) return; \
  GUI_LOG_FXN(lvl,START); \
  This->fxn(__VA_ARGS__); \
  GUI_LOG_FXN(lvl,OK);

void object_resize(Object_t * This)
{
  if (!This) return;
  This->isNeedResize = false;
  GUI_CALLBACK_WITH_LOG(DBG,This,resize,This->Master);
}

void object_current_handler(Object_t * This)
{
  if (!This) return;
  GUI_CALLBACK(This,current_handler,This->Master)
}

void object_draw(Object_t * This)
{
  if (!This) return;
  if (!This->isVisiable) return;
  if (This->isNeedResize)
    object_resize(This);
  GUI_CALLBACK_WITH_LOG(DBG,This,draw,This->Master);
}

void object_tick_handler(Object_t * This)
{
  if (!This) return;
  GUI_CALLBACK(This,tick_handler,This->Master);
}

void object_key_handler(Object_t * This, BYTE key, BYTE isNew)
{
  if (!This) return;
  if (!This->isVisiable) return;
  GUI_CALLBACK(This,key_handler,This->Master, key, isNew);
}

void object_key_edit_finished(Object_t * This)
{
  if (!This) return;
  if (!This->isVisiable) return;
  GUI_CALLBACK(This,key_edit_finished,This->Master);
}

BYTE object_minH(Object_t* This)
{
  if (!This) return -1;
  if (!This->minFxn.h)
    return Object_MinHAuto(This);
  return This->minFxn.h(This->Master);
}

BYTE object_minW(Object_t* This)
{
  if (!This) return -1;
  if (!This->minFxn.w)
    return Object_MinWAuto(This);
  return This->minFxn.w(This->Master);
}

BYTE object_maxH(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = This->geometry.h;
  GUI_CALLBACK_WITH_RET(defaultVal, This,maxFxn.h,This->Master);
}

BYTE object_maxW(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = This->geometry.w;
  GUI_CALLBACK_WITH_RET(defaultVal, This,maxFxn.w,This->Master);
}

BYTE object_content_minH(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = OBJECT_CONTENT_GEOMETRY_H(This);
  GUI_CALLBACK_WITH_RET(defaultVal, This,contentMinFxn.h,This->Master);
}

BYTE object_content_minW(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = OBJECT_CONTENT_GEOMETRY_W(This);
  GUI_CALLBACK_WITH_RET(defaultVal, This,contentMinFxn.w,This->Master);
}

BYTE object_content_maxH(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = OBJECT_CONTENT_GEOMETRY_H(This);
  GUI_CALLBACK_WITH_RET(defaultVal, This,contentMaxFxn.h,This->Master);
}

BYTE object_content_maxW(Object_t* This)
{
  if (!This) return -1;
  BYTE defaultVal = OBJECT_CONTENT_GEOMETRY_W(This);
  GUI_CALLBACK_WITH_RET(defaultVal, This,contentMaxFxn.w,This->Master);
}
